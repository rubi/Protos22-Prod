import os, sys
from optparse import OptionParser
#patterns:
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_SM_CTEQ6L1_13TeV.TXT.mc15_v1
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_Pzplus1_CTEQ6L1_13TeV.TXT.mc15_v1
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_Pzminus1_CTEQ6L1_13TeV.TXT.mc15_v1
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_Pxplus1_CTEQ6L1_13TeV.TXT.mc15_v1
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_Pxminus1_CTEQ6L1_13TeV.TXT.mc15_v
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_Pyplus1_CTEQ6L1_13TeV.TXT.mc15_v1
#group.phys-gener.protos22b.410908.st_tchan_lept_tbj_Pyminus1_CTEQ6L1_13TeV.TXT.mc15_v1

# define colors
BLUE = '\033[94m'
GREEN = '\033[92m'
BOLD = "\033[1m"
WARNING = '\033[93m'
ERROR = '\033[91m'
ENDC = '\033[0m'
Red = '\033[91m'
Cyan = '\033[96m'
White = '\033[97m'
Yellow = '\033[93m'
Magenta = '\033[95m'
Grey = '\033[90m'
Black = '\033[90m'
Default = '\033[99m'

# =================================================================================
#  put
# =================================================================================
def put(text):
    # print '%s' % text
    myfile.write('%s\n' % text)

# =================================================================================
#  br
# =================================================================================
def br():
    # print
    myfile.write('\n')

# =================================================================================
#  prepareDATfile
# =================================================================================
def prepareDATfile(filename, rndseed, events):
    global myfile
    myfile = open(filename, "w")
    put(' 4                               ! Process code')
    put(' 13000.                          ! CM energy')
    put('172.50  1.45  4.95   125.0       ! mt, Gt, mb, MH')
    put('0    0.0                         ! Matching')
    put(' 1.000   0.000   0.000   0.000   ! Re VL, Re VR, Re gL, Re gR')
    put(' 0.000   0.000   0.000   0.000   ! Im VL, Im VR, Im gL, Im gR')
    put('    %d	 	          	 ! Initial random seed' % rndseed)
    put('    %d	 	 	 ! unweighted events' % events)
    put('4.4918   0.008445475	 	 ! t cross section and error')
    put('2.3507175   0.006097575	 	 ! tbar cross section and error')
    myfile.close()
#=====================================================================
# main()
#=====================================================================
def main():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage=usage, version="%prog 1.0")
    parser.add_option("-p", "--pattern", dest="pattern", default="group.phys-gener.protos22b.410908.st_tchan_lept_tbj_SM_CTEQ6L1_13TeV.TXT.mc15_v1",
                      help="set dataset pattern name [default: %default]", metavar="DS name", )
    parser.add_option("-i", "--inputfilename", dest="inputfilepath", default="../test/test10K24R01.dat",
                      help="set path of the binary input file [default: %default]", metavar="PATH")
    parser.add_option("--start", dest="start", default=1,
                      help="set the starting event number to be processed [default: %default]", metavar="NUM", type="int")
    parser.add_option("--end", dest="end", default=240000,
                      help="set the last event number to be processed [default: %default]", metavar="NUM", type="int")
    parser.add_option("--step", dest="step", default=5000,
                      help="set the number of events per file [default: %default]", metavar="NUM", type="int")
    parser.add_option("-r", "--reprocess", action="store_true", dest="reprocess", default=False,
                      help="set if the files should be reprocessed [default: %default]")
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                      help="set debug mode [default: %default]")
    parser.add_option("--polManip", action="store_true", dest="polManip", default=False,
                      help="polManip algorithm on [default: %default]")
    parser.add_option("--mode", action="store", dest="polMode", default="sm",
                      help="polarization mode wanted for polManip. [default: %default]", type="string")
    (options, args) = parser.parse_args()

    if options.end < options.step:
        print +RED + "ERROR :: check your requested end value" + ENDC
        sys.exit()

    workingDir = os.environ['PWD']
    # print workingDir

    # os.system('rm -fr %s' % options.pattern)
    os.system('mkdir -p %s' % options.pattern)
    os.chdir(options.pattern)
    
    doDummy = False

    for index,i in enumerate(range(options.start-1, options.end, options.step)):
        # print index
        myindex = (((options.start-1)/options.step)+1)+index
        firstEvent = (i+1)-(options.start-1)
        lastEvent = (myindex*options.step)-(options.start-1)
        print str(firstEvent) + '; ' + str(lastEvent)
        # print myindex
        if os.path.isfile('%s._%05d.tar.gz' % (options.pattern, myindex)):
            if options.reprocess:
                print 'INFO :: File %s._%05d.tar.gz already exists but it will be overwritten!' % (options.pattern, myindex)
            else:
                print WARNING + 'WARNING :: File %s._%05d.tar.gz already exists! It will not be overwritten!' % (options.pattern, myindex) + ENDC
                continue
        if not doDummy:
            filenameEVENTS = '%s._%05d.events' % (options.pattern, myindex)

            cmd = '/afs/cern.ch/user/r/rubi/work/Protos22-Prod/toAtlas %s %d %d > %s' % (options.inputfilepath, firstEvent, lastEvent, filenameEVENTS)
            cmdPolZ = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -px 0 -py 0 -pz 1.0 -g 1 -n 10000' % filenameEVENTS
            cmdPolZ0 = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -px 0 -py 0 -pz -1.0 -g 1 -n 10000' % filenameEVENTS
            cmdPolX = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -px 1.0 -py 0 -pz 0 -g 1 -n 10000' % filenameEVENTS
            cmdPolX0 = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -px -1.0 -py 0 -pz 0 -g 1 -n 10000' % filenameEVENTS
            cmdPolY = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -px 0 -py 1 -pz 0 -g 1 -n 10000' % filenameEVENTS
            cmdPolY0 = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -px 0 -py -1.0 -pz 0 -g 1 -n 10000' % filenameEVENTS
            cmdPolSM = '/afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/build/stPolManip -f %s -g -1 -n 10000' % filenameEVENTS

            if options.debug: print BLUE + cmd + ENDC
            os.system(cmd)
            filenameDAT = '%s._%05d.dat' % (options.pattern, myindex)
            # print filenameDAT
            prepareDATfile(filenameDAT, myindex, options.step)
            # os.system('more %s' % filenameDAT)
            if options.debug:
                os.system('head -8 %s' % filenameEVENTS)
                os.system('tail -n 8 %s' % filenameEVENTS)
            if options.polManip:
                if options.polMode == "+z":
                    print Magenta + cmdPolZ + ENDC
                    os.system(cmdPolZ)
                if options.polMode == "-z":
                    print Magenta + cmdPolZ0 + ENDC
                    os.system(cmdPolZ0)
                if options.polMode == "+x":
                    print Magenta + cmdPolX + ENDC
                    os.system(cmdPolX)
                if options.polMode == "-x":
                    print Magenta + cmdX0 + ENDC
                    os.system(cmdPolX0)
                if options.polMode == "+y":
                    print Magenta + cmdY + ENDC
                    os.system(cmdPolY)
                if options.polMode == "-y":
                    print Magenta + cmdY0 + ENDC
                    os.system(cmdPolY0)
                if options.polMode == "sm":
                    print Magenta + cmdPolSM + ENDC
                    os.system(cmdPolSM)
                os.system('python /afs/cern.ch/user/r/rubi/work/stPolarization/stPolManip/pyScript/rootTotxt.py -f stPolarization.root')
                os.system('mv fromROOT.events %s' % filenameEVENTS)
            os.system('ln -fs %s._%05d.events protos.events' % (options.pattern, myindex))
            os.system('ln -fs %s._%05d.dat protos.dat' % (options.pattern, myindex))
            os.system('rm -f %s._%05d.tar.gz' % (options.pattern, myindex))
            os.system('tar -cf %s._%05d.tar protos.dat %s %s protos.events' % (options.pattern, myindex, filenameEVENTS, filenameDAT))
            os.system('gzip -f %s._%05d.tar' % (options.pattern, myindex))
            os.system('cp %s._%05d.tar.gz /eos/user/r/rubi/shares/12M' % (options.pattern, myindex))
            if options.debug:
                os.system('ls -lh protos.events protos.dat')
                os.system('ls -lh %s._%05d*' % (options.pattern, myindex))
            os.system('rm %s %s' % (filenameEVENTS, filenameDAT))
            os.system('rm protos.events protos.dat')
            if options.debug: print

    os.chdir(workingDir)
    print '%s is ready!' % options.pattern

# ====================================================================
#  __main__
# ====================================================================
if __name__ == "__main__":
    main()
