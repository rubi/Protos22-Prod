extern "C" void protos_(int &seed,int &saveGrid);
#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <fstream>
#include <random>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int worldWideNumberOfEvents    =0;
int worldWidePipefd            =0;
double worldWideMaxWeight      =0;
double worldWideWeightCieling  =0;
bool worldWideGridCalc         =false;

#include "Protos.h"

// Fortran Common Blocks:

extern "C" struct {
  double Pl[4];
  double Pn[4];
  double Pb[4];
  double Pj[4];
  double PbX[4];
} momext_;
    


extern "C" struct {
  int initgrid;
  int iproc;
} multigr_;

extern "C" struct {
  double q1[4];
  double q2[4];  
} momini_;

extern "C" struct {
  double fxni[16];
} cross_;

extern "C" struct {
    double x1, x2, q;
    int idir, il;
} miscdata_;


// We intercept Protos's call to evtout.  We will write it ourselves. To a pipe!
extern "C" void evtout_(int & mode) {

  
  // Initialize with static variables:  
  static int iEvent=0;
  static std::random_device dev;
  static std::mt19937       engine(dev());
  static std::uniform_real_distribution<double> *flat = worldWideWeightCieling!=0.0 ? new std::uniform_real_distribution<double>(0,worldWideWeightCieling) : NULL; 

  // Not meant to handle mode -1 (file open in protos) or 1 (run summary in protos);
  if (mode!=0) return;
  
  // Event processing:
  double weight=cross_.fxni[multigr_.iproc-1];
  worldWideMaxWeight=std::max(weight,worldWideMaxWeight);


  
  // Unweighting, if desired:
  if (flat)  {
    if (weight > worldWideWeightCieling) {
      std::cerr << "Warning, weight " << weight << " exceeds cieling of " << worldWideWeightCieling << ". Continue at your risk" << std::endl;
    }
    if ((*flat)(engine) > weight) {
      return;
    }
  }
  //
  // 1. Set the global information
  // 
  ProtosEvent event;
  event.eventNumber=iEvent;
  event.weight=weight;
  event.Q=miscdata_.q;
  
  //
  // 2. Set the particle ID information
  //
  static const int idtab1[8] = { 2,4,-1,-3,-2,-4,1,3 };
  static const int idtab2[8] = { 21,21,21,21,21,21,21,21 };
  static const int idtab5[8] = { 5,5,5,5,-5,-5,-5,-5 };
  static const int idtab6[8] = { -5,-5,-5,-5,5,5,5,5 };
  static const int idtab7[8] = { 1,3,-2,-4,-1,-3,2,4 };

  event.incoming[0].pid = idtab1[multigr_.iproc % 9 + multigr_.iproc / 9 - 1];
  event.incoming[1].pid = idtab2[multigr_.iproc % 9 + multigr_.iproc / 9 - 1];
  event.outgoing[2].pid = idtab5[multigr_.iproc % 9 + multigr_.iproc / 9 - 1];
  event.outgoing[3].pid = idtab6[multigr_.iproc % 9 + multigr_.iproc / 9 - 1];
  event.outgoing[4].pid = idtab7[multigr_.iproc % 9 + multigr_.iproc / 9 - 1];
  if (miscdata_.il==1) {
    event.outgoing[0].pid=12;
    event.outgoing[1].pid=-11;
  }
  else if (miscdata_.il==2) {
    event.outgoing[0].pid=14;
    event.outgoing[1].pid=-13;
  }
  else if (miscdata_.il==3) {
    event.outgoing[0].pid=16;
    event.outgoing[1].pid=-15;
  }
  else {
    throw std::runtime_error("Wrong IL in evtout! IL="+std::to_string(miscdata_.il)); 
  }
  if (event.outgoing[2].pid<0) {
    event.outgoing[0].pid *= -1;
    event.outgoing[1].pid *= -1;
  }
  //
  // 3. Set the color information
  //

  event.incoming[0].color1= event.incoming[0].pid > 0 ? 1 : 0;
  event.incoming[0].color2= event.incoming[0].pid > 0 ? 0 : 1;
  event.incoming[1].color1= 2;
  event.incoming[1].color2= 3;
  event.outgoing[0].color1=0;
  event.outgoing[0].color2=0;
  event.outgoing[1].color1=0;
  event.outgoing[1].color2=0;
  event.outgoing[2].color1=event.outgoing[2].pid > 0 ? event.incoming[1].color1 : 0;
  event.outgoing[2].color2=event.outgoing[2].pid > 0 ? 0                        : event.incoming[1].color2;
  event.outgoing[3].color1=event.outgoing[3].pid > 0 ? event.incoming[1].color1 : 0;
  event.outgoing[3].color2=event.outgoing[3].pid > 0 ? 0                        : event.incoming[1].color2;
  event.outgoing[4].color1=event.incoming[0].color1;
  event.outgoing[4].color2=event.incoming[0].color2;
  
  //
  // 4. Set pz information for incoming particles
  //
  if (miscdata_.idir==0) {
    event.incoming[0].pz=momini_.q1[3];
    event.incoming[1].pz=momini_.q2[3];
  }
  else {
    event.incoming[0].pz=momini_.q2[3];
    event.incoming[1].pz=momini_.q1[3];
  }

  if (event.incoming[0].pz < 0) {
    std::swap(event.incoming[0],event.incoming[1]);
  }
  //
  // 5. Set px,py,pz information for outgoing particles.
  //
  std::copy(momext_.Pn+1, momext_.Pn+4,  event.outgoing[0].p);
  std::copy(momext_.Pl+1, momext_.Pl+4,  event.outgoing[1].p);
  std::copy(momext_.Pb+1, momext_.Pb+4,  event.outgoing[2].p);
  std::copy(momext_.PbX+1,momext_.PbX+4, event.outgoing[3].p);
  std::copy(momext_.Pj+1, momext_.Pj+4,  event.outgoing[4].p);
  
  //
  // 6. Write the event output on the pipe.
  // 
  if (!worldWideGridCalc) {
    size_t remaining=sizeof(ProtosEvent);
    char *ptr = (char *) &event;
    while (remaining!=0) {
      size_t bytesWrit=write(worldWidePipefd, ptr, remaining); 
      if (bytesWrit==-1) {
	perror("write");
	throw std::runtime_error("Error writing to pipe");
      }
      remaining -= bytesWrit;
      ptr += bytesWrit;
    }
  }
  //
  // Close the pipe when event processing has finished. 
  //
  if (iEvent++==worldWideNumberOfEvents) {
    close(worldWidePipefd);
    std::cout << "GOODBYE from child process " << getpid() << " Max weight was " << worldWideMaxWeight << "." << std::endl;
    exit(0);
  }
}

class ProtosController {

public:

  // Constructor:
  ProtosController(int nEvents, int seed, double wMax, int saveGrid=0);

  // Compute:
  void compute();

  // Harvest:
  const std::vector<ProtosEvent> * harvest() const;

private:

  int    nEvents;
  int    seed;
  double wMax;
  int    saveGrid;
  std::vector<ProtosEvent> protosEvent;

  // Ceci n'est pas une pipe:
  int pipefd[2];

  
};

//=====Constructor:
ProtosController::ProtosController(int nEvents, int seed, double wMax, int saveGrid): nEvents(nEvents),seed(seed),wMax(wMax),saveGrid(saveGrid),protosEvent(nEvents){
  if (pipe(pipefd)==-1) {
    perror("pipe");
    exit(0);
  }
};

//=====Compute:
void ProtosController::compute() {
  worldWideNumberOfEvents=nEvents;

  // Don't wait for child to finish. 
  signal(SIGCHLD,SIG_IGN);

  // Fork subprocess:
  pid_t child = fork();
  
  if (child==0 )  {// Child writes to the pipe:
    close(pipefd[0]);
    worldWidePipefd=pipefd[1];
    worldWideWeightCieling=wMax;
    worldWideGridCalc=saveGrid;
    protos_(seed,saveGrid);     // call protos now:
  }
  else { // parent reads from pipe:
    close (pipefd[1]);
    for (int i=0;i<nEvents;i++) {
      ProtosEvent event;
      if (!saveGrid) {
	size_t remaining=sizeof(ProtosEvent);
	char *ptr = (char *) &event;
	while (remaining!=0) {
	  size_t bytesRead=read(pipefd[0],&event,remaining);
	  if (bytesRead==-1) {
	    perror("read");
	    throw std::runtime_error("Error reading from pipe");
	  }
	  remaining -= bytesRead;
	  ptr += bytesRead;
	}
      }
      protosEvent[i]=event;
    }
    wait(NULL);
    close(pipefd[0]);
  }
  
  
}

//=====Harvest:
const std::vector<ProtosEvent> *ProtosController::harvest() const{
  return &protosEvent;
}



int main(int argc, char **argv) {

  const std::string argv0 = argv[0];
  const std::string usage = "usage: " + argv0 + " -o outputFile [-n value] [-s] [-wMax value]";

  //
  // Steering parameters:
  //
  int         nEvents=1000;
  int         saveGrid=0;
  double      wMax    =0;
  std::string outputFile="/dev/null";
  
   
  for (int i=1;i<argc; i++) {
    if (argv[i]==std::string("-?")) {
      std::cerr << usage << std::endl;
      std::cerr << "-n     Number of Events  " << std::endl;
      std::cerr << "-wMax  Maximum Weight    " << std::endl;
      std::cerr << "-o     Output file name  " << std::endl;
      std::cerr << "-s     To switch on save of grid" << std::endl;
      exit(0);
    }
    else if (argv[i]==std::string("-s")) {
      saveGrid=1;
      std::copy(argv+i+1,argv+argc, argv+i);
      argc-=1;
      i-=1;
    }
    else if (argv[i]==std::string("-o")) {
      if (i+1<argc) {
	outputFile=argv[i+1];
	std::copy(argv+i+1,argv+argc, argv+i-1);
	argc-=2;
	i-=2;
      }
      else {
	std::cerr << usage << std::endl;
	exit(0);
      }
    }
    else if (argv[i]==std::string("-wMax")) {
      if (i+1<argc) {
	wMax=atof(argv[i+1]);
	std::copy(argv+i+1,argv+argc, argv+i-1);
	argc-=2;
	i-=2;
      }
      else {
	std::cerr << usage << std::endl;
	exit(0);
      }
    }
    else if (argv[i]==std::string("-n")) {
      if (i+1<argc) {
	nEvents=atoi(argv[i+1]);
	std::copy(argv+i+1,argv+argc, argv+i-1);
	argc-=2;
	i-=2;
      }
      else {
	std::cerr << usage << std::endl;
	exit(0);
      }
    }
      
  }

  size_t NPROCESSORS = saveGrid ? 1 : std::thread::hardware_concurrency();

  std::cout << "JFB PROTOS running on " << NPROCESSORS << " processors. steering parameters:"     << std::endl;
  std::cout << "Grid is " << (saveGrid ? "saved to output file" : "restored from input file")     << std::endl;
  std::cout << "Generating " << nEvents << ( wMax==0 ? " WEIGHTED events" : " UNWEIGHTED Events") << std::endl; 
  std::cout << "Output written to file: " << outputFile << std::endl;

  
  
  std::vector<ProtosController*> controllerSet;
  std::vector<std::thread>      threadSet;
  std::random_device dev;
  for (size_t i=0; i<NPROCESSORS; i++) {
    int seed=dev();
    controllerSet.push_back(new ProtosController(nEvents,seed,wMax,saveGrid));
    threadSet.push_back(std::thread(&ProtosController::compute,controllerSet.back()));
  }

  for (size_t i=0; i<NPROCESSORS; i++) {
    threadSet[i].join();
  }

  std::ofstream outputStream(outputFile);
  
  for (size_t i=0; i<NPROCESSORS; i++) {
    const std::vector<ProtosEvent> *event=controllerSet[i]->harvest();
    for (auto e : *event) {
      if (wMax!=0.0) e.weight=1.0;
      outputStream.write((char *) &e,sizeof(ProtosEvent));
    }
  }

  return 0;
}
