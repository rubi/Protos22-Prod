      SUBROUTINE UNW_Red(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM

!     Parameters in .par file

      INTEGER NCOUP
      INTEGER IPROC,idum0,NIN,NOUT,IMATCH
      REAL*8 ET
      REAL*8 mt,Gt,mb,MH
      REAL*8 XMAXUP,SIG_t,ERR_t,SIG_tbar,ERR_tbar,MCUT
      REAL*8 coup(4),coup2(4),coupI(4),coup2I(4)

!     Data reconstruction

      INTEGER NPART,IDUP(10),ICOL1(10),ICOL2(10)
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(10,0:3)
      INTEGER NUMEVT,ndum

!     Dummy

      INTEGER i

!     -------------------------------------

      NCOUP=4
      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      IF (IPROC .EQ. 3) THEN          ! tj
        NPART=6
      ELSE IF (IPROC .EQ. 4) THEN     ! tbj
        NPART=7
      ELSE IF (IPROC .EQ. 5) THEN     ! tb
        NPART=6
      ELSE IF (IPROC .EQ. 6) THEN     ! tW
        NPART=7
      ELSE IF (IPROC .EQ. 7) THEN     ! tWb
        NPART=8
      ELSE IF (IPROC .EQ. 8) THEN     ! tt
        NPART=8
      ELSE
        PRINT *,'Wrong process in UNW_Red'
        STOP
      ENDIF

      READ (36,*) ET
      READ (36,*) mt,Gt,mb,MH

      IF ((IPROC .EQ. 3) .OR. (IPROC .EQ. 4) .OR. (IPROC .EQ. 7))
     &  READ (36,*) IMATCH,MCUT
      IF (IPROC .EQ. 6) READ (36,*) IMATCH

      READ (36,*) (COUP(i),i=1,NCOUP)
      READ (36,*) (COUPI(i),i=1,NCOUP)
      IF (IPROC .EQ. 8) READ (36,*) (COUP2(i),i=1,NCOUP)
      IF (IPROC .EQ. 8) READ (36,*) (COUP2I(i),i=1,NCOUP)
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG_t,ERR_t
      IF (IPROC .NE. 8) READ (36,*) SIG_tbar,ERR_tbar
      CLOSE(36)

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10
      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH

      IF ((IPROC .EQ. 3) .OR. (IPROC .EQ. 4) .OR. (IPROC .EQ. 7))
     &  WRITE (36,4006) IMATCH,MCUT
      IF (IPROC .EQ. 6) WRITE (36,4007) IMATCH

      IF (IPROC .NE. 8) THEN
        WRITE (36,4010) (COUP(i),i=1,NCOUP)
        WRITE (36,4011) (COUPI(i),i=1,NCOUP)
      ELSE
        WRITE (36,4012) (COUP(i),i=1,NCOUP)
        WRITE (36,4013) (COUPI(i),i=1,NCOUP)
        WRITE (36,4014) (COUP2(i),i=1,NCOUP)
        WRITE (36,4015) (COUP2I(i),i=1,NCOUP)
      ENDIF

      WRITE (36,4018) idum0
      WRITE (36,4020) NUMEVT

      IF (IPROC .NE. 8) THEN
        WRITE (36,4040) SIG_t,ERR_t
        WRITE (36,4050) SIG_tbar,ERR_tbar
      ELSE
        WRITE (36,4041) SIG_t,ERR_t
      ENDIF

      CLOSE (36)
      RETURN
      
3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                        ',
     & '       ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (I1,'  ',F5.1,'                         ! Matching')
4007  FORMAT (I1,'                                ! Matching')
4010  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Re VL, Re VR, Re gL, Re gR')
4011  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Im VL, Im VR, Im gL, Im gR')
4012  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Re VL, Re VR, Re gL, Re gR (lep)')
4013  FORMAT (F6.1,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Clqp, Im VR, Im gL, Im gR (lep)')
4014  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Re VL, Re VR, Re gL, Re gR (had)')
4015  FORMAT (F6.1,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Cqqp, Im VR, Im gL, Im gR (had)')
4018  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I8,
     &  '                         ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! t cross section and error')
4041  FORMAT (D12.6,'   ',D12.6,
     & '      ! tt cross section and error')
4050  FORMAT (D12.6,'   ',D12.6,
     & '      ! tbar cross section and error')
      END



      SUBROUTINE UNW_White(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM
        
!     Parameters in .par file

      INTEGER NCOUP,IQFLAV
      INTEGER IPROC,idum0,NIN,NOUT
      REAL*8 ET
      REAL*8 mt,Gt,mb,MH
      REAL*8 XMAXUP,SIG_t,ERR_t,SIG_tbar,ERR_tbar
      REAL*8 coup(4)

!     Data reconstruction

      INTEGER NPART,IDUP(10),ICOL1(10),ICOL2(10)
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(10,0:3)
      INTEGER NUMEVT,ndum

!     Dummy

      INTEGER i

!     -------------------------------------

      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      IF (IPROC .EQ. 9) THEN          ! Zqt
        NPART=8
        NCOUP=4
      ELSE IF (IPROC .EQ. 10) THEN    ! Zt
        NPART=7
        NCOUP=4
      ELSE IF (IPROC .EQ. 11) THEN    ! Aqt
        NPART=7
        NCOUP=2
      ELSE IF (IPROC .EQ. 12) THEN    ! At
        NPART=6
        NCOUP=2
      ELSE IF (IPROC .EQ. 13) THEN    ! gqt
        NPART=7
        NCOUP=2
      ELSE IF (IPROC .EQ. 14) THEN    ! t
        NPART=5
        NCOUP=2
      ELSE IF (IPROC .EQ. 15) THEN    ! Hqt
        NPART=7
        NCOUP=2
      ELSE IF (IPROC .EQ. 16) THEN    ! Ht
        NPART=6
        NCOUP=2
      ELSE
        PRINT *,'Wrong process in UNW_White'
        STOP
      ENDIF

      READ (36,*) ET
      READ (36,*) mt,Gt,mb,MH
      READ (36,*) IQFLAV
      READ (36,*) (COUP(i),i=1,NCOUP)
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP

      READ (36,*) SIG_t,ERR_t
      IF ( (IPROC .EQ. 10) .OR. (IPROC .EQ. 12) .OR. (IPROC .EQ. 14)
     &  .OR. (IPROC .EQ. 16)) READ (36,*) SIG_tbar,ERR_tbar

      CLOSE(36)

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10
      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4009) IQFLAV

      IF ((IPROC .EQ. 9) .OR. (IPROC .EQ. 10)) THEN
        WRITE (36,4013) (coup(i),i=1,NCOUP)
      ELSE IF ((IPROC .EQ. 11) .OR. (IPROC .EQ. 12)) THEN
        WRITE (36,4014) (coup(i),i=1,NCOUP)
      ELSE IF ((IPROC .EQ. 13) .OR. (IPROC .EQ. 14)) THEN
        WRITE (36,4015) (coup(i),i=1,NCOUP)
      ELSE IF ((IPROC .EQ. 15) .OR. (IPROC .EQ. 16)) THEN
        WRITE (36,4016) (coup(i),i=1,NCOUP)
      ENDIF

      WRITE (36,4019) idum0
      WRITE (36,4020) NUMEVT

      IF ((IPROC .EQ. 10) .OR. (IPROC .EQ. 12) .OR. (IPROC .EQ. 14)
     &  .OR. (IPROC .EQ. 16)) THEN
        WRITE (36,4040) SIG_t,ERR_t
        WRITE (36,4050) SIG_tbar,ERR_tbar
      ELSE
        WRITE (36,4041) SIG_t,ERR_t
      ENDIF

      CLOSE (36)
      RETURN
3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)

4000  FORMAT (I2,'                        ',
     & '       ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4009  FORMAT (I1,
     &  '                                ! Light quark flavour')
4013  FORMAT (F6.4,'  ',F6.4,'  ',F6.4,'  ',F6.4,
     &  '   ! XL, XR, kL, kR')
4014  FORMAT (F6.4,'  ',F6.4,
     &  '                   ! laL, laR')
4015  FORMAT (F6.4,'  ',F6.4,
     &  '                   ! zL, zR')
4016  FORMAT (F6.4,'  ',F6.4,
     &  '                   ! etaL, etaR')
4019  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I8,
     &  '                         ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! t cross section and error')
4041  FORMAT (D12.6,'   ',D12.6,
     & '      ! tt cross section and error')
4050  FORMAT (D12.6,'   ',D12.6,
     & '      ! tbar cross section and error')      
      END







      SUBROUTINE UNW_2HL(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM

!     Parameters in .par file

      INTEGER IPROC,idum0,NIN,NOUT
      REAL*8 ET
      REAL*8 MZP,GZP
      INTEGER IZPMOD,IMA
      REAL*8 mL,GE,GN,MH,VlN(3)
      REAL*8 XMAXUP,SIG,ERR

!     Data reconstruction

      INTEGER NPART,NPART0,IDUP(10),ICOL1(10),ICOL2(10)
      INTEGER IDB1,IDB2
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(10,0:3)
      INTEGER NUMEVT,ndum

!     Statistics and checks

      INTEGER IB1,IB2
      INTEGER POS1,POS2,IDL1,IDL2,IL1,IL2
      INTEGER CHL1,CHL2,CHB1,CHB2,CHF1,CHF2,CHini
      REAL*8 countB(3,3),countL(3,3),ncount

!     Dummy

      INTEGER i,j

!     -------------------------------------

      NPART0=8

!     Clear arrays

      DO i=1,3
        DO j=1,3
          countB(i,j)=0d0
          countL(i,j)=0d0
        ENDDO
      ENDDO
      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      READ (36,*) ET

      IF ((IPROC .GE. 34) .AND. (IPROC .LE. 36))
     &  READ (36,*) MZp,GZp,IZPMOD,IMA

      READ (36,*) mL,GE,GN,MH
      READ (36,*) (VlN(i),i=1,3)
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG,ERR
      CLOSE(36)

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      NPART=NPART0
      IF ((IPROC .GE. 31) .AND. (IPROC .LE. 36)) THEN
        READ (36,*) IDB1,IDB2
        IF (IDB1 .EQ. 25) NPART=NPART-1
        IF (IDB2 .EQ. 25) NPART=NPART-1
      ELSE
        STOP
      ENDIF

      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

!     Checks

      POS1=3
      POS2=6
      IF (IDB1 .EQ. 25) POS2=5

      IF (ABS(IDB1) .EQ. 24) THEN
        IB1=1
      ELSE IF (IDB1 .EQ. 23) THEN
        IB1=2
      ELSE IF (IDB1 .EQ. 25) THEN
        IB1=3
      ELSE
        PRINT *,'Error: IDB1 = ',IDB1
        STOP
      ENDIF

      IF (ABS(IDB2) .EQ. 24) THEN
        IB2=1
      ELSE IF (IDB2 .EQ. 23) THEN
        IB2=2
      ELSE IF (IDB2 .EQ. 25) THEN
        IB2=3
      ELSE
        PRINT *,'Error: IDB2 = ',IDB2
        STOP
      ENDIF

      IDL1=IDUP(POS1)
      IDL2=IDUP(POS2)

      IF ((ABS(IDL1) .EQ. 11) .OR. (ABS(IDL1) .EQ. 12)) THEN
        IL1=1
      ELSE IF ((ABS(IDL1) .EQ. 13) .OR. (ABS(IDL1) .EQ. 14)) THEN
        IL1=2
      ELSE IF ((ABS(IDL1) .EQ. 15) .OR. (ABS(IDL1) .EQ. 16)) THEN
        IL1=3
      ELSE
        PRINT *,'Error: IDL1 = ',IDL1
        STOP
      ENDIF

      IF ((ABS(IDL2) .EQ. 11) .OR. (ABS(IDL2) .EQ. 12)) THEN
        IL2=1
      ELSE IF ((ABS(IDL2) .EQ. 13) .OR. (ABS(IDL2) .EQ. 14)) THEN
        IL2=2
      ELSE IF ((ABS(IDL2) .EQ. 15) .OR. (ABS(IDL2) .EQ. 16)) THEN
        IL2=3
      ELSE
        PRINT *,'Error: IDL2 = ',IDL2
        STOP
      ENDIF

      CHL1=-SIGN(1d0,IDL1)
      IF ( (ABS(IDL1) .EQ. 12) .OR. (ABS(IDL1) .EQ. 14)
     &  .OR. (ABS(IDL1) .EQ. 16)) CHL1=0
      CHL2=-SIGN(1d0,IDL2)
      IF ( (ABS(IDL2) .EQ. 12) .OR. (ABS(IDL2) .EQ. 14)
     &  .OR. (ABS(IDL2) .EQ. 16)) CHL2=0

      IF (IB1 .NE. 3) THEN
        CHB1=IDUP(POS1+1)+IDUP(POS1+2)
      ELSE
        CHB1=0
      ENDIF

      IF (IB2 .NE. 3) THEN
        CHB2=IDUP(POS2+1)+IDUP(POS2+2)
      ELSE
        CHB2=0
      ENDIF

      IF ( ((IDB1 .EQ. 23) .AND. (CHB1 .NE. 0))
     &  .OR. ((IDB1 .EQ. 24) .AND. (CHB1 .NE. 1))
     &  .OR. ((IDB1 .EQ. -24) .AND. (CHB1 .NE. -1)) ) THEN
        PRINT *,'Wrong charge for B1 = ',CHB1
        STOP
      ENDIF

      IF ( ((IDB2 .EQ. 23) .AND. (CHB2 .NE. 0))
     &  .OR. ((IDB2 .EQ. 24) .AND. (CHB2 .NE. 1))
     &  .OR. ((IDB2 .EQ. -24) .AND. (CHB2 .NE. -1)) ) THEN
        PRINT *,'Wrong charge for B2 = ',CHB2
        STOP
      ENDIF

      CHF1=CHL1+CHB1
      CHF2=CHL2+CHB2

      IF ((IPROC. EQ. 31) .OR. (IPROC. EQ. 34)) THEN
        IF (CHF1 .NE. 1) THEN
          PRINT *,'Wrong charge for F1 = ',CHF1
          STOP
        ENDIF
        IF (CHF2 .NE. -1) THEN
          PRINT *,'Wrong charge for F2 = ',CHF2
          STOP
        ENDIF
      ELSE IF ((IPROC .EQ. 32) .OR. (IPROC. EQ. 35)) THEN
        IF (ABS(CHF1) .NE. 1) THEN
          PRINT *,'Wrong charge for F1 = ',CHF1
          STOP
        ENDIF
        IF (CHF2 .NE. 0) THEN
          PRINT *,'Wrong charge for F2 = ',CHF2
          STOP
        ENDIF
      ELSE IF ((IPROC .EQ. 33) .OR. (IPROC. EQ. 36)) THEN
        IF (ABS(CHF1) .NE. 0) THEN
          PRINT *,'Wrong charge for F1 = ',CHF1
          STOP
        ENDIF
        IF (CHF2 .NE. 0) THEN
          PRINT *,'Wrong charge for F2 = ',CHF2
          STOP
        ENDIF
      ELSE
        STOP
      ENDIF

      CHini=IDUP(1)+IDUP(2)
      IF (CHini .NE. CHF1+CHF2) THEN
        PRINT *,'Wrong total charge = ',CHF1+CHF2,' ini = ',CHINI
        STOP
      ENDIF
      
!     --------------- All checks done! ---------------

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10

      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      countB(IB1,IB2)=countB(IB1,IB2)+1d0
      countL(IL1,IL2)=countL(IL1,IL2)+1d0

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      IF ((IPROC .GE. 31) .AND. (IPROC .LE. 36)) THEN
        WRITE (37,3015) IDB1,IDB2
      ELSE
        STOP
      ENDIF
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET

      IF ((IPROC .GE. 34) .AND. (IPROC .LE. 36))
     &  WRITE (36,4004) MZp,GZp,IZPMOD,IMA

      WRITE (36,4005) mL,GE,GN,MH
      IF (GN .NE. 0d0) THEN
      WRITE (36,4006) VlN(1),VlN(2),VlN(3)
      ELSE
      WRITE (36,4007) VlN(1),VlN(2),VlN(3)
      ENDIF
      WRITE (36,4018) idum0
      WRITE (36,4020) NUMEVT
      WRITE (36,4040) SIG,ERR
      CLOSE (36)

!     Write statistics

      ncount=0d0
      DO i=1,3
        DO j=1,3
          ncount=ncount+countB(i,j)
        ENDDO
      ENDDO
      PRINT 999
      PRINT 1300
      PRINT 999
      PRINT 1301
      PRINT 1302,(countB(1,i)/ncount,i=1,3)
      PRINT 1303,(countB(2,i)/ncount,i=1,3)
      PRINT 1304,(countB(3,i)/ncount,i=1,3)
      ncount=0
      DO i=1,3
        DO j=1,3
          ncount=ncount+countL(i,j)
        ENDDO
      ENDDO
      PRINT 999
      PRINT 1305
      PRINT 999
      PRINT 1306
      PRINT 1307,(countL(1,i)/ncount,i=1,3)
      PRINT 1308,(countL(2,i)/ncount,i=1,3)
      PRINT 1309,(countL(3,i)/ncount,i=1,3)
      PRINT 999

      RETURN

999   FORMAT ('')
1300  FORMAT ('Fraction of boson decays in unweighted sample')
1301  FORMAT (' F1\\F2   W       Z       H')
1302  FORMAT ('  W   ',F6.4,'  ',F6.4,'  ',F6.4)
1303  FORMAT ('  Z   ',F6.4,'  ',F6.4,'  ',F6.4)
1304  FORMAT ('  H   ',F6.4,'  ',F6.4,'  ',F6.4)
1305  FORMAT ('Fraction of lepton flavours in unweighted sample')
1306  FORMAT (' F1\\F2   e       mu     tau')
1307  FORMAT (' e    ',F6.4,'  ',F6.4,'  ',F6.4)
1308  FORMAT (' mu   ',F6.4,'  ',F6.4,'  ',F6.4)
1309  FORMAT (' tau  ',F6.4,'  ',F6.4,'  ',F6.4)

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3015  FORMAT (I3,' ',I3)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)

4000  FORMAT (I2,'                        ',
     & '            ! Process code')
4001  FORMAT (F6.0,'                                ! CM energy')
4004  FORMAT (F6.1,' ',F5.2,'  ',I1,'  ',I1,
     &  '                    ! MZp, GZp, IZPMOD, IMA')
4005  FORMAT (F6.1,'  ',D9.3,'  ',D9.3,'  ',F6.2,
     &  '  ! ML, GE, GN, MH')
4006  FORMAT (D9.3,'  ',D9.3,'  ',D9.3,
     &  '       ! VeN, VmN, VtN')
4007  FORMAT (D9.3,'  ',D9.3,'  ',D9.3,
     &  '       ! VE1, VE2, VE3')
4009  FORMAT (I1,
     &  '                                ! Light quark flavour')
4018  FORMAT (I5,
     &  '                                 ! Initial random seed')
4020  FORMAT (I8,
     &  '                              ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '           ! total cross section and error')

      END




      SUBROUTINE UNW_1HL(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM

!     Parameters in .par file

      INTEGER IPROC,idum0,NIN,NOUT
      REAL*8 ET
      REAL*8 MWP,GWP
      INTEGER IWPMOD,IMA
      REAL*8 mL,GE,GN,MH,VL(3),VR(3)
      REAL*8 XMAXUP,SIG,ERR

!     Data reconstruction

      INTEGER NPART,NPART0,IDUP(10),ICOL1(10),ICOL2(10)
      INTEGER IDB1
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(10,0:3)
      INTEGER NUMEVT,ndum

!     Statistics and checks

      INTEGER IB1
      INTEGER POS1,POS2,IDL1,IDL2,IL1,IL2
      INTEGER CHL1,CHL2,CHB1,CHF1,CHF2,CHini
      REAL*8 countB(5),countP(3),countL(3),countR(3),ncount

!     Dummy

      INTEGER i

!     -------------------------------------

      NPART0=6

!     Clear arrays

      DO i=1,3
        countB(i)=0d0
        countP(i)=0d0
        countL(i)=0d0
        countR(i)=0d0
      ENDDO
      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      READ (36,*) ET

      READ (36,*) MWp,GWp,IWPMOD,IMA
      READ (36,*) mL,GE,GN,MH
      READ (36,*) (VL(i),i=1,3)
      READ (36,*) (VR(i),i=1,3)
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG,ERR
      CLOSE(36)

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      NPART=NPART0
      READ (36,*) IDB1
      IF (IDB1 .EQ. 25) NPART=NPART-1
      IF (ABS(IDB1) .EQ. 134) NPART=NPART+2

      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

!     Checks

      POS1=4
      POS2=3

      IF (ABS(IDB1) .EQ. 24) THEN
        IB1=1
      ELSE IF (IDB1 .EQ. 23) THEN
        IB1=2
      ELSE IF (IDB1 .EQ. 25) THEN
        IB1=3
      ELSE IF (ABS(IDB1) .EQ. 34) THEN
        IB1=4
      ELSE IF (ABS(IDB1) .EQ. 134) THEN
        IB1=5
      ELSE
        PRINT *,'Error: IDB1 = ',IDB1
        STOP
      ENDIF

      IDL1=IDUP(POS1)
      IDL2=IDUP(POS2)

      IF ((ABS(IDL1) .EQ. 11) .OR. (ABS(IDL1) .EQ. 12)) THEN
        IL1=1
      ELSE IF ((ABS(IDL1) .EQ. 13) .OR. (ABS(IDL1) .EQ. 14)) THEN
        IL1=2
      ELSE IF ((ABS(IDL1) .EQ. 15) .OR. (ABS(IDL1) .EQ. 16)) THEN
        IL1=3
      ELSE
        PRINT *,'Error: IDL1 = ',IDL1
        STOP
      ENDIF

      IF ((ABS(IDL2) .EQ. 11) .OR. (ABS(IDL2) .EQ. 12)) THEN
        IL2=1
      ELSE IF ((ABS(IDL2) .EQ. 13) .OR. (ABS(IDL2) .EQ. 14)) THEN
        IL2=2
      ELSE IF ((ABS(IDL2) .EQ. 15) .OR. (ABS(IDL2) .EQ. 16)) THEN
        IL2=3
      ELSE
        PRINT *,'Error: IDL2 = ',IDL2
        STOP
      ENDIF

      CHL1=-SIGN(1d0,IDL1)
      IF ( (ABS(IDL1) .EQ. 12) .OR. (ABS(IDL1) .EQ. 14)
     &  .OR. (ABS(IDL1) .EQ. 16)) CHL1=0
      CHL2=-SIGN(1d0,IDL2)
      IF ( (ABS(IDL2) .EQ. 12) .OR. (ABS(IDL2) .EQ. 14)
     &  .OR. (ABS(IDL2) .EQ. 16)) CHL2=0

      IF (IB1 .EQ. 3) THEN
        CHB1=0
      ELSE IF (IB1 .EQ. 5) THEN
        CHB1=IDUP(POS1+1)+IDUP(POS1+2)+IDUP(POS1+3)+IDUP(POS1+4)
      ELSE
        CHB1=IDUP(POS1+1)+IDUP(POS1+2)
      ENDIF

      IF ( ((IDB1 .EQ. 23) .AND. (CHB1 .NE. 0))
     &  .OR. ((IDB1 .EQ. 24) .AND. (CHB1 .NE. 1))
     &  .OR. ((IDB1 .EQ. -24) .AND. (CHB1 .NE. -1))
     &  .OR. ((IDB1 .EQ. 34) .AND. (CHB1 .NE. 1))
     &  .OR. ((IDB1 .EQ. -34) .AND. (CHB1 .NE. -1))
     &  .OR. ((IDB1 .EQ. 134) .AND. (CHB1 .NE. 1))
     &  .OR. ((IDB1 .EQ. -134) .AND. (CHB1 .NE. -1)) ) THEN
        PRINT *,'Wrong charge for B1 = ',CHB1
        STOP
      ENDIF

      CHF1=CHL1+CHB1
      CHF2=CHL2

      IF (IPROC. EQ. 38) THEN
        IF (CHF1 .NE. 0) THEN
          PRINT *,'Wrong charge for F1 = ',CHF1
          STOP
        ENDIF
        IF (ABS(CHF2) .NE. 1) THEN
          PRINT *,'Wrong charge for F2 = ',CHF2
          STOP
        ENDIF
      ELSE
        STOP
      ENDIF

      CHini=IDUP(1)+IDUP(2)
      IF (CHini .NE. CHF1+CHF2) THEN
        PRINT *,'Wrong total charge = ',CHF1+CHF2,' ini = ',CHINI
        STOP
      ENDIF
      
!     --------------- All checks done! ---------------

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10

      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      countB(IB1)=countB(IB1)+1d0
      countP(IL2)=countP(IL2)+1d0
      IF (IB1 .LE. 3) THEN
      countL(IL1)=countL(IL1)+1d0
      ELSE
      countR(IL1)=countR(IL1)+1d0
      ENDIF

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3015) IDB1
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4004) MWp,GWp,IWPMOD,IMA

      WRITE (36,4005) mL,GE,GN,MH
      WRITE (36,4006) VL(1),VL(2),VL(3)
      WRITE (36,4007) VR(1),VR(2),VR(3)
      WRITE (36,4018) idum0
      WRITE (36,4020) NUMEVT
      WRITE (36,4040) SIG,ERR
      CLOSE (36)

!     Write statistics

      ncount=0d0
      DO i=1,5
        ncount=ncount+countB(i)
      ENDDO
      PRINT 999
      PRINT 1300
      PRINT 999
      PRINT 1301
      PRINT 1302,(countB(i)/ncount,i=1,5)
      PRINT 999

      ncount=0
      DO i=1,3
        ncount=ncount+countP(i)
      ENDDO
      PRINT 999
      PRINT 1304
      PRINT 999
      PRINT 1307
      PRINT 1302,(countP(i)/ncount,i=1,3)
      PRINT 999

      ncount=0
      DO i=1,3
        ncount=ncount+countL(i)
      ENDDO
      PRINT 999
      PRINT 1305
      PRINT 999
      PRINT 1307
      PRINT 1302,(countL(i)/ncount,i=1,3)
      PRINT 999

      ncount=0
      DO i=1,3
        ncount=ncount+countR(i)
      ENDDO
      PRINT 999
      PRINT 1306
      PRINT 999
      PRINT 1307
      PRINT 1302,(countR(i)/ncount,i=1,3)
      PRINT 999

      PRINT 999

      RETURN

999   FORMAT ('')
1300  FORMAT ('Fraction of N decays in sample')
1301  FORMAT ('          W       Z       H     jj      tb')
1302  FORMAT ('      ',F6.4,'  ',F6.4,'  ',F6.4,'  ',F6.4,'  ',F6.4)
1304  FORMAT ('Fraction of lepton flavours for W -> lN')
1305  FORMAT ('Fraction of lepton flavours for N -> W/Z/H')
1306  FORMAT ('Fraction of lepton flavours for N -> jjl,tbl')

1307  FORMAT ('          e       mu     tau')

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3015  FORMAT (I4)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)

4000  FORMAT (I2,'                        ',
     & '            ! Process code')
4001  FORMAT (F6.0,'                                ! CM energy')
4004  FORMAT (F6.1,' ',F5.2,'  ',I1,'  ',I1,
     &  '                    ! MWp, GWp, IWPMOD, IMA')
4005  FORMAT (F6.1,'  ',D9.3,'  ',D9.3,'  ',F6.2,
     &  '  ! ML, GE, GN, MH')
4006  FORMAT (D9.3,'  ',D9.3,'  ',D9.3,
     &  '       ! VLe, VLm, VLt')
4007  FORMAT (D9.3,'  ',D9.3,'  ',D9.3,
     &  '       ! VRe, VRm, VRt')
4018  FORMAT (I5,
     &  '                                 ! Initial random seed')
4020  FORMAT (I8,
     &  '                              ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '           ! total cross section and error')

      END







      SUBROUTINE UNW_DDll(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM

!     Parameters in .par file

      INTEGER IPROC,idum0,NIN,NOUT
      REAL*8 ET
      REAL*8 MD,GD
      REAL*8 XMAXUP,SIG,ERR

!     Data reconstruction

      INTEGER NPART,IDUP(10),ICOL1(10),ICOL2(10)
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(10,0:3)
      INTEGER NUMEVT,ndum

!     Statistics and checks

      INTEGER IDL1,IDL2,IDL3,IDL4,IL1,IL2,IL3,IL4
      INTEGER CHL1,CHL2,CHL3,CHL4,CHD1,CHD2,CHini
      REAL*8 countL(3,3),ncount

!     Dummy

      INTEGER i,j

!     -------------------------------------

      NPART=6

!     Clear arrays

      DO i=1,3
        DO j=1,3
          countL(i,j)=0d0
        ENDDO
      ENDDO

      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      READ (36,*) ET
      READ (36,*) MD,GD
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG,ERR
      CLOSE(36)

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

!     Check charge

      IDL1=IDUP(3)
      IDL2=IDUP(4)
      IDL3=IDUP(5)
      IDL4=IDUP(6)

      IF ((ABS(IDL1) .EQ. 11) .OR. (ABS(IDL1) .EQ. 12)) THEN
        IL1=1
      ELSE IF ((ABS(IDL1) .EQ. 13) .OR. (ABS(IDL1) .EQ. 14)) THEN
        IL1=2
      ELSE IF ((ABS(IDL1) .EQ. 15) .OR. (ABS(IDL1) .EQ. 16)) THEN
        IL1=3
      ELSE
        PRINT *,'Error: IDL1 = ',IDL1
        STOP
      ENDIF

      IF ((ABS(IDL2) .EQ. 11) .OR. (ABS(IDL2) .EQ. 12)) THEN
        IL2=1
      ELSE IF ((ABS(IDL2) .EQ. 13) .OR. (ABS(IDL2) .EQ. 14)) THEN
        IL2=2
      ELSE IF ((ABS(IDL2) .EQ. 15) .OR. (ABS(IDL2) .EQ. 16)) THEN
        IL2=3
      ELSE
        PRINT *,'Error: IDL2 = ',IDL2
        STOP
      ENDIF

      IF ((ABS(IDL3) .EQ. 11) .OR. (ABS(IDL3) .EQ. 12)) THEN
        IL3=1
      ELSE IF ((ABS(IDL3) .EQ. 13) .OR. (ABS(IDL3) .EQ. 14)) THEN
        IL3=2
      ELSE IF ((ABS(IDL3) .EQ. 15) .OR. (ABS(IDL3) .EQ. 16)) THEN
        IL3=3
      ELSE
        PRINT *,'Error: IDL3 = ',IDL3
        STOP
      ENDIF

      IF ((ABS(IDL4) .EQ. 11) .OR. (ABS(IDL4) .EQ. 12)) THEN
        IL4=1
      ELSE IF ((ABS(IDL4) .EQ. 13) .OR. (ABS(IDL4) .EQ. 14)) THEN
        IL4=2
      ELSE IF ((ABS(IDL4) .EQ. 15) .OR. (ABS(IDL4) .EQ. 16)) THEN
        IL4=3
      ELSE
        PRINT *,'Error: IDL4 = ',IDL4
        STOP
      ENDIF

      CHL1=-SIGN(1d0,IDL1)
      IF ( (ABS(IDL1) .EQ. 12) .OR. (ABS(IDL1) .EQ. 14)
     &  .OR. (ABS(IDL1) .EQ. 16)) CHL1=0
      CHL2=-SIGN(1d0,IDL2)
      IF ( (ABS(IDL2) .EQ. 12) .OR. (ABS(IDL2) .EQ. 14)
     &  .OR. (ABS(IDL2) .EQ. 16)) CHL2=0
      CHL3=-SIGN(1d0,IDL3)
      IF ( (ABS(IDL3) .EQ. 12) .OR. (ABS(IDL3) .EQ. 14)
     &  .OR. (ABS(IDL3) .EQ. 16)) CHL3=0
      CHL4=-SIGN(1d0,IDL4)
      IF ( (ABS(IDL4) .EQ. 12) .OR. (ABS(IDL4) .EQ. 14)
     &  .OR. (ABS(IDL4) .EQ. 16)) CHL4=0

      CHD1=CHL1+CHL2
      CHD2=CHL3+CHL4
      IF (IPROC .EQ. 41) THEN
        IF ((CHD1 .NE. 2) .OR. (CHD2 .NE. -2)) THEN
          PRINT *,'Wrong charge for D++ D-- = ',CHD1,' ',CHD2
          STOP
        ENDIF
      ELSE IF (IPROC .EQ. 42) THEN
        IF ( .NOT. ((CHD1 .EQ. 2) .AND. (CHD2 .EQ. -1))
     & .AND. .NOT. ((CHD1 .EQ. 1) .AND. (CHD2 .EQ. -2)) ) THEN
          PRINT *,'Wrong charge for D++ D- = ',CHD1,' ',CHD2
          STOP
        ENDIF
      ELSE
        IF ((CHD1 .NE. 1) .OR. (CHD2 .NE. -1)) THEN
          PRINT *,'Wrong charge for D++ D-- = ',CHD1,' ',CHD2
          STOP
        ENDIF
      ENDIF

      CHini=IDUP(1)+IDUP(2)
      IF (CHini .NE. CHD1+CHD2) THEN
        PRINT *,'Wrong total charge = ',CHD1+CHD2,' ini = ',CHini
        STOP
      ENDIF

!     --------------- All checks done! ---------------

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10
      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      countL(IL1,IL2)=countL(IL1,IL2)+1d0

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4005) MD,GD
      WRITE (36,4015) idum0
      WRITE (36,4020) NUMEVT
      WRITE (36,4040) SIG,ERR
      CLOSE (36)

!     Write statistics

      ncount=0
      DO i=1,3
        DO j=1,3
          ncount=ncount+countL(i,j)
        ENDDO
      ENDDO
      PRINT 999
      PRINT 1305
      PRINT 999
      PRINT 1306
      PRINT 1307,(countL(1,i)/ncount,i=1,3)
      PRINT 1308,(countL(2,i)/ncount,i=1,3)
      PRINT 1309,(countL(3,i)/ncount,i=1,3)
      PRINT 999

      RETURN

999   FORMAT ('')
1305  FORMAT ('Fraction of lepton flavours in unweighted sample')
1306  FORMAT (' D1\\D2   e       mu     tau')
1307  FORMAT (' e    ',F6.4,'  ',F6.4,'  ',F6.4)
1308  FORMAT (' mu   ',F6.4,'  ',F6.4,'  ',F6.4)
1309  FORMAT (' tau  ',F6.4,'  ',F6.4,'  ',F6.4)
3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)

4000  FORMAT (I2,'                       ',
     & '        ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F9.6,
     &  '                ! MD, GD')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I8,
     &  '                         ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! total cross section and error')

      END








      SUBROUTINE UNW_Rose1(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM

!     Parameters in .par file

      INTEGER IPROC,idum0,NIN,NOUT,IMATCH
      REAL*8 ET
      REAL*8 mt,Gt,mb,MH,mQ,GQ,Vmix,MCUT
      REAL*8 XMAXUP,SIG_t,ERR_t,SIG_tb,ERR_tb

!     Data reconstruction

      INTEGER NPART,NPART0,NDZ,NDH
      INTEGER IDUP(12),ICOL1(12),ICOL2(12)
      INTEGER IDB1
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(12,0:3)
      INTEGER NUMEVT,ndum

!     Statistics and checks

      INTEGER IB1
      REAL*8 countB(3),ncount
      REAL*8 Psum(3)

!     Dummy

      INTEGER i

!     -------------------------------------

!     Clear arrays

      DO i=1,3
          countB(i)=0d0
      ENDDO
      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC

      READ (36,*) ET
      READ (36,*) mt,Gt,mb,MH
      READ (36,*) mQ,GQ,Vmix

      IF ((IPROC .GE. 61) .AND. (IPROC .LE. 66))
     &  READ (36,*) IMATCH,MCUT

      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG_t,ERR_t
      READ (36,*) SIG_tb,ERR_tb
      CLOSE(36)

!     Number of particles for different decays

      IF (IPROC .EQ. 61) THEN
        NPART0=6
        NDZ=2
        NDH=1
      ELSE IF (IPROC .EQ. 62) THEN
        NPART0=7
        NDZ=2
        NDH=1
      ELSE IF (IPROC .EQ. 63) THEN
        NPART0=8
        NDZ=-2
        NDH=-3
      ELSE IF (IPROC .EQ. 64) THEN
        NPART0=9
        NDZ=-2
        NDH=-3
      ELSE IF (IPROC .EQ. 65) THEN
        NPART0=6
        NDZ=0
        NDH=0
      ELSE IF (IPROC .EQ. 66) THEN
        NPART0=7
        NDZ=0
        NDH=0
      ELSE IF (IPROC .EQ. 67) THEN
        NPART0=9
        NDZ=2
        NDH=1
      ELSE IF (IPROC .EQ. 69) THEN
        NPART0=11
        NDZ=0
        NDH=0
      ENDIF 

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      NPART=NPART0
      READ (36,*) IDB1
      
      IF (IDB1 .EQ. 23) NPART=NPART+NDZ
      IF (IDB1 .EQ. 25) NPART=NPART+NDH

      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      Psum(1)=0d0
      Psum(2)=0d0
      Psum(3)=-Pz1-Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      Psum(1)=Psum(1)+P(i,1)
      Psum(2)=Psum(2)+P(i,2)
      Psum(3)=Psum(3)+P(i,3)

      ENDDO

      IF (ABS(Psum(1))+ABS(Psum(2))+ABS(Psum(3)) .GT. 1d-3) THEN
      PRINT 1401,Psum(1),Psum(2),Psum(3)
      ENDIF

!     Checks

      IF (ABS(IDB1) .EQ. 24) THEN
        IB1=1
      ELSE IF (IDB1 .EQ. 23) THEN
        IB1=2
      ELSE IF (IDB1 .EQ. 25) THEN
        IB1=3
      ELSE
        PRINT *,'Error: IDB1 = ',IDB1
        STOP
      ENDIF

!     --------------- All checks done! ---------------

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10

      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      countB(IB1)=countB(IB1)+1d0

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3016) IDB1
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH

      IF ((IPROC .EQ. 61) .OR. (IPROC .EQ. 62) .OR. (IPROC .EQ. 67))
     &  THEN
        WRITE (36,4006) mQ,GQ,Vmix
      ELSE IF ((IPROC .EQ. 63) .OR. (IPROC .EQ. 64)) THEN
        WRITE (36,4007) mQ,GQ,Vmix
      ELSE IF ((IPROC .EQ. 65) .OR. (IPROC .EQ. 66)) THEN
        WRITE (36,4009) mQ,GQ,Vmix
      ELSE IF (IPROC .EQ. 69) THEN
        WRITE (36,4008) mQ,GQ,Vmix
      ENDIF

      IF ((IPROC .GE. 61) .AND. (IPROC .LE. 66))
     &  WRITE (36,4017) IMATCH,MCUT

      WRITE (36,4018) idum0
      WRITE (36,4020) NUMEVT

      IF ((IPROC .EQ. 61) .OR. (IPROC .EQ. 62) .OR. (IPROC .EQ. 67))
     &  THEN
        WRITE (36,4040) SIG_t,ERR_t
        WRITE (36,4050) SIG_tb,ERR_tb
      ELSE IF ((IPROC .EQ. 63) .OR. (IPROC .EQ. 64)) THEN
        WRITE (36,4041) SIG_t,ERR_t
        WRITE (36,4051) SIG_tb,ERR_tb
      ELSE IF ((IPROC .EQ. 65) .OR. (IPROC .EQ. 66))  THEN
        WRITE (36,4043) SIG_t,ERR_t
        WRITE (36,4053) SIG_tb,ERR_tb
      ELSE IF (IPROC .EQ. 69) THEN
        WRITE (36,4042) SIG_t,ERR_t
        WRITE (36,4052) SIG_tb,ERR_tb
      ENDIF

      CLOSE (36)

!     Write statistics

      ncount=0
      DO i=1,3
        ncount=ncount+countB(i)
      ENDDO
      PRINT 999
      PRINT 1300
      PRINT 1301
      PRINT 1302,(countB(i)/ncount,i=1,3)
      PRINT 999

      RETURN

999   FORMAT ('')
1300  FORMAT ('Fraction of boson decays in unweighted sample')
1301  FORMAT ('          W       Z       H')
1302  FORMAT ('      ',F6.4,'  ',F6.4,'  ',F6.4)
1401  FORMAT ('Warning: P sum = ',D9.2,'  ',D9.2,'  ',D9.2)
3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3016  FORMAT (I3)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mT, GT, Vmix')
4007  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mB, GB, Vmix')
4008  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mX, GX, Vmix')
4009  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mY, GY, Vmix')
4017  FORMAT (I1,'  ',F5.1,'                         ! Matching')
4018  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I8,
     &  '                         ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! T cross section and error')
4041  FORMAT (D12.6,'   ',D12.6,
     & '      ! B cross section and error')
4042  FORMAT (D12.6,'   ',D12.6,
     & '      ! X cross section and error')
4043  FORMAT (D12.6,'   ',D12.6,
     & '      ! Y cross section and error')
4050  FORMAT (D12.6,'   ',D12.6,
     & '      ! Tbar cross section and error')
4051  FORMAT (D12.6,'   ',D12.6,
     & '      ! Bbar cross section and error')
4052  FORMAT (D12.6,'   ',D12.6,
     & '      ! Xbar cross section and error')
4053  FORMAT (D12.6,'   ',D12.6,
     & '      ! Ybar cross section and error')
      END










      SUBROUTINE UNW_Rose2(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM

!     Parameters in .par file

      INTEGER IPROC,idum0,NIN,NOUT
      REAL*8 ET
      REAL*8 mt,Gt,mb,MH,mQ,GQ,Vmix
      REAL*8 XMAXUP,SIG_t,ERR_t

!     Data reconstruction

      INTEGER NPART,NPART0,NDZ,NDH
      INTEGER IDUP(12),ICOL1(12),ICOL2(12)
      INTEGER IDB1,IDB2
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(12,0:3)
      INTEGER NUMEVT,ndum

!     Statistics and checks

      INTEGER IB1,IB2
      REAL*8 countB(3,3),ncount
      REAL*8 Psum(3)

!     Dummy

      INTEGER i,j

!     -------------------------------------


!     Clear arrays

      DO i=1,3
        DO j=1,3
          countB(i,j)=0d0
        ENDDO
      ENDDO
      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC

      READ (36,*) ET
      READ (36,*) mt,Gt,mb,MH
      READ (36,*) mQ,GQ,Vmix
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG_t,ERR_t
      CLOSE(36)

!     Number of particles for different decays

      IF (IPROC .EQ. 51) THEN
        NPART0=8
        NDZ=2
        NDH=1
      ELSE IF (IPROC .EQ. 52) THEN
        NPART0=12
        NDZ=-2
        NDH=-3
      ELSE IF (IPROC .EQ. 53) THEN
        NPART0=12
        NDZ=0
        NDH=0
      ELSE IF (IPROC .EQ. 54) THEN
        NPART0=8
        NDZ=0
        NDH=0
      ENDIF 

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      NPART=NPART0
      READ (36,*) IDB1,IDB2
      
      IF (IDB1 .EQ. 23) NPART=NPART+NDZ
      IF (IDB2 .EQ. 23) NPART=NPART+NDZ
      IF (IDB1 .EQ. 25) NPART=NPART+NDH
      IF (IDB2 .EQ. 25) NPART=NPART+NDH

      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      Psum(1)=0d0
      Psum(2)=0d0
      Psum(3)=-Pz1-Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      Psum(1)=Psum(1)+P(i,1)
      Psum(2)=Psum(2)+P(i,2)
      Psum(3)=Psum(3)+P(i,3)
      ENDDO

      IF (ABS(Psum(1))+ABS(Psum(2))+ABS(Psum(3)) .GT. 1d-3) THEN
      PRINT 1401,Psum(1),Psum(2),Psum(3)
      ENDIF

!     Checks

      IF (ABS(IDB1) .EQ. 24) THEN
        IB1=1
      ELSE IF (IDB1 .EQ. 23) THEN
        IB1=2
      ELSE IF (IDB1 .EQ. 25) THEN
        IB1=3
      ELSE
        PRINT *,'Error: IDB1 = ',IDB1
        STOP
      ENDIF

      IF (ABS(IDB2) .EQ. 24) THEN
        IB2=1
      ELSE IF (IDB2 .EQ. 23) THEN
        IB2=2
      ELSE IF (IDB2 .EQ. 25) THEN
        IB2=3
      ELSE
        PRINT *,'Error: IDB2 = ',IDB2
        STOP
      ENDIF

!     --------------- All checks done! ---------------

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10

      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'

      countB(IB1,IB2)=countB(IB1,IB2)+1d0

      WRITE(37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3015) IDB1,IDB2
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH

      IF (IPROC .EQ. 51) THEN
        WRITE (36,4006) mQ,GQ,Vmix
      ELSE IF (IPROC .EQ. 52) THEN
        WRITE (36,4007) mQ,GQ,Vmix
      ELSE IF (IPROC .EQ. 53) THEN
        WRITE (36,4008) mQ,GQ,Vmix
      ELSE IF (IPROC .EQ. 54) THEN
        WRITE (36,4009) mQ,GQ,Vmix
      ENDIF

      WRITE (36,4018) idum0
      WRITE (36,4020) NUMEVT

      IF (IPROC .EQ. 51) THEN
        WRITE (36,4040) SIG_t,ERR_t
      ELSE IF (IPROC .EQ. 52) THEN
        WRITE (36,4041) SIG_t,ERR_t
      ELSE IF (IPROC .EQ. 53) THEN
        WRITE (36,4042) SIG_t,ERR_t
      ELSE IF (IPROC .EQ. 54) THEN
        WRITE (36,4043) SIG_t,ERR_t
      ENDIF

      CLOSE (36)

!     Write statistics

      ncount=0d0
      DO i=1,3
        DO j=1,3
          ncount=ncount+countB(i,j)
        ENDDO
      ENDDO
      PRINT 999
      PRINT 1300
      PRINT 999
      PRINT 1301
      PRINT 1302,(countB(1,i)/ncount,i=1,3)
      PRINT 1303,(countB(2,i)/ncount,i=1,3)
      PRINT 1304,(countB(3,i)/ncount,i=1,3)

      RETURN

999   FORMAT ('')
1300  FORMAT ('Fraction of boson decays in unweighted sample')
1301  FORMAT (' F1\\F2   W       Z       H')
1302  FORMAT ('  W   ',F6.4,'  ',F6.4,'  ',F6.4)
1303  FORMAT ('  Z   ',F6.4,'  ',F6.4,'  ',F6.4)
1304  FORMAT ('  H   ',F6.4,'  ',F6.4,'  ',F6.4)
1401  FORMAT ('Warning: P sum = ',D9.2,'  ',D9.2,'  ',D9.2)
3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3015  FORMAT (I3,' ',I3)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                        ',
     & '       ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mT, GT, Vmix')
4007  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mB, GB, Vmix')
4008  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mX, GX, Vmix')
4009  FORMAT (F6.2,'  ',F6.3,'  ',F6.4,
     &  '           ! mY, GY, Vmix')
4018  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I8,
     &  '                         ! unweighted events')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! TT cross section and error')
4041  FORMAT (D12.6,'   ',D12.6,
     & '      ! BB cross section and error')
4042  FORMAT (D12.6,'   ',D12.6,
     & '      ! XX cross section and error')
4043  FORMAT (D12.6,'   ',D12.6,
     & '      ! YY cross section and error')
      END


      SUBROUTINE UNW_4F(NUMEVT)
      IMPLICIT NONE

!     Files to unweight

      CHARACTER*100 FILE0i,FILE0o,FILE1i,FILE1o
      COMMON /IOfiles/ FILE0i,FILE0o,FILE1i,FILE1o

!     External

      REAL*8 ran2
      INTEGER IDUM
        
!     Parameters in .par file

      INTEGER NCOUP,IQFLAV,IQFLAV2,ILEP2,ILEP3
      INTEGER IPROC,idum0,NIN,NOUT
      REAL*8 ET
      REAL*8 mt,Gt,mb,MH
      REAL*8 XMAXUP,SIG_t,ERR_t
      REAL*8 coup(4),coup2(4)

!     Data reconstruction

      INTEGER NPART,IDUP(10),ICOL1(10),ICOL2(10)
      REAL*8 XWGTUP,Q
      REAL*8 Pz1,Pz2
      REAL*8 P(10,0:3)
      INTEGER NUMEVT,ndum

!     Dummy

      INTEGER i

!     -------------------------------------

      NUMEVT=0

      OPEN (36,file=FILE0i,status='old')
      READ (36,*) IPROC
      IF ((IPROC .EQ. 17) .OR. (IPROC .EQ. 18)) THEN         ! llqt, ttlike
        NPART=8
        NCOUP=4
      ELSE
        PRINT *,'Wrong process in UNW_4F'
        STOP
      ENDIF

      READ (36,*) ET
      READ (36,*) mt,Gt,mb,MH
      IF (IPROC .EQ. 17) THEN
      READ (36,*) IQFLAV,ILEP2,ILEP3
      ELSE IF (IPROC .EQ. 18) THEN
      READ (36,*) IQFLAV,IQFLAV2
      ENDIF
      READ (36,*) (COUP(i),i=1,NCOUP)
      IF (IPROC .EQ. 17) THEN
      READ (36,*) (COUP2(i),i=1,NCOUP)
      ELSE
      READ (36,*) (COUP2(i),i=1,2)
      ENDIF
      READ (36,*) idum0
      READ (36,*) NIN,NOUT
      READ (36,*) XMAXUP
      READ (36,*) SIG_t,ERR_t

      CLOSE(36)

!     Begin unweighting

      OPEN (36,file=FILE1i,status='old')
      OPEN (37,file=FILE1o,status='unknown')

10    READ (36,3010,END=100) ndum,XWGTUP,Q
      READ (36,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      READ (36,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      READ (36,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      IF (ran2(idum) .GT. ABS(XWGTUP)/XMAXUP) GOTO 10
      NUMEVT = NUMEVT + 1
      IF (MOD(NUMEVT,10000) .EQ. 0) 
     &   PRINT *,'Obtained ',NUMEVT,' events'
      IF (NUMEVT .GT. 10000) GOTO 100

      WRITE (37,3010) NUMEVT,SIGN(1d0,XWGTUP),Q
      WRITE (37,3020) IDUP(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (37,3020) IDUP(2),ICOL1(2),ICOL2(2),Pz2
      DO i=3,NPART
      WRITE (37,3030) IDUP(i),ICOL1(i),ICOL2(i),P(i,1),P(i,2),P(i,3)
      ENDDO

      GOTO 10

100   CLOSE(36)
      CLOSE(37)
      PRINT *,'Unweighted events: ',NUMEVT

      OPEN (36,file=FILE0o,status='unknown')
      WRITE (36,4000) IPROC
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      IF (IPROC .EQ. 17) THEN
      WRITE (36,4010) IQFLAV,ILEP2,ILEP3
      ELSE IF (IPROC .EQ. 18) THEN
      WRITE (36,4011) IQFLAV,IQFLAV2
      ENDIF
      IF (IPROC .EQ. 17) THEN
      WRITE (36,4017) (coup(i),i=1,NCOUP)
      WRITE (36,4018) (coup2(i),i=1,NCOUP)
      ELSE IF (IPROC .EQ. 18) THEN
      WRITE (36,4027) (coup(i),i=1,NCOUP)
      WRITE (36,4028) (coup2(i),i=1,2)
      ENDIF
      WRITE (36,4019) idum0
      WRITE (36,4020) NUMEVT
      WRITE (36,4041) SIG_t,ERR_t

      CLOSE (36)
      RETURN
3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',D14.8,' ',D14.8,' ',D14.8)

4000  FORMAT (I2,'                        ',
     & '       ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4010  FORMAT (I1,' ',I1,' ',I1,'     ',
     &  '                       ! Light quark & lepton flavours')
4011  FORMAT (I1,' ',I1,'        ',
     &  '                      ! Initial flavours')

4017  FORMAT (F6.2,'  ',F6.2,'  ',F6.2,'  ',F6.2,
     &  '   ! Clq, Clu, Cqe, Ceu')
4018  FORMAT (F6.2,'  ',F6.2,'  ',F6.2,'  ',F6.2,
     &  '   ! Cqle1, Clqe1, Cqle2, Clqe2')
4027  FORMAT (F6.1,'  ',F6.1,'  ',F6.1,'  ',F6.1,
     &  '   ! Cqq, Cqup, Cqu, Cuu')
4028  FORMAT (F6.1,'  ',F6.1,'                   ',
     &  '! Cqup2, Cqu2')
4019  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I8,
     &  '                         ! unweighted events')
4041  FORMAT (D12.6,'   ',D12.6,
     & '      ! tt cross section and error')
      END

