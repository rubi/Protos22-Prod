      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

c      GWF(1)=-g/2d0
c      GWF(2)=-g/2d0

      GG(1)=-gs
      GG(2)=-gs

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

      GZuu(1)=-gcw*(1d0/2d0-sw2*2d0/3d0)
      GZuu(2)=gcwsw2*2d0/3d0

      GZdd(1)=-gcw*(-1d0/2d0+sw2/3d0)
      GZdd(2)=-gcwsw2/3d0

      GZvv(1)=-gcw*(1d0/2d0)
      GZvv(2)=0d0

c      GAuu(1)=-2d0/3d0*e         ! Auu
c      GAuu(2)=-2d0/3d0*e

      GZtq(1)=-gcw/2d0*XL
      GZtq(2)=-gcw/2d0*XR

      G2Ztq(1)=-gcw/2d0/MZ*kL    ! t in q out: top decay
      G2Ztq(2)=-gcw/2d0/MZ*kR
      G2Zqt(1)=-G2Ztq(2)         ! t out q in: antitop decay
      G2Zqt(2)=-G2Ztq(1)

!     two factor to keep standard colour factors for lambda/2 in QCD vertex 
!     (anomalous operator has lambda)

      G2Gtq(1)=-2d0*gs/mt*kL    ! t in q out: top decay / tbar production
      G2Gtq(2)=-2d0*gs/mt*kR
      G2Gqt(1)=-G2Gtq(2)         ! t out q in: antitop decay / t production
      G2Gqt(2)=-G2Gtq(1)

