      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pn(0:3),Pl(0:3),Pb(0:3),PA(0:3),Pq(0:3)
      COMMON /MOMEXT/ Pn,Pl,Pb,PA,Pq
      REAL*8 Pt1(0:3),Pt2(0:3),PW1(0:3)
      COMMON /MOMINT/ Pt1,Pt2,PW1
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      INTEGER IFCN_T
      COMMON /tflagF/ IFCN_T

!     Local variables

      REAL*8 EPS,y1,y2,flux
      REAL*8 PCM_LAB(0:3)
      REAL*8 Qt1,Qt2,QW1,F1MASS,m1,m2
      REAL*8 WT1,WT2,WT3,WT,WT_BREIT,WT_PDF,WT_PROD,WT_DEC
      INTEGER ILQ

!     --------------------------------------------

      WTPS=0d0

!     Selection of decay channels

      y1=RAN2(idum)
      IF (y1 .LT. 0.5d0) THEN
        IFCN_T = 1
      ELSE
        IFCN_T = 0
      ENDIF

!     Generation of the masses of the virtual t, tbar, W+, W-
      
      CALL BREIT(mt,Gt,NGt,Qt1,WT1)
      CALL BREIT(mt,Gt,NGt,Qt2,WT2)
      CALL BREIT(MW,GW,NGW,QW1,WT3)
      IF (Qt1 .LT. QW1+mb) RETURN
      WT_BREIT=WT1*WT2*WT3*(2d0*pi)**9

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(Qt1+Qt2)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    

      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system

      IDIR=0
      IF (IPROC .GT. 5) IDIR=1

      m1=0d0
      m2=0d0

      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Select fermion from W decay
  
      IF ((ILEP .GE. 1) .AND. (ILEP .LE. 3)) THEN   ! ILEP = 1,2,3: sel W -> e, mu, tau
        ILQ=ILEP
      ELSE IF (ILEP .EQ. 4) THEN                    ! ILEP = 4: sum W -> e,mu,tau
        y1=RAN2(idum)
        IF (y1 .LT. 1d0/3d0) THEN
          ILQ=1
        ELSE IF (y1 .LT. 2d0/3d0) THEN
          ILQ=2
        ELSE
          ILQ=3
        ENDIF
      ELSE IF (ILEP .EQ. 5) THEN
        y1=RAN2(idum)
        IF (y1 .LT. 0.5d0) THEN
          ILQ=4
        ELSE
          ILQ=5
        ENDIF
      ENDIF

      IF1=ILQ

      F1MASS=0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Generation

      CALL PHASE2sym(SQRT(s),Qt1,Qt2,PCM_LAB,Pt1,Pt2,WT)      ! Main process
      WT_PROD=(2d0*pi)**4*WT

      CALL PHASE2(Qt1,QW1,mb,Pt1,PW1,Pb,WT)                   ! t (tbar) decay to Wb
      WT_DEC=WT

      CALL PHASE2(QW1,0d0,F1MASS,PW1,Pn,Pl,WT)                ! W+- decay
      WT_DEC=WT_DEC*WT

      CALL PHASE2(Qt2,0d0,0d0,Pt2,PA,Pq,WT)                   ! tbar decay
      WT_DEC=WT_DEC*WT

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF/flux

      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Pn(0:3),Pl(0:3),Pb(0:3),PA(0:3),Pq(0:3)
      COMMON /MOMEXT/ Pn,Pl,Pb,PA,Pq
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local variables

!     --------------------------------------------

      INOT=1

98    INOT=0

      RETURN
      END





      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 GG_TT,UU_TT

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pn(0:3),Pl(0:3),Pb(0:3),PA(0:3),Pq(0:3)
      COMMON /MOMEXT/ Pn,Pl,Pb,PA,Pq
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC
      INTEGER IFCN_T
      COMMON /tflagF/ IFCN_T

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER i,j,k,ISTAT,IAMP
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=mt*QFAC

      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .EQ. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .EQ. 0) RETURN
      
      STR(1)=fg1*fg2          ! g  g  -  No factor here nor in global fact() 
      IF (IPPBAR .EQ. 0) THEN
      STR(2)=fu1*fus2
      STR(3)=fd1*fds2
      STR(4)=fs1*fs2
      STR(5)=fc1*fc2
      STR(6)=fus1*fu2
      STR(7)=fds1*fd2
      STR(8)=fs1*fs2
      STR(9)=fc1*fc2
      ELSE
      STR(2)=fu1*fu2
      STR(3)=fd1*fd2
      STR(4)=fs1*fs2
      STR(5)=fc1*fc2
      STR(6)=fus1*fus2
      STR(7)=fds1*fds2
      STR(8)=fs1*fs2
      STR(9)=fc1*fc2
      ENDIF

!     Set "coloured" squared amplitudes to zero

      DO i=1,3
        DO j=1,MAXFL
          CAMP2(i,j)=0d0
        ENDDO
      ENDDO

!     Select helicity amplitude and process

      IAMP=IPROC
      IF (IF1 .EQ. 3) IAMP=IAMP+NPROC
      IF (IFCN_T .EQ. 1) IAMP=IAMP+2*NPROC
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 100) ntry(IAMP) = 100

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      IF (IPROC .NE. 1) GOTO 12

!     g g -> t t~

      ME=0
      ICSTR=1
      CAMP2(ICSTR,0)=2
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=GG_TT(Q1,Q2,Pn,Pl,Pb,PA,Pq,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0
      
      GOTO 20

12    CONTINUE

!     q q~

      ME=0d0
      ICSTR=2
      IF (IDIR .EQ. 1) ICSTR=3
      CAMP2(ICSTR,0)=1
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          M1=UU_TT(Q1,Q2,Pn,Pl,Pb,PA,Pq,nhel(1,k))
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)
      RETURN
      END
