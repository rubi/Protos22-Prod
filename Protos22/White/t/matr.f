      REAL*8 FUNCTION GQ_T(P1,P2,P3,P4a,P4b,NHEL)

!     P1 = g    P2 = q    P3 = b    P4 = W+

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=5)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 zl,zr
      COMMON /tcoupF3/ zl,zr
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1

!     Couplings and other

      REAL*8 GWF(2)
      COMPLEX*16 G2gtq(2),G2gqt(2)
      REAL*8 F1MASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /-1d0/

      INCLUDE 'input/coupling.inc'

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)          ! g
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)           ! q
      CALL OXXXXX(P3,mb,NHEL(3),1,W3)            ! b

      CALL OXXXXX(P4a,0d0,NHEL(4),1,W4a)         ! nu / q
      CALL IXXXXX(P4b,F1MASS,NHEL(5),-1,W4b)     ! e+ / qbar
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4)          ! W+

      CALL FVOXXX(W3,W4,GWF,mt,Gt,W5)            ! t
      CALL IOVSmX(W2,W5,W1,G2Gqt,-1,AMP(1))

      GQ_T = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQ_T = GQ_T + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP)
      ENDDO
      END


      REAL*8 FUNCTION GQB_TB(P1,P2,P3,P4a,P4b,NHEL)

!     P1 = g    P2 = q~    P3 = b~    P4 = W-

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=5)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3(0:3),P4a(0:3),P4b(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W4a(6),W4b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 zl,zr
      COMMON /tcoupF3/ zl,zr
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1

!     Couplings and other

      REAL*8 GWF(2)
      COMPLEX*16 G2Gtq(2),G2Gqt(2)
      REAL*8 F1MASS

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.1666666666666666d0/
      DATA EIGEN_VEC(1,1) /-1d0/

      INCLUDE 'input/coupling.inc'

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                 ! g
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                 ! q~
      CALL IXXXXX(P3,mb,NHEL(3),-1,W3)                  ! b~

      CALL IXXXXX(P4a,0d0,NHEL(4),-1,W4a)               ! nu~ / qbar
      CALL OXXXXX(P4b,F1MASS,NHEL(5),1,W4b)             ! e-  / q
      CALL JIOXXX(W4a,W4b,GWF,MW,GW,W4)                 ! W-

      CALL FVIXXX(W3,W4,GWF,mt,Gt,W5)                   ! t~
      CALL IOVSmX(W5,W2,W1,G2Gtq,1,AMP(1))

      GQB_TB = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          GQB_TB = GQB_TB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

