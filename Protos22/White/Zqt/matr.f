      REAL*8 FUNCTION GG_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     PROCESS : g g  -> t t~  with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                            

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      REAL*8 xl,xr,kl,kr
      COMMON /tcoupF/ xl,xr,kl,kr
      INTEGER IFCN_T,IFCN_TB
      COMMON /tflagF/ IFCN_T,IFCN_TB
      REAL*8 F1MASS,F2MASS,F1aMASS,F2aMASS,F3MASS,F4MASS
      COMMON /EXTFMASS/ F1MASS,F2MASS,F1aMASS,F2aMASS,F3MASS,F4MASS
      REAL*8 B1MASS,B1WID,B2MASS,B2WID
      COMMON /INTBMASS/ B1MASS,B1WID,B2MASS,B2WID

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWF(2),GZll(2),GZtq(2),GG(2)
      REAL*8 GT1(2),GT2(2),GB1(2),GB2(2)
      COMPLEX*16 G2Ztq(2),G2Zqt(2)
      COMPLEX*16 Wdum(6)
  
!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2
      
      GZtq(1)=-gcw/2d0*XL
      GZtq(2)=-gcw/2d0*XR

      G2Ztq(1)=-gcw/2d0/MZ*kL    ! t in q out: top decay
      G2Ztq(2)=-gcw/2d0/MZ*kR
      G2Zqt(1)=-G2Ztq(2)         ! t out q in: antitop decay
      G2Zqt(2)=-G2Ztq(1)

!     For top decay

      GT1(1)=GWF(1)
      GT1(2)=GWF(2)
      GB1(1)=GWF(1)
      GB1(2)=GWF(2)
      IF (IFCN_T .EQ. 1) THEN
        GT1(1)=GZtq(1)
        GT1(2)=GZtq(2)
        GB1(1)=GZll(1)
        GB1(2)=GZll(2)
      ENDIF

!     For antitop decay

      GT2(1)=GWF(1)
      GT2(2)=GWF(2)
      GB2(1)=GWF(1)
      GB2(2)=GWF(2)
      IF (IFCN_TB .EQ. 1) THEN
        GT2(1)=GZtq(1)
        GT2(2)=GZtq(2)
        GB2(1)=GZll(1)
        GB2(2)=GZll(2)
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                            
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)                            

      CALL OXXXXX(P3a,F1aMASS,NHEL(3),1,W3a)          ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+ / qbar
      CALL OXXXXX(P3c,F3MASS,NHEL(5),1,W3c)           ! b
      CALL JIOXXX(W3b,W3a,GB1,B1MASS,B1WID,W3d)       ! W+
      CALL FVOXXX(W3c,W3d,GT1,mt,Gt,W3)               ! t
      IF (IFCN_T .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Ztq,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL IXXXXX(P4a,F2aMASS,NHEL(6),-1,W4a)             ! nu~ / qbar
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)               ! e-  / q
      CALL IXXXXX(P4c,F4MASS,NHEL(8),-1,W4c)              ! b~
      CALL JIOXXX(W4a,W4b,GB2,B2MASS,B2WID,W4d)           ! W-
      CALL FVIXXX(W4c,W4d,GT2,mt,Gt,W4)                   ! t~
      IF (IFCN_TB .EQ. 1) THEN
        CALL FVISmX(W4c,W4d,G2Zqt,mt,Gt,-1,Wdum)          ! t~ anomalous
        W4(1)=W4(1)+Wdum(1)
        W4(2)=W4(2)+Wdum(2)
        W4(3)=W4(3)+Wdum(3)
        W4(4)=W4(4)+Wdum(4)
      ENDIF

      CALL FVOXXX(W3,W2,GG,mt,Gt,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mt,Gt,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_TT = GG_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_TT = GG_TT/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END




      REAL*8 FUNCTION UU_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> t t~    with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      REAL*8 xl,xr,kl,kr
      COMMON /tcoupF/ xl,xr,kl,kr
      INTEGER IFCN_T,IFCN_TB
      COMMON /tflagF/ IFCN_T,IFCN_TB
      REAL*8 F1MASS,F2MASS,F1aMASS,F2aMASS,F3MASS,F4MASS
      COMMON /EXTFMASS/ F1MASS,F2MASS,F1aMASS,F2aMASS,F3MASS,F4MASS
      REAL*8 B1MASS,B1WID,B2MASS,B2WID
      COMMON /INTBMASS/ B1MASS,B1WID,B2MASS,B2WID

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWF(2),GZll(2),GZtq(2),GG(2)
      REAL*8 GT1(2),GT2(2),GB1(2),GB2(2)
      COMPLEX*16 G2Ztq(2),G2Zqt(2)
      COMPLEX*16 Wdum(6)
  
!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GZll(1)=-gcw*(-1d0/2d0+sw2)
      GZll(2)=-gcwsw2

      GZtq(1)=-gcw/2d0*XL
      GZtq(2)=-gcw/2d0*XR

      G2Ztq(1)=-gcw/2d0/MZ*kL    ! t in q out: top decay
      G2Ztq(2)=-gcw/2d0/MZ*kR
      G2Zqt(1)=-G2Ztq(2)         ! t out q in: antitop decay
      G2Zqt(2)=-G2Ztq(1)

!     For top decay

      GT1(1)=GWF(1)
      GT1(2)=GWF(2)
      GB1(1)=GWF(1)
      GB1(2)=GWF(2)
      IF (IFCN_T .EQ. 1) THEN
        GT1(1)=GZtq(1)
        GT1(2)=GZtq(2)
        GB1(1)=GZll(1)
        GB1(2)=GZll(2)
      ENDIF

!     For antitop decay

      GT2(1)=GWF(1)
      GT2(2)=GWF(2)
      GB2(1)=GWF(1)
      GB2(2)=GWF(2)
      IF (IFCN_TB .EQ. 1) THEN
        GT2(1)=GZtq(1)
        GT2(2)=GZtq(2)
        GB2(1)=GZll(1)
        GB2(2)=GZll(2)
      ENDIF

!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)                       
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                       

      CALL OXXXXX(P3a,F1aMASS,NHEL(3),1,W3a)          ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+ / qbar
      CALL OXXXXX(P3c,F3MASS,NHEL(5),1,W3c)           ! b
      CALL JIOXXX(W3b,W3a,GB1,B1MASS,B1WID,W3d)       ! W+
      CALL FVOXXX(W3c,W3d,GT1,mt,Gt,W3)               ! t
      IF (IFCN_T .EQ. 1) THEN
        CALL FVOSmX(W3c,W3d,G2Ztq,mt,Gt,1,Wdum)       ! t anomalous
        W3(1)=W3(1)+Wdum(1)
        W3(2)=W3(2)+Wdum(2)
        W3(3)=W3(3)+Wdum(3)
        W3(4)=W3(4)+Wdum(4)
      ENDIF

      CALL IXXXXX(P4a,F2aMASS,NHEL(6),-1,W4a)             ! nu~ / qbar
      CALL OXXXXX(P4b,F2MASS,NHEL(7),1,W4b)               ! e-  / q
      CALL IXXXXX(P4c,F4MASS,NHEL(8),-1,W4c)              ! b~
      CALL JIOXXX(W4a,W4b,GB2,B2MASS,B2WID,W4d)           ! W-
      CALL FVIXXX(W4c,W4d,GT2,mt,Gt,W4)                   ! t~
      IF (IFCN_TB .EQ. 1) THEN
        CALL FVISmX(W4c,W4d,G2Zqt,mt,Gt,-1,Wdum)          ! t~ anomalous
        W4(1)=W4(1)+Wdum(1)
        W4(2)=W4(2)+Wdum(2)
        W4(3)=W4(3)+Wdum(3)
        W4(4)=W4(4)+Wdum(4)
      ENDIF

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))

      CAMP(1) = -AMP(1)

      UU_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_TT = UU_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_TT = UU_TT/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END

