      REAL*8 FUNCTION GG_TT(P1,P2,Pn,Pl,Pb,Pq,PH,NHEL)

!     PROCESS : g g  -> t t~  with decay to Wb Hq

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),Pn(0:3),Pl(0:3),Pb(0:3),PH(0:3),Pq(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)  
      COMPLEX*16 Wn(6),Wl(6),Wb(6),WW(6)
      COMPLEX*16 Wq(6),WH(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 etal,etar
      COMMON /tcoupF4/ etal,etar
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      INTEGER IFCN_T
      COMMON /tflagF/ IFCN_T

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GHtq(2),GHqt(2)
      REAL*8 F1MASS
  
!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs
      
      GHtq(1)=-etal/SQRT(2d0)          ! t in q out: top decay
      GHtq(2)=-etar/SQRT(2d0)
      GHqt(1)=CONJG(GHtq(2))         ! t out q in: antitop decay
      GHqt(2)=CONJG(GHtq(1))

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)               ! g
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)               ! g
      CALL SXXXXX(PH,1,WH)                            ! H

      IF (IFCN_T .EQ. 0) THEN                         ! t decay to Wb

      CALL OXXXXX(Pn,0d0,NHEL(3),1,Wn)                ! nu / q
      CALL IXXXXX(Pl,F1MASS,NHEL(4),-1,Wl)            ! e+ / qbar
      CALL OXXXXX(Pb,mb,NHEL(5),1,Wb)                 ! b
      CALL JIOXXX(Wl,Wn,GWF,MW,GW,WW)                 ! W+
      CALL FVOXXX(Wb,WW,GWF,mt,Gt,W3)                 ! t

      CALL IXXXXX(Pq,0d0,NHEL(6),-1,Wq)               ! q~
      CALL FSIXXX(Wq,WH,GHqt,mt,Gt,W4)                ! t~ anomalous

      ELSE                                            ! tbar decay to Wb

      CALL IXXXXX(Pn,0d0,NHEL(3),-1,Wn)               ! nu~ / qbar
      CALL OXXXXX(Pl,F1MASS,NHEL(4),1,Wl)             ! e-  / q
      CALL IXXXXX(Pb,mb,NHEL(5),-1,Wb)                ! b~
      CALL JIOXXX(Wn,Wl,GWF,MW,GW,WW)                 ! W-
      CALL FVIXXX(Wb,WW,GWF,mt,Gt,W4)                 ! t~

      CALL OXXXXX(Pq,0d0,NHEL(6),1,Wq)                ! q
      CALL FSOXXX(Wq,WH,GHtq,mt,Gt,W3)                ! t anomalous

      ENDIF

      CALL FVOXXX(W3,W2,GG,mt,Gt,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mt,Gt,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_TT = GG_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_TT = GG_TT/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i)+CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END



      REAL*8 FUNCTION UU_TT(P1,P2,Pn,Pl,Pb,Pq,PH,NHEL)

!     FOR PROCESS : q q~  -> t t~  with decay to Wb Hq

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=6)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),Pn(0:3),Pl(0:3),Pb(0:3),PH(0:3),Pq(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 Wn(6),Wl(6),Wb(6),WW(6)
      COMPLEX*16 Wq(6),WH(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 etal,etar
      COMMON /tcoupF4/ etal,etar
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      INTEGER IFCN_T
      COMMON /tflagF/ IFCN_T

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     Couplings and other

      REAL*8 GWF(2),GG(2)
      COMPLEX*16 GHtq(2),GHqt(2)
      REAL*8 F1MASS

!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

      GG(1)=-gs
      GG(2)=-gs

      GHtq(1)=-etal/SQRT(2d0)          ! t in q out: top decay
      GHtq(2)=-etar/SQRT(2d0)
      GHqt(1)=CONJG(GHtq(2))         ! t out q in: antitop decay
      GHqt(2)=CONJG(GHtq(1))

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      
!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)               ! q                
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)               ! qbar
      CALL SXXXXX(PH,1,WH)                            ! H

      IF (IFCN_T .EQ. 0) THEN                          ! t decay to Wb

      CALL OXXXXX(Pn,0d0,NHEL(3),1,Wn)                ! nu / q
      CALL IXXXXX(Pl,F1MASS,NHEL(4),-1,Wl)            ! e+ / qbar
      CALL OXXXXX(Pb,mb,NHEL(5),1,Wb)                 ! b
      CALL JIOXXX(Wl,Wn,GWF,MW,GW,WW)                 ! W+
      CALL FVOXXX(Wb,WW,GWF,mt,Gt,W3)                 ! t

      CALL IXXXXX(Pq,0d0,NHEL(6),-1,Wq)               ! q~
      CALL FSIXXX(Wq,WH,GHqt,mt,Gt,W4)                ! t~ anomalous
      
      ELSE                                            ! tbar decay to Wb

      CALL IXXXXX(Pn,0d0,NHEL(3),-1,Wn)               ! nu~ / qbar
      CALL OXXXXX(Pl,F1MASS,NHEL(4),1,Wl)             ! e-  / q
      CALL IXXXXX(Pb,mb,NHEL(5),-1,Wb)                ! b~
      CALL JIOXXX(Wn,Wl,GWF,MW,GW,WW)                 ! W-
      CALL FVIXXX(Wb,WW,GWF,mt,Gt,W4)                 ! t~

      CALL OXXXXX(Pq,0d0,NHEL(6),1,Wq)                ! q
      CALL FSOXXX(Wq,WH,GHtq,mt,Gt,W3)                ! t anomalous

      ENDIF

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))

      CAMP(1) = -AMP(1)

      UU_TT = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_TT = UU_TT + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_TT = UU_TT/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END


