      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     External functions used

      INTEGER COL
      REAL*8 RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pn(0:3),Pl(0:3),Pb(0:3),PH(0:3),Pq(0:3)
      COMMON /MOMEXT/ Pn,Pl,Pb,PH,Pq
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      INTEGER IFCN_T
      COMMON /tflagF/ IFCN_T
      INTEGER IQFLAV
      COMMON /QFLAV/ IQFLAV

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 etal,etar
      COMMON /tcoupF4/ etal,etar
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables for colour structure

      REAL*8 CFAPROB(0:MAXFL)
      INTEGER NFLOWS

!     Local variables for flavour structure

      INTEGER IDTAB1(9),IDTAB2(9)

!     Local variables

      INTEGER ID(NPART+1),ICOL1(NPART+1),ICOL2(NPART+1),IDQ
      REAL*8 XWGTUP,Pz1,Pz2,SIGtot,RCH
      INTEGER i

      DATA IDTAB1 /21, 2, 1, 3, 4,-2,-1,-3,-4/
      DATA IDTAB2 /21,-2,-1,-3,-4, 2, 1, 3, 4/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0

      RETURN

10    IF (IMODE .NE. 0) GOTO 11

!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)                          ! Already in pb
      IF (ABS(XWGTUP) .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,ABS(XWGTUP))
      NUMEVT=NUMEVT+1

!     Flavour

      ID(1)=IDTAB1(IPROC)
      ID(2)=IDTAB2(IPROC)
      ID(6)=25

!     Select up or charm 
     
      IF (IQFLAV .EQ. 1) THEN
        IDQ=2
      ELSE
        IDQ=4
      ENDIF

!     Quarks from t tbar decay

      IF (IFCN_T .EQ. 0) THEN        ! top to Wb, antitop to gamma q
        ID(5)=5
        ID(7)=-IDQ
      ELSE
        ID(5)=-5
        ID(7)=IDQ
      ENDIF

!     Fermions from W   ---   by default, from t -> W+ b

      IF (IF1 .EQ. 1) THEN
        ID(3)=12
        ID(4)=-11
      ELSE IF (IF1 .EQ. 2) THEN
        ID(3)=14
        ID(4)=-13
      ELSE IF (IF1 .EQ. 3) THEN
        ID(3)=16
        ID(4)=-15
      ELSE IF (IF1 .EQ. 4) THEN
        ID(3)=2
        ID(4)=-1
      ELSE IF (IF1 .EQ. 5) THEN
        ID(3)=4
        ID(4)=-3
      ENDIF

      IF (IFCN_T .EQ. 1) THEN     ! change if tbar -> W- bbar
        ID(3)=-ID(3)
        ID(4)=-ID(4)
      ENDIF

!     Colour

      DO i=1,NPART+1
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

!     Select colour structure: g g, q q~, q~ q

      IF (IPROC .EQ. 1) THEN
        ICSTR=1
      ELSE IF (IPROC .LE. 5) THEN
        ICSTR=2
      ELSE
        ICSTR=3
      ENDIF
      NFLOWS=CAMP2(ICSTR,0)

!     Calculate probabilities for each colour flow
      
      SIGtot=0d0
      DO i=1,NFLOWS
        SIGtot=SIGtot+ABS(CAMP2(ICSTR,i))
      ENDDO
      CFAPROB(0)=0d0
      DO i=1,NFLOWS
        CFAPROB(i)=CFAPROB(i-1)+ABS(CAMP2(ICSTR,i))/SIGtot
      ENDDO

!     Select colour flow

      RCH=RAN2(IDUM)
      IFL=0
      DO WHILE (CFAPROB(IFL) .LT. RCH)
        IFL=IFL+1
      END DO
      IF (IFL .GT. NFLOWS) THEN
        PRINT *,'IFL > NFLOWS error'
        PRINT *,IFL,nflows
        STOP
      ENDIF

!     Store all colours

      ICOL1(1)=COL(1,1)
      ICOL2(1)=COL(1,2)
      ICOL1(2)=COL(2,1)
      ICOL2(2)=COL(2,2)
      IF (IFCN_T .EQ. 0) THEN
        ICOL1(5)=COL(3,1)
        ICOL2(5)=COL(3,2)
        ICOL1(7)=COL(4,1)
        ICOL2(7)=COL(4,2)
      ELSE
        ICOL1(7)=COL(3,1)
        ICOL2(7)=COL(3,2)
        ICOL1(5)=COL(4,1)
        ICOL2(5)=COL(4,2)
      ENDIF

      DO i=3,4
        IF (ABS(ID(i)) .LT. 10) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=11
          ELSE
            ICOL2(i)=11
          ENDIF
        ENDIF
      ENDDO

      Pz1=Q1(3)
      Pz2=Q2(3)

      WRITE (35,3010) NUMEVT,XWGTUP,2d0*Q
      WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),Pn(1),Pn(2),Pn(3)
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),Pl(1),Pl(2),Pl(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pb(1),Pb(2),Pb(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),PH(1),PH(2),PH(3)
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),Pq(1),Pq(2),Pq(3)
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 15
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4009) IQFLAV
      WRITE (36,4010) etal,etar
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_tot,ERR_tot
      CLOSE (36)
      RETURN

12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.2,
     &  '       ! mt, Gt, mb, MH')
4009  FORMAT (I1,
     &  '                                ! Light quark flavour')
4010  FORMAT (F6.4,'  ',F6.4,
     &  '                   ! etaL, etaR')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! tt cross section and error')
      END


      INTEGER FUNCTION COL(IPART,I)
      IMPLICIT NONE
      INTEGER IPART,I
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      INTEGER IGFLOW(2,2,8,10)
      DATA (IGFLOW(1,1,i,  1),i=1,  4)/502,501,501,  0/
      DATA (IGFLOW(1,2,i,  1),i=1,  4)/503,502,  0,503/
      DATA (IGFLOW(1,1,i,  2),i=1,  4)/501,502,501,  0/
      DATA (IGFLOW(1,2,i,  2),i=1,  4)/502,503,  0,503/
      DATA (IGFLOW(2,1,i,  1),i=1,  4)/501,  0,501,  0/
      DATA (IGFLOW(2,2,i,  1),i=1,  4)/  0,502,  0,502/

      IF (ICSTR .LT. 3) THEN
        COL=IGFLOW(ICSTR,I,IPART,IFL)
      ELSE IF (IPART .GT. 2) THEN
        COL=IGFLOW(2,I,IPART,IFL)
      ELSE
        COL=IGFLOW(2,I,3-IPART,IFL)
      ENDIF
      IF (COL .NE.0) COL=COL-500
      RETURN
      END
