
      REAL*8 FUNCTION UU_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : u u  -> t t    with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=14,NEIGEN=2,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      REAL*8 Cqq,Cqup,Cqu,Cuu,Cqup2,Cqu2
      COMMON /COUP4F/ Cqq,Cqup,Cqu,Cuu,Cqup2,Cqu2

!     Couplings and other

      REAL*8 GWF(2)
      REAL*8 F1MASS,F2MASS
      REAL*8 PL(2),PR(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.6666666666666666d0/   ! a != b
      DATA EIGEN_VEC(1,1) / 2d0 /                ! LL
      DATA EIGEN_VEC(2,1) / 0d0 /
      DATA EIGEN_VEC(3,1) / 2d0 /                ! RR
      DATA EIGEN_VEC(4,1) / 0d0 /
      DATA EIGEN_VEC(5,1) / 1d0 /                ! LR
      DATA EIGEN_VEC(6,1) / 0d0 /
      DATA EIGEN_VEC(11,1) / 0d0 /
      DATA EIGEN_VEC(12,1) / 0d0 /
      DATA EIGEN_VEC(7,1) / 1d0 /                ! RL
      DATA EIGEN_VEC(8,1) /-1d0 /                ! LR dif. colour contraction
      DATA EIGEN_VEC(9,1) / 0d0 /
      DATA EIGEN_VEC(13,1) / 0d0 /
      DATA EIGEN_VEC(14,1) / 0d0 /
      DATA EIGEN_VEC(10,1) /-1d0 /               ! RL dif. colour contraction

      DATA EIGEN_VAL(2) /0.1666666666666666d0/   ! a = b
      DATA EIGEN_VEC(1,2) / 2d0 /                ! LL
      DATA EIGEN_VEC(2,2) /-2d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(3,2) / 2d0 /                ! RR
      DATA EIGEN_VEC(4,2) /-2d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(5,2) / 1d0 /                ! LR
      DATA EIGEN_VEC(6,2) /-1d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(11,2) / -1d0 /              ! - with u u exchanged
      DATA EIGEN_VEC(12,2) /1d0 /                ! - with both exchanged
      DATA EIGEN_VEC(7,2) / 0d0 /
      DATA EIGEN_VEC(8,2) /-1d0 /                ! LR dif. colour contraction
      DATA EIGEN_VEC(9,2) / 1d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(13,2) /1d0 /                ! - with u u exchanged
      DATA EIGEN_VEC(14,2) / -1d0 /               ! - with both exchanged
      DATA EIGEN_VEC(10,2) / 0d0 /

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Four-fermion projectors

      PL(1) = 1d0
      PL(2) = 0d0
      PR(1) = 0d0
      PR(2) = 1d0

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau
      
!     Code

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)                       
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)                       

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)              ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+ / qbar
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)               ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)               ! t
      
      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)              ! nu / q
      CALL IXXXXX(P4b,F2MASS,NHEL(7),-1,W4b)          ! e+  / qbar
      CALL OXXXXX(P4c,mb,NHEL(8),1,W4c)               ! b
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4d)              ! W+
      CALL FVOXXX(W4c,W4d,GWF,mt,Gt,W4)               ! t

      CALL IOIOVX(W1,W3,W2,W4,PL,PL,AMP(1))           ! (u_L t_L) (u_L t_L)
      CALL IOIOVX(W1,W4,W2,W3,PL,PL,AMP(2))           ! 4,3 exchanged

      CALL IOIOVX(W1,W3,W2,W4,PR,PR,AMP(3))           ! (u_R t_R) (u_R t_R)
      CALL IOIOVX(W1,W4,W2,W3,PR,PR,AMP(4))           ! 4,3 exchanged

      CALL IOIOVX(W1,W3,W2,W4,PL,PR,AMP(5))           ! (u_La t_La) (u_Rb t_Rb)
      CALL IOIOVX(W1,W4,W2,W3,PL,PR,AMP(6))           ! 4,3 exchanged
      CALL IOIOVX(W2,W3,W1,W4,PL,PR,AMP(11))          ! 1,2 exchanged
      CALL IOIOVX(W2,W4,W1,W3,PL,PR,AMP(12))          ! 1,2;4,3 exchanged
      CALL IOIOVX(W1,W3,W2,W4,PR,PL,AMP(7))           ! (u_Ra t_Rb) (u_Lb t_La)


c      CALL IOIOVX(W1,W4,W2,W3,PL,PR,AMP(8))           ! (u_La t_Lb) (u_Rb t_Ra)
      AMP(8)=AMP(6)
c      CALL IOIOVX(W1,W3,W2,W4,PL,PR,AMP(9))           ! 4,3 exchanged
      AMP(9)=AMP(5)
c      CALL IOIOVX(W2,W4,W1,W3,PL,PR,AMP(13))           ! 1,2 exchanged
      AMP(13)=AMP(12)
c      CALL IOIOVX(W2,W3,W1,W4,PL,PR,AMP(14))           ! 1,2;4,3 exchanged
      AMP(14)=AMP(11)
      CALL IOIOVX(W1,W4,W2,W3,PR,PL,AMP(10))

      AMP(1)=AMP(1)*Cqq/2d0/1d6
      AMP(2)=AMP(2)*Cqq/2d0/1d6
      AMP(3)=AMP(3)*Cuu/2d0/1d6
      AMP(4)=AMP(4)*Cuu/2d0/1d6
      AMP(5)=-AMP(5)*Cqup/2d0/1d6
      AMP(6)=-AMP(6)*Cqup/2d0/1d6
      AMP(7)=-AMP(7)*Cqup/2d0/1d6
      AMP(8)=-AMP(8)*Cqu/2d0/1d6
      AMP(9)=-AMP(9)*Cqu/2d0/1d6
      AMP(10)=-AMP(10)*Cqu/2d0/1d6

!     new contractions

      AMP(11)=-AMP(11)*Cqup/2d0/1d6
      AMP(12)=-AMP(12)*Cqup/2d0/1d6
      AMP(13)=-AMP(13)*Cqu/2d0/1d6
      AMP(14)=-AMP(14)*Cqu/2d0/1d6

      UU_TT = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          UU_TT = UU_TT + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



      REAL*8 FUNCTION UC_TT(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : u c  -> t t    with decay

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=12,NEIGEN=2,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN),EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      REAL*8 Cqq,Cqup,Cqu,Cuu,Cqup2,Cqu2
      COMMON /COUP4F/ Cqq,Cqup,Cqu,Cuu,Cqup2,Cqu2

!     Couplings and other

      REAL*8 GWF(2)
      REAL*8 F1MASS,F2MASS
      REAL*8 PL(2),PR(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.6666666666666666d0/   ! a != b
      DATA EIGEN_VEC(1,1) / 1d0 /                ! LL
      DATA EIGEN_VEC(2,1) / 0d0 /
      DATA EIGEN_VEC(3,1) / 1d0 /                ! RR
      DATA EIGEN_VEC(4,1) / 0d0 /
      DATA EIGEN_VEC(5,1) / 1d0 /                ! LR
      DATA EIGEN_VEC(6,1) / 0d0 /
      DATA EIGEN_VEC(7,1) / 1d0 /                ! RL
      DATA EIGEN_VEC(8,1) / 0d0 /
      DATA EIGEN_VEC(9,1) / -1d0 /                ! LR dif. colour contraction
      DATA EIGEN_VEC(10,1) / 0d0 /
      DATA EIGEN_VEC(11,1) / -1d0 /               ! RL dif. colour contraction
      DATA EIGEN_VEC(12,1) / 0d0 /

      DATA EIGEN_VAL(2) /0.1666666666666666d0/   ! a = b
      DATA EIGEN_VEC(1,2) / 1d0 /                ! LL
      DATA EIGEN_VEC(2,2) /-1d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(3,2) / 1d0 /                ! RR
      DATA EIGEN_VEC(4,2) /-1d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(5,2) / 1d0 /                ! LR
      DATA EIGEN_VEC(6,2) /-1d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(7,2) / 1d0 /                ! RL
      DATA EIGEN_VEC(8,2) /-1d0 /                ! - with t t exchanged
      DATA EIGEN_VEC(9,2) / -1d0 /               ! LR dif. colour contraction
      DATA EIGEN_VEC(10,2) / 1d0 /               ! - with t t exchanged
      DATA EIGEN_VEC(11,2) / -1d0 /              ! RL dif. colour contraction
      DATA EIGEN_VEC(12,2) / 1d0 /               ! - with t t exchanged


      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Four-fermion projectors

      PL(1) = 1d0
      PL(2) = 0d0
      PR(1) = 0d0
      PR(2) = 1d0

      F1MASS=0d0
      IF (IF1 .EQ. 3) F1MASS=mtau
      F2MASS=0d0
      IF (IF2 .EQ. 3) F2MASS=mtau
      
!     Code

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)                ! u                      
      CALL IXXXXX(P2,0d0,NHEL(2),1,W2)                ! c    

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)              ! nu / q
      CALL IXXXXX(P3b,F1MASS,NHEL(4),-1,W3b)          ! e+ / qbar
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)               ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)              ! W+
      CALL FVOXXX(W3c,W3d,GWF,mt,Gt,W3)               ! t
      
      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)              ! nu / q
      CALL IXXXXX(P4b,F2MASS,NHEL(7),-1,W4b)          ! e+  / qbar
      CALL OXXXXX(P4c,mb,NHEL(8),1,W4c)               ! b
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4d)              ! W+
      CALL FVOXXX(W4c,W4d,GWF,mt,Gt,W4)               ! t

      CALL IOIOVX(W1,W3,W2,W4,PL,PL,AMP(1))           ! (u_L t_L) (c_L t_L)
      CALL IOIOVX(W1,W4,W2,W3,PL,PL,AMP(2))           ! exchanged

      CALL IOIOVX(W1,W3,W2,W4,PR,PR,AMP(3))           ! (u_R t_R) (c_R t_R)
      CALL IOIOVX(W1,W4,W2,W3,PR,PR,AMP(4))           ! exchanged

      CALL IOIOVX(W1,W3,W2,W4,PL,PR,AMP(5))           ! (u_L t_L) (c_R t_R)
      CALL IOIOVX(W1,W4,W2,W3,PL,PR,AMP(6))           ! exchanged

      CALL IOIOVX(W1,W3,W2,W4,PR,PL,AMP(7))           ! (u_R t_R) (c_L t_L)
      CALL IOIOVX(W1,W4,W2,W3,PR,PL,AMP(8))           ! exchanged

c      CALL IOIOVX(W1,W4,W2,W3,PL,PR,AMP(9))           ! (u_La t_Lb) (c_Rb t_Ra)
      AMP(9)=AMP(6)
c      CALL IOIOVX(W1,W3,W2,W4,PL,PR,AMP(10))          ! exchanged
      AMP(10)=AMP(5)

c      CALL IOIOVX(W1,W4,W2,W3,PR,PL,AMP(11))           ! (u_R t_R) (c_L t_L)
      AMP(11)=AMP(8)
c      CALL IOIOVX(W1,W3,W2,W4,PR,PL,AMP(12))           ! exchanged
      AMP(12)=AMP(7)


      AMP(1)=AMP(1)*Cqq/2d0/1d6
      AMP(2)=AMP(2)*Cqq/2d0/1d6
      AMP(3)=AMP(3)*Cuu/2d0/1d6
      AMP(4)=AMP(4)*Cuu/2d0/1d6
      AMP(5)=-AMP(5)*Cqup/2d0/1d6
      AMP(6)=-AMP(6)*Cqup/2d0/1d6
      AMP(7)=-AMP(7)*Cqup2/2d0/1d6
      AMP(8)=-AMP(8)*Cqup2/2d0/1d6
      AMP(9)=-AMP(9)*Cqu/2d0/1d6
      AMP(10)=-AMP(10)*Cqu/2d0/1d6
      AMP(11)=-AMP(11)*Cqu2/2d0/1d6
      AMP(12)=-AMP(12)*Cqu2/2d0/1d6

      UC_TT = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          UC_TT = UC_TT + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

