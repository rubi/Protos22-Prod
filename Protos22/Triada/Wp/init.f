      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF,PW_N3b

!     Input parameters 

      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VL(3),VR(3)
      COMMON /Ncoup/ VL,VR
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      INTEGER IDW1,IDZ1,IDH1,IDjj1,IDtb1,IDLNC,IDLNV
      COMMON /Nfstate/ IDW1,IDZ1,IDH1,IDjj1,IDtb1,IDLNC,IDLNV

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 gR
      COMMON /WPcoup/ gR
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      REAL*8 WTT1(5),WTFL(3),WTFR(3),WTZ(4)
      REAL*8 BRT1ac(0:5),BRFLac(0:3),BRFRac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTFL,WTFR,WTZ,BRT1ac,BRFLac,BRFRac,BRZac

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGN,NGW,NGZ,NGt
      COMMON /BWdata/ NGN,NGW,NGZ,NGt
      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &        nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,nhel4,GOODHEL,ncomb8,ncomb6,ncomb4,ntry

!     Variables for weighting of final states

      REAL*8 PW_T1
      REAL*8 PRT1(5),PRZ(4),PRFL(3),PRFR(3)

!     Local variables

      INTEGER i,j
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 GN_Wl,GN_Zv,GN_Hv,GN_jjl,GN_tbl
      REAL*8 GWp_u,GWp_t,GWp_N
      REAL*8 xW,xb,EW,qW,xt,xN
      REAL*8 r_W,r_Z,r_H,VLsq,VRsq
      REAL*8 gevpb
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb*1000d0                   ! Cross section in fb
      ENDDO
      
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(MWP)
      CALL SETALPHAS(MWP)
      cw=SQRT(1d0-sw2)

!     Select W' model

      OPEN (31,FILE='input/WPpar.dat',status='old')
      READ (31,*) gR
      CLOSE (31)
      gR=gR*g
!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)

!     Z boson

      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)

!     Higgs  -- not used

      GH=1d0

!     Heavy N

      r_Z = (MZ/mN)**2
      r_W = (MW/mN)**2
      r_H = (MH/mN)**2
      VLsq = VL(1)**2 + VL(2)**2 + VL(3)**2
      VRsq = VR(1)**2 + VR(2)**2 + VR(3)**2
      VR(1)=VR(1)/SQRT(VRsq)
      VR(2)=VR(2)/SQRT(VRsq)
      VR(3)=VR(3)/SQRT(VRsq)
      VRsq = VR(1)**2 + VR(2)**2 + VR(3)**2

      GN_Wl = g**2/(64d0*pi)*VLsq*
     &  (mN**3/MW**2)*(1d0-r_W)*(1d0+r_W-2d0*r_W**2)
      IF (MW .GT. mN) GN_Wl=0d0
      IF (IMA .EQ. 1) GN_Wl=2d0*GN_Wl

      GN_Zv = g**2/(128d0*pi*cw**2)*VLsq*
     &  (mN**3/MZ**2)*(1d0-r_Z)*(1d0+r_Z-2d0*r_Z**2)
      IF (MZ .GT. mN) GN_Zv=0d0
      IF (IMA .EQ. 1) GN_Zv=2d0*GN_Zv

      GN_Hv = g**2/(128d0*pi)*VLsq*
     &  (mN**3/MW**2)*(1d0-r_H)**2
      IF (MH .GT. mN) GN_Hv=0d0
      IF (IMA .EQ. 1) GN_Hv=2d0*GN_Hv
      
      GN_jjl = 6d0*PW_N3b(mN,0d0,0d0,0d0)
      IF (IMA .EQ. 1) GN_jjl=2d0*GN_jjl

      GN_tbl = 3d0*PW_N3b(mN,0d0,mt,0d0)
      IF (IMA .EQ. 1) GN_tbl=2d0*GN_tbl

      GN=GN_Wl+GN_Zv+GN_Hv+GN_jjl+GN_tbl

!     W'

      GWp_u=3d0*gR**2/(48d0*pi) * MWp

      xt=mt/MWp
      GWp_t=3d0*gR**2/(48d0*pi) * MWp * (1d0-xt**2) *
     &  (1d0-xt**2/2d0-xt**4/2d0)

      xN=mN/MWp
      GWp_N=gR**2/(48d0*pi) * MWp * (1d0-xN**2) *
     &  (1d0-xN**2/2d0-xN**4/2d0)
      IF (MWp .LT. mN) GWp_N=0d0
      IF (IMA .EQ. 0) GWp_N=2d0*GWp_N
c      GWp_N=3d0*GWp_N                                                 ! 3 N's

      GWp=2d0*GWp_u+GWp_t+GWp_N

      PRINT 1050
      PRINT 1150,MWp,GWp
      PRINT 1160,GWp_u/GWp,GWp_t/GWp
      PRINT 1170,GWp_N/GWp
      PRINT 999
      PRINT 1100,mN,GN
      PRINT 1204,GN_Wl/GN,GN_Zv/GN,GN_Hv/GN
      PRINT 1205,GN_jjl/GN,GN_tbl/GN
      PRINT 999
      PRINT 1201,GZ_nu/GZ,GZ_emt/GZ
      PRINT 1202,GZ_uc/GZ,GZ_dsb/GZ
      PRINT 999

!     Probabilities for each decay channel

      PW_T1=GN_Wl*DFLOAT(IDW1)*DFLOAT(IDLNC+IDLNV)/2d0
     &  +GN_Zv*DFLOAT(IDZ1)+GN_Hv*DFLOAT(IDH1)+GN_jjl*DFLOAT(IDjj1)
     &  +GN_tbl*DFLOAT(IDtb1)
      PRT1(1)=GN_Wl*DFLOAT(IDW1)*DFLOAT(IDLNC+IDLNV)/2d0/PW_T1
      PRT1(2)=GN_Zv*DFLOAT(IDZ1)/PW_T1
      PRT1(3)=GN_Hv*DFLOAT(IDH1)/PW_T1
      PRT1(4)=GN_jjl*DFLOAT(IDjj1)*DFLOAT(IDLNC+IDLNV)/2d0/PW_T1
      PRT1(5)=GN_tbl*DFLOAT(IDtb1)*DFLOAT(IDLNC+IDLNV)/2d0/PW_T1

      IF (PW_T1 .EQ. 0d0) THEN
        PRINT 985
        PRINT 999
        STOP
      ENDIF

      BRT1ac(0)=0d0
      DO i=1,5
        BRT1ac(i)=BRT1ac(i-1)+PRT1(i)
        WTT1(i)=1d0/PRT1(i)
        IF (PRT1(i) .EQ. 0d0) WTT1(i)=0d0
      ENDDO
      IF (ABS(BRT1ac(5)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRT1ac(5)
      BRT1ac(5)=1d0

      PRZ(1)=GZ_nu/GZ
      PRZ(2)=GZ_emt/GZ
      PRZ(3)=GZ_uc/GZ
      PRZ(4)=GZ_dsb/GZ

      BRZac(0)=0d0
      DO i=1,4
        BRZac(i)=BRZac(i-1)+PRZ(i)
        WTZ(i)=1d0/PRZ(i)
      ENDDO
      IF (ABS(BRZac(4)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRZac(4)
      BRZac(4)=1d0

      PRFL(1)=VL(1)**2/VLsq
      PRFL(2)=VL(2)**2/VLsq
      PRFL(3)=VL(3)**2/VLsq

      BRFLac(0)=0d0
      DO i=1,3
        BRFLac(i)=BRFLac(i-1)+PRFL(i)
        WTFL(i)=1d0/PRFL(i)
        IF (PRFL(i) .EQ. 0d0) WTFL(i)=0d0
      ENDDO
      IF (ABS(BRFLac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRFLac(3)
      BRFLac(3)=1d0

      PRFR(1)=VR(1)**2/VRsq
      PRFR(2)=VR(2)**2/VRsq
      PRFR(3)=VR(3)**2/VRsq

      BRFRac(0)=0d0
      DO i=1,3
        BRFRac(i)=BRFRac(i-1)+PRFR(i)
        WTFR(i)=1d0/PRFR(i)
        IF (PRFR(i) .EQ. 0d0) WTFR(i)=0d0
      ENDDO
      IF (ABS(BRFRac(3)-1d0) .GT. 1d-8) PRINT 1303,1d0-BRFRac(3)
      BRFRac(3)=1d0

!     Include here colour factors and sum over fermions for Z, H

      WTZ(1)=WTZ(1)*3d0
      WTZ(2)=WTZ(2)*3d0
      WTZ(3)=WTZ(3)*6d0
      WTZ(4)=WTZ(4)*9d0

      NGN=mN/GN*0.95d0
      NGW=MW/GW*0.95d0
      NGZ=MZ/GZ*0.95d0
      NGt=mt/Gt*0.95d0

!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO

      CALL SETPOL(istep,nhel8,ncomb8,8)
      DO i=1,MAXAMP
        DO j=1,ncomb8
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO
      CALL SETPOL(istep,nhel6,ncomb6,6)
      CALL SETPOL(istep,nhel4,ncomb4,4)

c      DO i=1,ncomb6
c      PRINT *,(nhel6(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb6
c      DO i=1,ncomb4
c      PRINT *,(nhel4(j,i),j=1,NPART)
c      ENDDO
c      PRINT *,ncomb4
c      STOP

      RETURN

500   FORMAT (' _____    _           _       ')
501   FORMAT ('|_   _|  (_)         | |      ')
502   FORMAT ('  | |_ __ _  __ _  __| | __ _ ')
503   FORMAT ('  | | \'__| |/ _` |/ _` |/ _` |')
504   FORMAT ('  | | |  | | (_| | (_| | (_| |')
505   FORMAT ('  \\_/_|  |_|\\__,_|\\__,_|\\__,_|')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
985   FORMAT ('Please open some decay channel for heavy leptons!')
999   FORMAT ('')
1000  FORMAT ('---------------------------------------')
1050  FORMAT ('Calculating W\' -> lN production')
1100  FORMAT ('mN = ',F6.1,'   GN = ',F9.6)
1150  FORMAT ('MW\' = ',F6.1,'   GW\' = ',F5.2)      
1160  FORMAT ('Br(W\' -> ud) = ',F5.4,'   Br(W\' -> tb) = ',F5.4)
1170  FORMAT ('Br(W\' -> Nl) = ',F5.4)
1201  FORMAT ('BR(Z -> vv) = ',F5.3,'  BR(Z -> ll) = ',F5.3)
1202  FORMAT ('BR(Z -> uu) = ',F5.3,'  BR(Z -> dd) = ',F5.3)
1204  FORMAT ('BR(N -> Wl)  = ',F5.3,
     &  '   BR(N -> Zv)  = ',F5.3,'   BR(N -> Hv) = ',F5.3)
1205  FORMAT ('BR(N -> jjl) = ',F5.3,
     &  '   BR(N -> tbl) = ',F5.3)
1303  FORMAT ('Warning: in initialisation acc sum = ',D9.4)
      END


      SUBROUTINE SETPOL(nspin,hel,nhel,num)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel,num

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,num
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,num
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=num
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,num
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END


      DOUBLE PRECISION FUNCTION PW_N3b(mN,ml,mu,md)
      IMPLICIT NONE

!     Arguments

      REAL*8 mN,ml,mu,md

!     Parameters

      INTEGER num
      PARAMETER (num=200000)

!     Functions used

      REAL*8 DOT

!     Required data

      REAL*8 gR
      COMMON /WPcoup/ gR
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP


!     Local

      REAL*8 pi
      REAL*8 K(0:3),P1(0:3),P2(0:3),P3(0:3)
      REAL*8 flux,WT,Msq,prop,SIG,dSIG
      INTEGER it

      pi=2d0*dasin(1d0)
      SIG=0d0

!     Loop

      DO 100 it=1,num

      flux=2d0*mN
      K(0)=mN
      K(1)=0d0
      K(2)=0d0
      K(3)=0d0
      CALL INITXRN(5)
      CALL PHASE3(mN,ml,mu,md,K,P1,P2,P3,WT)
      WT=(2d0*pi)**4*WT
      prop=(2d0*DOT(P2,P3)-MWp**2)**2+(MWp*GWp)**2
      Msq = 2d0*gR**4*DOT(K,P2)*DOT(P1,P3) / prop
      dSIG=Msq*WT/flux
      SIG=SIG+dSIG

100   CONTINUE

      PW_N3b=SIG/DFLOAT(num)
      END

