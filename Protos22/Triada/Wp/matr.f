      REAL*8 FUNCTION QQ_lN(NHEL)

!     FOR PROCESS : q q~  -> l N with all decays

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      INTEGER NHEL(NEXTERNAL)

!     Momenta

      REAL*8 P1(0:3),P2(0:3)               ! Different name!!!
      COMMON /MOMINI/ P1,P2
      REAL*8 PN(0:3),PB1(0:3),Pt(0:3),PW(0:3)
      COMMON /MOMINT/ PN,PB1,Pt,PW
      REAL*8 Pl1(0:3),Pl2(0:3),Pf1(0:3),Pfb1(0:3)
      COMMON /MOMEXT/ Pl1,Pl2,Pf1,Pfb1
      REAL*8 Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXTRA/ Pf2,Pfb2

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6)
      COMPLEX*16 Wl1(6),Wl2(6),WB1(6),WN(6)
      COMPLEX*16 Wf1(6),Wfb1(6),Wf2(6),Wfb2(6),WW(6),Wt(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VL(3),VR(3)
      COMMON /Ncoup/ VL,VR
      INTEGER IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IF1,ILNV
      REAL*8 MWP,GWP
      COMMON /WPmass/ MWP,GWP
      REAL*8 gR
      COMMON /WPcoup/ gR

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GWFR(2)
      REAL*8 GWlN(2),GWlN_I(2),GZNv(2),GZvN(2),GWplN(2),GWplN_I(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 MB1,GB1
      REAL*8 GB1ff(2),G3B1(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Mass and SM coupling of the bosons

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        IF (ILNV .EQ. 0) THEN
          G3B1(1)=GWlN(1)
          G3B1(2)=GWlN(2)
        ELSE
          G3B1(1)=GWlN_I(1)
          G3B1(2)=GWlN_I(2)
        ENDIF
        GB1ff(1)=GWF(1)
        GB1ff(2)=GWF(2)
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        G3B1(1)=GZNv(1)
        G3B1(2)=GZNv(2)
        IF (IF1 .EQ. 0) THEN
          GB1ff(1)=GZvv(1)
          GB1ff(2)=GZvv(2)
        ELSE IF (IF1 .EQ. 1) THEN
          GB1ff(1)=GZll(1)
          GB1ff(2)=GZll(2)
        ELSE IF (IF1 .EQ. 2) THEN
          GB1ff(1)=GZuu(1)
          GB1ff(2)=GZuu(2)
        ELSE IF (IF1 .EQ. 3) THEN
          GB1ff(1)=GZdd(1)
          GB1ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF1 = ',IF1,'in QQ_lN'
          STOP
        ENDIF
      ELSE IF ((IB1 .EQ. 4) .OR. (IB1 .EQ. 5)) THEN
        MB1=MWp
        GB1=GWp
        IF (ILNV .EQ. 0) THEN
          G3B1(1)=GWplN(1)
          G3B1(2)=GWplN(2)
        ELSE
          G3B1(1)=GWplN_I(1)
          G3B1(2)=GWplN_I(2)
        ENDIF
        GB1ff(1)=GWFR(1)
        GB1ff(2)=GWFR(2)
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar
      IF (IQ .EQ. 1) THEN                        ! W'+
        CALL IXXXXX(Pl2,0d0,NHEL(3),-1,Wl1)      ! l+
        CALL OXXXXX(Pl1,0d0,NHEL(4),1,Wl2)       ! Fermion flow as l- / nu
      ELSE
        CALL OXXXXX(Pl2,0d0,NHEL(3),1,Wl1)       ! l-
        CALL IXXXXX(Pl1,0d0,NHEL(4),-1,Wl2)      ! Fermion flow as l+ / nubar
      ENDIF

!     N

      IF (IB1 .EQ. 3) THEN
        CALL SXXXXX(PB1,1,WB1)                            ! H
        IF (IQ .EQ. 1) THEN
          CALL FSOXXX(Wl2,WB1,GHNv,mN,GN,WN)              ! N (N in v out)
        ELSE
          CALL FSIXXX(Wl2,WB1,GHvN,mN,GN,WN)              ! N (v in N out)
        ENDIF 
      ELSE  IF (IB1 .EQ. 5) THEN
        CALL OXXXXX(Pf1,0d0,NHEL(5),1,Wf1)                ! f1
        CALL IXXXXX(Pfb1,0d0,NHEL(6),-1,Wfb1)             ! f1bar
        CALL OXXXXX(Pf2,0d0,NHEL(7),1,Wf2)                ! f2
        CALL IXXXXX(Pfb2,0d0,NHEL(8),-1,Wfb2)             ! f2bar
        CALL JIOXXX(Wfb2,Wf2,GWF,MW,GW,WW)                ! W+- from t/tbar
        IF (   ((IQ .EQ. 1) .AND. (ILNV .EQ. 0)) 
     &    .OR. ((IQ .EQ. 2) .AND. (ILNV .EQ. 1)) ) THEN   ! it's a top
          CALL FVOXXX(Wf1,WW,GWF,mt,Gt,Wt)                ! t out
          CALL JIOXXX(Wfb1,Wt,GB1ff,MB1,GB1,WB1)          ! W'+-
        ELSE                                              ! it's an antitop
          CALL FVIXXX(Wfb1,WW,GWF,mt,Gt,Wt)               ! tbar in
          CALL JIOXXX(Wt,Wf1,GB1ff,MB1,GB1,WB1)           ! W'+-
        ENDIF
        IF (IQ .EQ. 1) THEN
          CALL FVOXXX(Wl2,WB1,G3B1,mN,GN,WN)              ! N
        ELSE
          CALL FVIXXX(Wl2,WB1,G3B1,mN,GN,WN)              ! N
        ENDIF
      ELSE                                                ! IB1 = 1,2,4
        CALL OXXXXX(Pf1,0d0,NHEL(5),1,Wf1)                ! f1
        CALL IXXXXX(Pfb1,0d0,NHEL(6),-1,Wfb1)             ! f1bar
        CALL JIOXXX(Wfb1,Wf1,GB1ff,MB1,GB1,WB1)           ! W+- / Z / W'+-
        IF (IQ .EQ. 1) THEN
          CALL FVOXXX(Wl2,WB1,G3B1,mN,GN,WN)              ! N
        ELSE
          CALL FVIXXX(Wl2,WB1,G3B1,mN,GN,WN)              ! N
        ENDIF
      ENDIF

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GWFR,MWP,GWP,W3)
      IF (IQ .EQ. 1) THEN
        CALL IOVXXX(Wl1,WN,W3,GWFR,AMP(1))
      ELSE
        CALL IOVXXX(WN,Wl1,W3,GWFR,AMP(1))
      ENDIF

      AMP(1)=AMP(1)*VR(IL2)
      IF (IB1 .LE. 3) THEN
        AMP(1)=AMP(1)*VL(IL1)
      ELSE
        AMP(1)=AMP(1)*VR(IL1)
      ENDIF

      QQ_lN = 0d0 
      DO I = 1, NEIGEN
          ZTEMP = (0d0,0d0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          QQ_lN = QQ_lN + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


