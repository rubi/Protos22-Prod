      REAL*8 FUNCTION FXN(X,WGT)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      REAL*8 X(MAXDIM),WGT

!     Data needed

      INTEGER IRECORD,IWRITE,IHISTO
      COMMON /FXNflags/ IRECORD,IWRITE,IHISTO
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Output

      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT

!     Local variables      

      REAL*8 WTPS,WTME
      INTEGER i,INOT
      INTEGER ND

!     --------------------------------------------

      ND=NDIM
      CALL STOREXRN(X,ND)

      FXN=0d0
      IF (IRECORD .EQ. 1) NIN=NIN+1

      DO i=1,NPROC
        FXNi(i)=0d0
      ENDDO

      IF (IPROC .GT. NPROC) THEN
        PRINT 1301,IPROC
        RETURN
      ENDIF

      CALL GENMOM(WTPS)
      IF (WTPS .EQ. 0d0) RETURN

      CALL PSCUT(INOT)
      IF (INOT .EQ. 1) RETURN

      CALL MSQ(WTME)

      IF (IRECORD .EQ. 1)  NOUT=NOUT+1
      FXNi(IPROC)=WTME*WTPS*fact(IPROC)*WGT
      FXN=FXNi(IPROC)

      IF (INITGRID .EQ. 0) THEN
        SIG(IPROC)=SIG(IPROC)+FXNi(IPROC)*WT_ITMX
        SIG2(IPROC)=SIG2(IPROC)+FXNi(IPROC)**2*WT_ITMX
        ERR(IPROC)=SQRT(SIG2(IPROC)-SIG(IPROC)**2/FLOAT(NIN))*WT_ITMX
      ENDIF

      IF (IHISTO .EQ. 1) CALL ADDEV(FXN*WT_ITMX)
      IF (IWRITE .EQ. 1) CALL EVTOUT(0)
      FXN=FXN/WGT
      RETURN
1301  FORMAT ('Warning: wrong IPROC = ',I2,' found, skipping...')
      END



      SUBROUTINE GENMOM(WTPS)
      IMPLICIT NONE

!     Arguments

      REAL*8 WTPS

!     Multigrid integration: process selection done by VEGAS

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mE,GE
      COMMON /Smass/ mE,GE
      REAL*8 NGE,NGW,NGZ
      COMMON /BWdata/ NGE,NGW,NGZ
      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 WTT1(3),WTT2(3),WTFL(3),WTZ(4)
      REAL*8 BRT1ac(0:3),BRT2ac(0:3),BRFLac(0:3),BRZac(0:4)
      COMMON /DECCH/ WTT1,WTT2,WTFL,WTZ,BRT1ac,BRT2ac,BRFLac,BRZac

!     External functions used

      REAL*8 XRN,RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Outputs

      INTEGER IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Sflags/ IQ,IL1,IL2,IB1,IB2,IF1,IF2
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl1(0:3),Pf1(0:3),Pfb1(0:3),Pl2(0:3),Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pl1,Pf1,Pfb1,Pl2,Pf2,Pfb2
      REAL*8 PT1(0:3),PT2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT/ PT1,PT2,PB1,PB2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 countB(3,3),countL(3,3)
      COMMON /testdec/ countB,countL

!     Local variables

      REAL*8 EPS,y1,y2,flux
      REAL*8 PCM_LAB(0:3)
      REAL*8 QT1,QT2,QB1,QB2,m1,m2
      REAL*8 MB1,GB1,NGB1,MB2,GB2,NGB2
      REAL*8 WT1,WT2,WT3,WT4,WT_BREIT,WT_PDF,WT_PROD,WT_DEC,WTFS
      REAL*8 RCH,ICH

!     --------------------------------------------

      WTPS=0d0

      IDIR=0
      IF (IPROC .GT. 4) IDIR=1
      IF ((IPROC .EQ. 1) .OR. (IPROC .EQ. 3) .OR. (IPROC .EQ. 5)
     &   .OR. (IPROC .EQ. 7)) THEN
        IQ=1        !  u u~  c c~ for IP=1  /  u d~   c s~ for IP=2 (not used)
      ELSE
        IQ=2        !  d d~  s s~ for IP=1  /  d u~   s c~ for IP=2 (not used)
      ENDIF

!     Select final state

      WTFS=1d0

!     Lepton flavours from T1,T2

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRFLac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IL1=ICH

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRFLac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IL2=ICH

      WTFS=WTFS*WTFL(IL1)*WTFL(IL2)

!     Bosons from T1, T2

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT1ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB1=ICH

      RCH=RAN2(idum)
      ICH=0
      DO WHILE (BRT2ac(ICH) .LT. RCH)
        ICH=ICH+1
      ENDDO
      IB2=ICH

      WTFS=WTFS*WTT1(IB1)*WTT2(IB2)

!     Fermions from B1,B2 decay (for W and Z)

      IF (IB1 .EQ. 1) THEN
        IF1=1
        WTFS=WTFS*9d0
      ELSE IF (IB1 .EQ. 2) THEN
        RCH=RAN2(idum)
        ICH=0
        DO WHILE (BRZac(ICH) .LT. RCH)
          ICH=ICH+1
        ENDDO
        IF1=ICH-1
        WTFS=WTFS*WTZ(ICH)
      ENDIF

      IF (IB2 .EQ. 1) THEN
        IF2=1
        WTFS=WTFS*9d0
      ELSE IF (IB2 .EQ. 2) THEN
        RCH=RAN2(idum)
        ICH=0
        DO WHILE (BRZac(ICH) .LT. RCH)
          ICH=ICH+1
        ENDDO
        IF2=ICH-1
        WTFS=WTFS*WTZ(ICH)
      ENDIF

      countB(IB1,IB2)=countB(IB1,IB2)+1d0
      countL(IL1,IL2)=countL(IL1,IL2)+1d0

!     Select masses of final state

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        NGB1=NGW
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        NGB1=NGZ
      ELSE IF (IB1 .EQ. 3) THEN
        QB1=MH
      ELSE
        PRINT *,'IB1 = ',IB1,' in genmom'
        STOP
      ENDIF

      IF (IB2 .EQ. 1) THEN
        MB2=MW
        GB2=GW
        NGB2=NGW
      ELSE IF (IB2 .EQ. 2) THEN
        MB2=MZ
        GB2=GZ
        NGB2=NGZ
      ELSE IF (IB2 .EQ. 3) THEN
        QB2=MH
      ELSE
        PRINT *,'IB2 = ',IB2,' in genmom'
        STOP
      ENDIF

!     Generation of the masses of the virtual particles

      CALL BREIT(mE,GE,NGE,QT1,WT1)
      CALL BREIT(mE,GE,NGE,QT2,WT2)
      WT_BREIT=WT1*WT2*(2d0*pi)**6
      IF (IB1 .NE. 3) THEN
        CALL BREIT(MB1,GB1,NGB1,QB1,WT3)
        WT_BREIT=WT_BREIT*WT3*(2d0*pi)**3
      ENDIF
      IF (IB2 .NE. 3) THEN
        CALL BREIT(MB2,GB2,NGB2,QB2,WT4)
        WT_BREIT=WT_BREIT*WT4*(2d0*pi)**3
      ENDIF
      IF (QB1 .GT. QT1) RETURN
      IF (QB2 .GT. QT2) RETURN

c      print *,il1,il2
c      print *,ib1,ib2
c      print *,QT1,QT2,qb1,qb2

!     Generation of the momentum fractions x1, x2

      y1=XRN(0)
      y2=XRN(0)

      EPS=(QT1+QT2)**2/ET**2
      x1=EPS**(y2*y1)                                                 
      x2=EPS**(y2*(1d0-y1))
      WT_PDF=y2*EPS**y2*LOG(EPS)**2                                    
      
      s=x1*x2*ET**2
      flux=2d0*s

!     Initial parton momenta in LAB system

      m1=0d0
      m2=0d0
      
      Q1(3)=ET*x1/2d0
      Q1(1)=0d0
      Q1(2)=0d0
      Q1(0)=SQRT(Q1(3)**2+m1**2)
      IF (Q1(0) .GE. ET/2d0) RETURN

      Q2(3)=-ET*x2/2d0
      Q2(1)=0d0
      Q2(2)=0d0
      Q2(0)=SQRT(Q2(3)**2+m2**2)
      IF (Q2(0) .GE. ET/2d0) RETURN

      PCM_LAB(0)=Q1(0)+Q2(0)
      PCM_LAB(1)=0d0
      PCM_LAB(2)=0d0
      PCM_LAB(3)=Q1(3)+Q2(3)

!     Generation

      CALL PHASE2sym(SQRT(s),QT1,QT2,PCM_LAB,PT1,PT2,WT1)  ! T1 = E+ T2 = E-
      WT_PROD=(2d0*pi)**4*WT1

c      print *,PT1,SQRT(DOT(PT1,PT1))
c      print *,PT2,SQRT(DOT(PT2,PT2))

!     T1 (E+) decay 

      CALL PHASE2(QT1,0d0,QB1,PT1,Pl1,PB1,WT1)

c      print *,Pl1,DOT(Pl1,Pl1)
c      print *,PB1,SQRT(DOT(PB1,PB1))

!     T2 (E-) decay 

      CALL PHASE2(QT2,0d0,QB2,PT2,Pl2,PB2,WT2)

c      print *,Pl2,DOT(Pl2,Pl2)
c      print *,PB2,SQRT(DOT(PB2,PB2))

!     B1 decay, if W,Z

      WT3=1d0
      IF (IB1 .NE. 3) CALL PHASE2(QB1,0d0,0d0,PB1,Pf1,Pfb1,WT3)

c      IF (IB1 .NE. 3) THEN
c      print *,pf1,DOT(Pf1,Pf1)
c      print *,pfb1,DOT(Pfb1,Pfb1)
c      ENDIF

      WT4=1d0
      IF (IB2 .NE. 3) CALL PHASE2(QB2,0d0,0d0,PB2,Pf2,Pfb2,WT4)

c      IF (IB2 .NE. 3) THEN
c      print *,pf2,DOT(Pf2,Pf2)
c      print *,pfb2,DOT(Pfb2,Pfb2)
c      ENDIF

      WT_DEC=WT1*WT2*WT3*WT4

      WTPS=WT_BREIT*WT_PROD*WT_DEC*WT_PDF*WTFS/flux
      
c      print *,'WT = ',WTPS
c      STOP
      RETURN
      END




      SUBROUTINE PSCUT(INOT)
      IMPLICIT NONE

!     Arguments

      INTEGER INOT

!     Data needed

      REAL*8 Pl1(0:3),Pf1(0:3),Pfb1(0:3),Pl2(0:3),Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pl1,Pf1,Pfb1,Pl2,Pf2,Pfb2
      REAL*8 PT1(0:3),PT2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT/ PT1,PT2,PB1,PB2
      INTEGER ICUT
      COMMON /CUTFLAGS/ ICUT

!     External functions used

c      REAL*8 RLEGO,RAP

!     Local

!     --------------------------------------------

      INOT=1

98    INOT=0

      RETURN
      END




      SUBROUTINE MSQ(WTME)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     External functions

      REAL*8 EE_BB,EE_BH,EE_HB,EE_HH

!     Arguments (= output)

      REAL*8 WTME

!     Multigrid integration

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     Data needed

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mL,GE                                            ! Renamed!!!
      COMMON /Smass/ mL,GE
      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl1(0:3),Pf1(0:3),Pfb1(0:3),Pl2(0:3),Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pl1,Pf1,Pfb1,Pl2,Pf2,Pfb2
      REAL*8 PT1(0:3),PT2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT/ PT1,PT2,PB1,PB2
      INTEGER IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Sflags/ IQ,IL1,IL2,IB1,IB2,IF1,IF2
      INTEGER nhel8(NPART,3**NPART),nhel6(NPART,3**NPART),
     &        nhel4(NPART,3**NPART)
      LOGICAL GOODHEL(MAXAMP,3**NPART)
      INTEGER ncomb8,ncomb6,ncomb4,ntry(MAXAMP)
      COMMON /HELI/ nhel8,nhel6,nhel4,GOODHEL,
     &  ncomb8,ncomb6,ncomb4,ntry
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      REAL*8 QFAC
      COMMON /PDFSCALE/ QFAC

!     Local variables

      REAL*8 fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1
      REAL*8 fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2
      INTEGER i,j,k,ISTAT,IAMP,ncomb
      REAL*8 M1,ME,STR(NPROC)

!     --------------------------------------------

      WTME=0d0

!     Scale for structure functions

      Q=ML*QFAC
     
      CALL GETPDF(x1,Q,fu1,fd1,fus1,fds1,fs1,fc1,fb1,fg1,ISTAT)
      IF (ISTAT .LT. 0) RETURN
      CALL GETPDF(x2,Q,fu2,fd2,fus2,fds2,fs2,fc2,fb2,fg2,ISTAT)
      IF (ISTAT .LT. 0) RETURN

c      IF (IP .EQ. 1) THEN
        IF (IPPBAR .EQ. 0) THEN
          STR(1)=fu1*fus2         ! u  u~ 
          STR(2)=fd1*fds2         ! d  d~
          STR(5)=fus1*fu2         ! u~ u
          STR(6)=fds1*fd2         ! d~ d
        ELSE
          STR(1)=fu1*fu2          ! u  u~ 
          STR(2)=fd1*fd2          ! d  d~
          STR(5)=fus1*fus2        ! u~ u
          STR(6)=fds1*fds2        ! d~ d
        ENDIF
        STR(3)=fc1*fc2            ! c  c~
        STR(4)=fs1*fs2            ! s  s~
        STR(7)=fc1*fc2            ! c~ c
        STR(8)=fs1*fs2            ! s~ s
c      ELSE
c        IF (IPPBAR .EQ. 0) THEN
c          STR(1)=fu1*fds2         ! u  d~ 
c          STR(2)=fd1*fus2         ! d  u~ 
c          STR(5)=fds1*fu2         ! d~ u 
c          STR(6)=fus1*fd2         ! u~ d   
c        ELSE
c          STR(1)=fu1*fd2          ! u  d~ 
c          STR(2)=fd1*fu2          ! d  u~ 
c          STR(5)=fds1*fus2        ! d~ u 
c          STR(6)=fus1*fds2        ! u~ d   
c        ENDIF
c        STR(3)=fc1*fs2            ! c  s~ 
c        STR(4)=fs1*fc2            ! s  c~ 
c        STR(7)=fs1*fc2            ! s~ c 
c        STR(8)=fc1*fs2            ! c~ s   
c      ENDIF

!     Select helicity amplitude and process

      i=IB1
      j=IB2
      IAMP=i+3*(j-1)+9*(IPROC-1)
      ntry(IAMP)=ntry(IAMP)+1
      IF (ntry(IAMP) .GT. 200) ntry(IAMP)=200

c      PRINT *,'IAMP = ',IAMP,' ntry = ',ntry(IAMP)

      IF ((IB1 .NE. 3) .AND. (IB2 .NE. 3)) THEN
        ncomb=ncomb8
      ELSE IF ((IB1 .EQ. 3) .AND. (IB2 .EQ. 3)) THEN
        ncomb=ncomb4
      ELSE
        ncomb=ncomb6
      ENDIF

c      print *,'ncomb = ',ncomb

      IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      ME=0d0
      DO k=1,ncomb
        IF (GOODHEL(IAMP,k) .OR. ntry(IAMP) .LT. 100) THEN
          IF ((IB1 .NE. 3) .AND. (IB2 .NE. 3)) THEN
            M1=EE_BB(Q1,Q2,Pl1,Pf1,Pfb1,Pl2,Pf2,Pfb2,nhel8(1,k))
          ELSE IF ((IB1 .NE. 3) .AND. (IB2 .EQ. 3)) THEN
            M1=EE_BH(Q1,Q2,Pl1,Pf1,Pfb1,Pl2,PB2,nhel6(1,k))
          ELSE IF ((IB1 .EQ. 3) .AND. (IB2 .NE. 3)) THEN
            M1=EE_HB(Q1,Q2,Pl1,PB1,Pl2,Pf2,Pfb2,nhel6(1,k))
          ELSE
            M1=EE_HH(Q1,Q2,Pl1,PB1,Pl2,PB2,nhel4(1,k))
          ENDIF
          ME=ME+M1
          IF(M1 .GT. 0d0 .AND. .NOT. GOODHEL(IAMP,k)) THEN
            GOODHEL(IAMP,k)=.TRUE.
          ENDIF
        ENDIF
      ENDDO
      ME=ME/4d0

20    IF (IDIR .EQ. 1) CALL EXCHANGE(Q1,Q2)

      WTME=ME*STR(IPROC)

c      PRINT *,'ME = ',ME
c      STOP

      RETURN
      END

