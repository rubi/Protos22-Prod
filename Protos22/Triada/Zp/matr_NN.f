      REAL*8 FUNCTION NN_BB(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> Nbar N with decay to W/Z & W/Z
!     Fermion flux assigned so that first N is "Nbar"

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),P3b(0:3),P3c(0:3)
      REAL*8 P4a(0:3),P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VlN(3)
      COMMON /Ncoup/ VlN
      INTEGER IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      REAL*8 MZP,GZP
      COMMON /ZPmass/ MZP,GZP
      REAL*8 gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN
      COMMON /ZPcoup/ gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GZPuu(2),GZPdd(2),GZPNN(2)
      REAL*8 GWlN(2),GWlN_I(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZPqq(2)
      REAL*8 MB1,GB1,MB2,GB2
      REAL*8 GB1ff(2),GB2ff(2),G3B1(2),G3B2(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z' coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZPqq(1)=GZPuu(1)
        GZPqq(2)=GZPuu(2)
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZPqq(1)=GZPdd(1)
        GZPqq(2)=GZPdd(2)
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        IF (ILNV1 .EQ. 0) THEN
          G3B1(1)=GWlN(1)
          G3B1(2)=GWlN(2)
        ELSE
          G3B1(1)=GWlN_I(1)
          G3B1(2)=GWlN_I(2)
        ENDIF
        GB1ff(1)=GWF(1)
        GB1ff(2)=GWF(2)
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        G3B1(1)=GZNv(1)
        G3B1(2)=GZNv(2)
        IF (IF1 .EQ. 0) THEN
          GB1ff(1)=GZvv(1)
          GB1ff(2)=GZvv(2)
        ELSE IF (IF1 .EQ. 1) THEN
          GB1ff(1)=GZll(1)
          GB1ff(2)=GZll(2)
        ELSE IF (IF1 .EQ. 2) THEN
          GB1ff(1)=GZuu(1)
          GB1ff(2)=GZuu(2)
        ELSE IF (IF1 .EQ. 3) THEN
          GB1ff(1)=GZdd(1)
          GB1ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF1 = ',IF1
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,'in NN_BB'
        STOP
      ENDIF

      IF (IB2 .EQ. 1) THEN
        MB2=MW
        GB2=GW
        IF (ILNV2 .EQ. 0) THEN
          G3B2(1)=GWlN(1)
          G3B2(2)=GWlN(2)
        ELSE
          G3B2(1)=GWlN_I(1)
          G3B2(2)=GWlN_I(2)
        ENDIF
        GB2ff(1)=GWF(1)
        GB2ff(2)=GWF(2)
      ELSE IF (IB2 .EQ. 2) THEN
        MB2=MZ
        GB2=GZ
        G3B2(1)=GZNv(1)
        G3B2(2)=GZNv(2)
        IF (IF2 .EQ. 0) THEN
          GB2ff(1)=GZvv(1)
          GB2ff(2)=GZvv(2)
        ELSE IF (IF2 .EQ. 1) THEN
          GB2ff(1)=GZll(1)
          GB2ff(2)=GZll(2)
        ELSE IF (IF2 .EQ. 2) THEN
          GB2ff(1)=GZuu(1)
          GB2ff(2)=GZuu(2)
        ELSE IF (IF2 .EQ. 3) THEN
          GB2ff(1)=GZdd(1)
          GB2ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF2 = ',IF2
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,'in NN_BB'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar

!     B1 decay products

      CALL OXXXXX(P3b,0d0,NHEL(4),1,W3b)       ! f1:    u, nu  / q, l-, nu
      CALL IXXXXX(P3c,0d0,NHEL(5),-1,W3c)      ! f1bar: d~, l+ / q~, l+, nu~ 
      CALL JIOXXX(W3c,W3b,GB1ff,MB1,GB1,W3d)   ! W- / Z

!     Nbar: fermion flow as l+ / nubar

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! l+ / nubar
      CALL FVIXXX(W3a,W3d,G3B1,mN,GN,W3)       ! 'Nbar'

!     ------------------------------------------------

!     B2 decay products

      CALL OXXXXX(P4b,0d0,NHEL(7),1,W4b)       ! f2:    d, l-  / q, l-, nu
      CALL IXXXXX(P4c,0d0,NHEL(8),-1,W4c)      ! f2bar: u~,nu~ / q~, l+, nu~
      CALL JIOXXX(W4c,W4b,GB2ff,MB2,GB2,W4d)   ! W+ / Z

!     N: fermion flow as l- / nu

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)       ! l- / nu
      CALL FVOXXX(W4a,W4d,G3B2,mN,GN,W4)       ! N

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZPqq,MZP,GZP,W5)
      CALL IOVXXX(W3,W4,W5,GZPNN,AMP(1))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)

      NN_BB = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          NN_BB = NN_BB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


      REAL*8 FUNCTION NN_HB(P1,P2,P3a,PH,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> Nbar N with decay to H & W/Z
!     Fermion flux assigned so that first N is "Nbar"

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),PH(0:3)
      REAL*8 P4a(0:3),P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VlN(3)
      COMMON /Ncoup/ VlN
      INTEGER IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      REAL*8 MZP,GZP
      COMMON /ZPmass/ MZP,GZP
      REAL*8 gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN
      COMMON /ZPcoup/ gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GZPuu(2),GZPdd(2),GZPNN(2)
      REAL*8 GWlN(2),GWlN_I(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZPqq(2)
      REAL*8 MB2,GB2
      REAL*8 GB2ff(2),G3B2(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z' coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZPqq(1)=GZPuu(1)
        GZPqq(2)=GZPuu(2)
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZPqq(1)=GZPdd(1)
        GZPqq(2)=GZPdd(2)
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .NE. 3) THEN
        PRINT *,'Wrong IB1 = ',IB1,' in NN_HB'
        STOP
      ENDIF

      IF (IB2 .EQ. 1) THEN
        MB2=MW
        GB2=GW
        IF (ILNV2 .EQ. 0) THEN
          G3B2(1)=GWlN(1)
          G3B2(2)=GWlN(2)
        ELSE
          G3B2(1)=GWlN_I(1)
          G3B2(2)=GWlN_I(2)
        ENDIF
        GB2ff(1)=GWF(1)
        GB2ff(2)=GWF(2)
      ELSE IF (IB2 .EQ. 2) THEN
        MB2=MZ
        GB2=GZ
        G3B2(1)=GZNv(1)
        G3B2(2)=GZNv(2)
        IF (IF2 .EQ. 0) THEN
          GB2ff(1)=GZvv(1)
          GB2ff(2)=GZvv(2)
        ELSE IF (IF2 .EQ. 1) THEN
          GB2ff(1)=GZll(1)
          GB2ff(2)=GZll(2)
        ELSE IF (IF2 .EQ. 2) THEN
          GB2ff(1)=GZuu(1)
          GB2ff(2)=GZuu(2)
        ELSE IF (IF2 .EQ. 3) THEN
          GB2ff(1)=GZdd(1)
          GB2ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF2 = ',IF2
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,'in NN_HB'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar

!     Nbar: fermion flow as l+ / nubar

      CALL SXXXXX(PH,1,W3d)                      ! H
      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! l+ / nu
      CALL FSIXXX(W3a,W3d,GHvN,mN,GN,W3)       ! N (v in N out) 

!     ------------------------------------------------

!     B2 decay products

      CALL OXXXXX(P4b,0d0,NHEL(5),1,W4b)       ! f2:    d, l-  / q, l-, nu
      CALL IXXXXX(P4c,0d0,NHEL(6),-1,W4c)      ! f2bar: u~,nu~ / q~, l+, nu~
      CALL JIOXXX(W4c,W4b,GB2ff,MB2,GB2,W4d)   ! W+ / Z

!     N: fermion flow as l- / nu

      CALL OXXXXX(P4a,0d0,NHEL(4),1,W4a)       ! l- / nu
      CALL FVOXXX(W4a,W4d,G3B2,mN,GN,W4)       ! N

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZPqq,MZP,GZP,W5)
      CALL IOVXXX(W3,W4,W5,GZPNN,AMP(1))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)

      NN_HB = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          NN_HB = NN_HB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END



      REAL*8 FUNCTION NN_BH(P1,P2,P3a,P3b,P3c,P4a,PH,NHEL)

!     FOR PROCESS : q q~  -> Nbar N with decay to W/Z & H
!     Fermion flux assigned so that first N is "Nbar"

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),P3b(0:3),P3c(0:3)
      REAL*8 P4a(0:3),PH(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VlN(3)
      COMMON /Ncoup/ VlN
      INTEGER IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      REAL*8 MZP,GZP
      COMMON /ZPmass/ MZP,GZP
      REAL*8 gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN
      COMMON /ZPcoup/ gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GZPuu(2),GZPdd(2),GZPNN(2)
      REAL*8 GWlN(2),GWlN_I(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZPqq(2)
      REAL*8 MB1,GB1
      REAL*8 GB1ff(2),G3B1(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z' coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZPqq(1)=GZPuu(1)
        GZPqq(2)=GZPuu(2)
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZPqq(1)=GZPdd(1)
        GZPqq(2)=GZPdd(2)
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        IF (ILNV1 .EQ. 0) THEN
          G3B1(1)=GWlN(1)
          G3B1(2)=GWlN(2)
        ELSE
          G3B1(1)=GWlN_I(1)
          G3B1(2)=GWlN_I(2)
        ENDIF
        GB1ff(1)=GWF(1)
        GB1ff(2)=GWF(2)
      ELSE IF (IB1 .EQ. 2) THEN
        MB1=MZ
        GB1=GZ
        G3B1(1)=GZNv(1)
        G3B1(2)=GZNv(2)
        IF (IF1 .EQ. 0) THEN
          GB1ff(1)=GZvv(1)
          GB1ff(2)=GZvv(2)
        ELSE IF (IF1 .EQ. 1) THEN
          GB1ff(1)=GZll(1)
          GB1ff(2)=GZll(2)
        ELSE IF (IF1 .EQ. 2) THEN
          GB1ff(1)=GZuu(1)
          GB1ff(2)=GZuu(2)
        ELSE IF (IF1 .EQ. 3) THEN
          GB1ff(1)=GZdd(1)
          GB1ff(2)=GZdd(2)
        ELSE
          PRINT *,'Wrong IF1 = ',IF1
          STOP
        ENDIF
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,'in NN_BH'
        STOP
      ENDIF

      IF (IB2 .NE. 3) THEN
        PRINT *,'Wrong IB2 = ',IB2,' in NN_BH'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar

!     B1 decay products

      CALL OXXXXX(P3b,0d0,NHEL(4),1,W3b)       ! f1:    u, nu  / q, l-, nu
      CALL IXXXXX(P3c,0d0,NHEL(5),-1,W3c)      ! f1bar: d~, l+ / q~, l+, nu~ 
      CALL JIOXXX(W3c,W3b,GB1ff,MB1,GB1,W3d)   ! W- / Z

!     Nbar: fermion flow as l+ / nubar

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! l+ / nubar
      CALL FVIXXX(W3a,W3d,G3B1,mN,GN,W3)       ! 'Nbar'

!     ------------------------------------------------

!     N: fermion flow as l- / nu

      CALL SXXXXX(PH,1,W4d)                    ! H
      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)       ! l- / nu
      CALL FSOXXX(W4a,W4d,GHNv,mN,GN,W4)       ! N (N in v out) 

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZPqq,MZP,GZP,W5)
      CALL IOVXXX(W3,W4,W5,GZPNN,AMP(1))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)

      NN_BH = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          NN_BH = NN_BH + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END


      REAL*8 FUNCTION NN_HH(P1,P2,P3a,PH1,P4a,PH2,NHEL)

!     FOR PROCESS : q q~  -> Nbar N with decay to H & H
!     Fermion flux assigned so that first N is "Nbar"

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),PH1(0:3)
      REAL*8 P4a(0:3),PH2(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3d(6)
      COMPLEX*16 W4a(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mN,GN
      COMMON /Nmass/ mN,GN
      REAL*8 VlN(3)
      COMMON /Ncoup/ VlN
      INTEGER IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      COMMON /Nflags/ IMA,IQ,IL1,IL2,IB1,IB2,IF1,IF2,ILNV1,ILNV2
      REAL*8 MZP,GZP
      COMMON /ZPmass/ MZP,GZP
      REAL*8 gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN
      COMMON /ZPcoup/ gp,cLu,cRu,cLd,cRd,cLe,cRe,cLv,cRv,cLN,cRN

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GZPuu(2),GZPdd(2),GZPNN(2)
      REAL*8 GWlN(2),GWlN_I(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZPqq(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z' coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZPqq(1)=GZPuu(1)
        GZPqq(2)=GZPuu(2)
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZPqq(1)=GZPdd(1)
        GZPqq(2)=GZPdd(2)
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .NE. 3) THEN
        PRINT *,'Wrong IB1 = ',IB1,' in NN_HH'
        STOP
      ENDIF

      IF (IB2 .NE. 3) THEN
        PRINT *,'Wrong IB2 = ',IB2,' in NN_HH'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar

!     Nbar: fermion flow as l+ / nubar

      CALL SXXXXX(PH1,1,W3d)                   ! H
      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! l+ / nu
      CALL FSIXXX(W3a,W3d,GHvN,mN,GN,W3)       ! N (v in N out) 

!     ------------------------------------------------

!     N: fermion flow as l- / nu

      CALL SXXXXX(PH2,1,W4d)                   ! H
      CALL OXXXXX(P4a,0d0,NHEL(4),1,W4a)       ! l- / nu
      CALL FSOXXX(W4a,W4d,GHNv,mN,GN,W4)       ! N (N in v out) 

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZPqq,MZP,GZP,W5)
      CALL IOVXXX(W3,W4,W5,GZPNN,AMP(1))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)

      NN_HH = 0D0 
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          NN_HH = NN_HH + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

