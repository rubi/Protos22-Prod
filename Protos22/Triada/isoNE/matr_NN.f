      REAL*8 FUNCTION NN_BB(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,NHEL)

!     FOR PROCESS : q q~  -> Nbar N with decay to W & W

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEIGEN,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEIGEN=1,NEXTERNAL=8)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3)
      REAL*8 P3a(0:3),P3b(0:3),P3c(0:3)
      REAL*8 P4a(0:3),P4b(0:3),P4c(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      REAL*8 EIGEN_VAL(NEIGEN), EIGEN_VEC(NGRAPHS,NEIGEN)
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mL,GE,GN
      COMMON /Dmass/ mL,GE,GN
      REAL*8 VlN(3)
      COMMON /Dcoup/ VlN
      INTEGER IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Dflags/ IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2)
      REAL*8 GWEN(2),GZEE(2),GZNN(2),GAEE(2)
      REAL*8 GWEv(2),GWlN(2),GZEl(2),GZNv(2),GZvN(2)
      REAL*8 cw,gcw,gcwsw2
      COMPLEX*16 GHEl(2),GHlE(2),GHNv(2),GHvN(2)

!     Specific masses and couplings

      REAL*8 GZqq(2),GAqq(2)
      REAL*8 MB1,GB1,MB2,GB2
      REAL*8 GB1ff(2),GB2ff(2),G3B1(2),G3B2(2)

!     COLOR DATA

      DATA EIGEN_VAL(1) /0.333333333d0/                  
      DATA EIGEN_VEC(1,1) /1d0 /                  

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      INCLUDE 'input/coupling.inc'

!     -------------------------
!     Select specific couplings
!     -------------------------

!     Z, gamma coupling to initial quarks

      IF (IQ .EQ. 1) THEN                   ! u,c
        GZqq(1)=GZuu(1)
        GZqq(2)=GZuu(2)
        GAqq(1)=-2d0/3d0*e
        GAqq(2)=-2d0/3d0*e
      ELSE IF (IQ .EQ. 2) THEN              ! d,s
        GZqq(1)=GZdd(1)
        GZqq(2)=GZdd(2)
        GAqq(1)=1d0/3d0*e
        GAqq(2)=1d0/3d0*e
      ELSE
        PRINT *,'Wrong IQ = ',IQ
        STOP
      ENDIF

!     Mass and SM coupling of the bosons

      IF (IB1 .EQ. 1) THEN
        MB1=MW
        GB1=GW
        G3B1(1)=GWlN(1)
        G3B1(2)=GWlN(2)
        GB1ff(1)=GWF(1)
        GB1ff(2)=GWF(2)
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in NN_BB'
        STOP
      ENDIF

      IF (IB2 .EQ. 1) THEN
        MB2=MW
        GB2=GW
        G3B2(1)=GWlN(1)
        G3B2(2)=GWlN(2)
        GB2ff(1)=GWF(1)
        GB2ff(2)=GWF(2)
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in NN_BB'
        STOP
      ENDIF

!     ------------------------------------------------

      CALL IXXXXX(P1,0d0,NHEL(1),1,W1)     ! q
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)    ! qbar
      
!     B1 decay products: W- -> d u~

      CALL OXXXXX(P3b,0d0,NHEL(4),1,W3b)         ! f1:    d, l-  / q, l-, nu
      CALL IXXXXX(P3c,0d0,NHEL(5),-1,W3c)        ! f1bar: u~,nu~ / q~, l+, nu~
      CALL JIOXXX(W3c,W3b,GB1ff,MB1,GB1,W3d)     ! W-

!     Nbar -> l+ W-

      CALL IXXXXX(P3a,0d0,NHEL(3),-1,W3a)      ! l+
      CALL FVIXXX(W3a,W3d,G3B1,mL,GN,W3)       ! Nbar

!     ------------------------------------------------

!     B2 decay products: W+ -> u d~
  
      CALL OXXXXX(P4b,0d0,NHEL(7),1,W4b)         ! f2:    u, nu  / q, l-, nu
      CALL IXXXXX(P4c,0d0,NHEL(8),-1,W4c)        ! f2bar: d~, l+ / q~, l+, nu~ 
      CALL JIOXXX(W4c,W4b,GB2ff,MB2,GB2,W4d)     ! W+

!     N -> l- W+

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)       ! l-
      CALL FVOXXX(W4a,W4d,G3B2,mL,GN,W4)       ! N

!     ------------------------------------------------

      CALL JIOXXX(W1,W2,GZqq,MZ,GZ,W5)
      CALL IOVXXX(W3,W4,W5,GZNN,AMP(1))

      AMP(1)=AMP(1)*Vln(IL1)*Vln(IL2)

      NN_BB = 0D0
      DO I = 1, NEIGEN
          ZTEMP = (0D0,0D0)
          DO J = 1, NGRAPHS
              ZTEMP = ZTEMP + EIGEN_VEC(J,I)*AMP(J)
          ENDDO
          NN_BB = NN_BB + ZTEMP*EIGEN_VAL(I)*CONJG(ZTEMP) 
      ENDDO
      END

