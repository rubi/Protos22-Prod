      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     External functions

      REAL*8 RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl1(0:3),Pf1(0:3),Pfb1(0:3),Pl2(0:3),Pf2(0:3),Pfb2(0:3)
      COMMON /MOMEXT/ Pl1,Pf1,Pfb1,Pl2,Pf2,Pfb2
      REAL*8 PT1(0:3),PT2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT/ PT1,PT2,PB1,PB2
      INTEGER IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      COMMON /Dflags/ IP,IQ,IL1,IL2,IB1,IB2,IF1,IF2
      REAL*8 x1,x2,s,Q
      INTEGER IDIR
      COMMON /miscdata/ x1,x2,s,Q,IDIR
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 mL,GE,GN
      COMMON /Dmass/ mL,GE,GN
      REAL*8 VlN(3)
      COMMON /Dcoup/ VlN
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_tot,ERR_tot
      COMMON /FINALSTATS/ SIG_tot,ERR_tot

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables

      INTEGER IDTAB1(8),IDTAB2(8)
      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2
      INTEGER i,IPX

      REAL*8 RCH
      INTEGER ICH
      INTEGER IDTABE(3,3),IDTABN(3,3),IDTABB1(3),IDTABB2(3)
      INTEGER IDTABW1(9),IDTABW2(9)
      INTEGER IDTABZN(3),IDTABZL(3),IDTABZU(2),IDTABZD(3)
      INTEGER IDB1,IDB2
      
      
!                    IP = 1,3      IP = 2
      DATA IDTAB1  / 2, 1, 4, 3,   2, 1, 4, 3/
      DATA IDTAB2  /-2,-1,-4,-3,  -1,-2,-3,-4/
      DATA IDTABE  /-12,-14,-16,-11,-13,-15,-11,-13,-15/    ! For E+ decays
      DATA IDTABN  / 11, 13, 15, 12, 14, 16, 12, 14, 16/    ! For N LNC decays 
      DATA IDTABB1 / 24,23,25/
      DATA IDTABB2 /-24,23,25/
      DATA IDTABW1 / 12, 14, 16, 2, 2, 2, 4, 4, 4/          ! For W+ decays
      DATA IDTABW2 /-11,-13,-15,-1,-1,-1,-3,-3,-3/
      DATA IDTABZN /12,14,16/
      DATA IDTABZL /11,13,15/
      DATA IDTABZU /2,4/
      DATA IDTABZD /1,3,5/

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0

      RETURN

10    IF (IMODE .NE. 0) GOTO 11

!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)/1000d0                          ! In pb
      IF (XWGTUP .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,XWGTUP)
      NUMEVT=NUMEVT+1

!     Flavour

      IPX=MOD(IPROC,5)+IPROC/5
      IF (IP .EQ. 2) IPX=IPX+4
      ID(1)=IDTAB1(IPX)
      ID(2)=IDTAB2(IPX)

c      print *,'IP = ',IP,'  IPROC = ',IPROC,'  IQ = ',IQ,
c     &  '  IB1 = ',IB1,'  IB2 = ',IB2,'  ILNV = ',ILNV
c      print *,'q = ',ID(1),'  qbar = ',ID(2)

      IF (IP .EQ. 1) THEN
        ID(3)=IDTABE(IL1,IB1)
        ID(6)=-IDTABE(IL2,IB2)
        IDB1=IDTABB1(IB1)
        IDB2=IDTABB2(IB2)
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 1)) THEN
        ID(3)=IDTABE(IL1,IB1)
        ID(6)=IDTABN(IL2,IB2)
        IDB1=IDTABB1(IB1)
        IDB2=IDTABB1(IB2)
      ELSE IF ((IP .EQ. 2) .AND. (IQ .EQ. 2)) THEN
        ID(3)=-IDTABE(IL1,IB1)
        ID(6)=-IDTABN(IL2,IB2)
        IDB1=IDTABB2(IB1)
        IDB2=IDTABB2(IB2)
      ELSE IF (IP .EQ. 3) THEN
        ID(3)=-IDTABN(IL1,IB1)
        ID(6)=IDTABN(IL2,IB2)
        IDB1=IDTABB2(IB1)
        IDB2=IDTABB1(IB2)
      ENDIF

c      print *,'l1 = ',ID(3),'  l2 = ',ID(6)
c      print *,'B1 = ',IDB1,'  B2 = ',IDB2

!     Fill default for E+ decay, change later if not

      RCH=RAN2(idum)
      IF (IB1 .EQ. 1) THEN
        ICH=INT(RCH*9d0)+1
        ID(4)=IDTABW1(ICH)     ! For W+, to be changed if W-
        ID(5)=IDTABW2(ICH)
      ELSE IF ((IB1 .EQ. 2) .AND. (IF1 .EQ. 0)) THEN
        ICH=INT(RCH*3d0)+1
        ID(4)=IDTABZN(ICH)
        ID(5)=-IDTABZN(ICH)
      ELSE IF ((IB1 .EQ. 2) .AND. (IF1 .EQ. 1)) THEN
        ICH=INT(RCH*3d0)+1
        ID(4)=IDTABZL(ICH)
        ID(5)=-IDTABZL(ICH)
      ELSE IF ((IB1 .EQ. 2) .AND. (IF1 .EQ. 2)) THEN
        ICH=INT(RCH*2d0)+1
        ID(4)=IDTABZU(ICH)
        ID(5)=-IDTABZU(ICH)
      ELSE IF ((IB1 .EQ. 2) .AND. (IF1 .EQ. 3)) THEN
        ICH=INT(RCH*3d0)+1
        ID(4)=IDTABZD(ICH)
        ID(5)=-IDTABZD(ICH)
      ELSE IF (IB1 .EQ. 3) THEN
        ID(4)=0
        ID(5)=0
      ENDIF

!     Change fermions from B1 if decay is E- -> nu W- or Nbar -> l+ W-

      IF (((IP .EQ. 2) .AND. (IQ .EQ. 2) .AND. (IB1 .EQ. 1))
     &  .OR. (IP .EQ. 3)) THEN
        IDUM=ID(4)
        ID(4)=-ID(5)
        ID(5)=-IDUM
      ENDIF

c      print *,'f1 = ',ID(4),'  f1b = ',ID(5)

!     Fill default for E- decay, change later if not

      RCH=RAN2(idum)
      IF (IB2 .EQ. 1) THEN
        ICH=INT(RCH*9d0)+1
        ID(7)=-IDTABW2(ICH)      ! For W-, to be changed if W+
        ID(8)=-IDTABW1(ICH)
      ELSE IF ((IB2 .EQ. 2) .AND. (IF2 .EQ. 0)) THEN
        ICH=INT(RCH*3d0)+1
        ID(7)=IDTABZN(ICH)
        ID(8)=-IDTABZN(ICH)
      ELSE IF ((IB2 .EQ. 2) .AND. (IF2 .EQ. 1)) THEN
        ICH=INT(RCH*3d0)+1
        ID(7)=IDTABZL(ICH)
        ID(8)=-IDTABZL(ICH)
      ELSE IF ((IB2 .EQ. 2) .AND. (IF2 .EQ. 2)) THEN
        ICH=INT(RCH*2d0)+1
        ID(7)=IDTABZU(ICH)
        ID(8)=-IDTABZU(ICH)
      ELSE IF ((IB2 .EQ. 2) .AND. (IF2 .EQ. 3)) THEN
        ICH=INT(RCH*3d0)+1
        ID(7)=IDTABZD(ICH)
        ID(8)=-IDTABZD(ICH)
      ELSE IF (IB2 .EQ. 3) THEN
        ID(7)=0
        ID(8)=0
      ENDIF

!     Change fermions in N decay, if needed

      IF ( ((IP .EQ. 2) .AND. (IQ .EQ. 1) .AND. (IB2 .EQ. 1)) 
     &  .OR. (IP .EQ. 3)) THEN
        IDUM=ID(7)
        ID(7)=-ID(8)
        ID(8)=-IDUM
      ENDIF

c      print *,'f2 = ',ID(7),'  f2b = ',ID(8)
c      print *,'----------------'
c      STOP

!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

      DO i=1,2
        IF (ID(i) .GT. 0) THEN         ! Initial q / qbar and b / bbar
           ICOL1(i)=1
        ELSE
           ICOL2(i)=1
        ENDIF
      ENDDO

      IF (ABS(ID(4)) .LT. 10) THEN 
        DO i=4,5
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=2
          ELSE
            ICOL2(i)=2
          ENDIF
        ENDDO
      ENDIF

      IF (ABS(ID(7)) .LT. 10) THEN 
        DO i=7,8
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=3
          ELSE
            ICOL2(i)=3
          ENDIF
        ENDDO
      ENDIF

      IF (IDIR .EQ. 0) THEN
        Pz1=Q1(3)
        Pz2=Q2(3)
      ELSE
        Pz1=Q2(3)
        Pz2=Q1(3)
      ENDIF
      
      WRITE (35,3010) NUMEVT,XWGTUP,Q
      WRITE (35,3015) IDB1,IDB2
      IF (Pz1 .GT. 0d0) THEN
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      ELSE
        WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
        WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      ENDIF
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),Pl1(1),Pl1(2),Pl1(3)

      IF (IB1 .NE. 3) THEN
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),Pf1(1),Pf1(2),Pf1(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pfb1(1),Pfb1(2),Pfb1(3)
      ELSE
      WRITE (35,3030) IDB1,0,0,PB1(1),PB1(2),PB1(3)
      ENDIF

      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),Pl2(1),Pl2(2),Pl2(3)

      IF (IB2 .NE. 3) THEN
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),Pf2(1),Pf2(2),Pf2(3)
      WRITE (35,3030) ID(8),ICOL1(8),ICOL2(8),Pfb2(1),Pfb2(2),Pfb2(3)
      ELSE
      WRITE (35,3030) IDB2,0,0,PB2(1),PB2(2),PB2(3)
      ENDIF

      RETURN

      
      
11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 30+IP
      WRITE (36,4001) ET
      WRITE (36,4005) mL,GE,GN,MH
      WRITE (36,4006) VlN(1),VlN(2),VlN(3)
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_tot/1000d0,ERR_tot/1000d0
      CLOSE (36)
      RETURN



12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3015  FORMAT (I3,' ',I3)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)

4000  FORMAT (I2,'                       ',
     & '             ! Process code')
4001  FORMAT (F6.0,'                                ! CM energy')
4005  FORMAT (F6.1,'  ',D9.3,'  ',D9.3,'  ',F6.2,
     &  '  ! ML, GE, GN, MH')
4006  FORMAT (D9.3,'  ',D9.3,'  ',D9.3,
     &  '       ! VeN, VmN, VtN')
4015  FORMAT (I5,
     &  '                                 ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '                 ! Events generated, saved')
4030  FORMAT (D12.6,'                          ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '           ! total cross section and error')
      END

