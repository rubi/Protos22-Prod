!     W coupling to SM fermions

      GWF(1)=-g/SQRT(2d0)
      GWF(2)=0d0

!     Gluon coupling

      GG(1)=-gs
      GG(2)=-gs

!     Heavy quark couplings
      
      Xmix=Vmix

      GWtQ(1)=0d0
      GWtQ(2)=GWF(1)*Vmix

