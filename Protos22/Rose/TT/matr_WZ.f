      REAL*8 FUNCTION GG_WZ(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,Pf2,Pfb2,
     &  NHEL)

!     PROCESS : g g  -> T Tbar with decay to Wb(H) ZWb

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=3,NEXTERNAL=12)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3),Pf2(0:3),Pfb2(0:3)
      INTEGER NHEL(NEXTERNAL)

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)        
      COMPLEX*16 W6(6),W7(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)
      COMPLEX*16 Wt(6),Wtb(6),WH1(6),WZ2(6)
      COMPLEX*16 Wf2(6),Wf2b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IB1,IB2,IWF1,IWF2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IWF1,IWF2,IZF1,IZF2
      REAL*8 Pt1(0:3),Pt2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ Pt1,Pt2,PB1,PB2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=2)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),GZQt(2),Xmix
      COMPLEX*16 GHQt(2),GHtQ(2)

!     Specific couplings

      REAL*8 GB2ff(2)

!     Colour data
  
      DATA Denom(1) /3/                                       
      DATA (CF(i,1),i=1,2) /16,-2/                            
      DATA Denom(2) /3/                                       
      DATA (CF(i,2),i=1,2) /-2,16/                            

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (IZF2 .EQ. 0) THEN
        GB2ff(1)=GZvv(1)
        GB2ff(2)=GZvv(2)
      ELSE IF (IZF2 .EQ. 1) THEN
        GB2ff(1)=GZll(1)
        GB2ff(2)=GZll(2)
      ELSE IF (IZF2 .EQ. 2) THEN
        GB2ff(1)=GZuu(1)
        GB2ff(2)=GZuu(2)
      ELSE IF (IZF2 .EQ. 3) THEN
        GB2ff(1)=GZdd(1)
        GB2ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF2 = ',IZF2
        STOP
      ENDIF

!     Code

      CALL VXXXXX(P1,0d0,NHEL(1),-1,W1)                            
      CALL VXXXXX(P2,0d0,NHEL(2),-1,W2)                            

!     First heavy quark

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)                ! nu / q
      CALL IXXXXX(P3b,0d0,NHEL(4),-1,W3b)               ! e+ / qbar
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)                 ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)                ! W+

      IF (IB1 .EQ. 1) THEN                              ! T -> Wb
        CALL FVOXXX(W3c,W3d,GWQb,mQ,GQ,W3)
      ELSE IF (IB1 .EQ. 3) THEN                         ! T -> Ht
        CALL FVOXXX(W3c,W3d,GWF,mt,Gt,Wt)               ! t
        CALL SXXXXX(PB1,1,WH1)                          ! H
        CALL FSOXXX(Wt,WH1,GHQt,mQ,GQ,W3)               ! T
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in GG_WZ'
        STOP
      ENDIF

!     Second heavy quark

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)                ! e-  / q
      CALL IXXXXX(P4b,0d0,NHEL(7),-1,W4b)               ! nu~ / qbar
      CALL IXXXXX(P4c,mb,NHEL(8),-1,W4c)                ! bbar
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4d)                ! W-

      IF (IB2 .EQ. 2) THEN                              ! Tbar -> W bbar
        CALL FVIXXX(W4c,W4d,GWF,mt,Gt,Wtb)              ! tbar
        CALL OXXXXX(Pf2,0d0,NHEL(9),1,Wf2)              ! f2: q, l-, nu
        CALL IXXXXX(Pfb2,0d0,NHEL(10),-1,Wf2b)          ! f2bar: q~, l+, nu~ 
        CALL JIOXXX(Wf2b,Wf2,GB2ff,MZ,GZ,WZ2)           ! Z
        CALL FVIXXX(Wtb,WZ2,GZQt,mQ,GQ,W4)              ! Tbar
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in GG_WZ'
        STOP
      ENDIF

!     Amplitudes

      CALL FVOXXX(W3,W2,GG,mQ,GQ,W5)
      CALL IOVXXX(W4,W5,W1,GG,AMP(1))
      CALL FVOXXX(W3,W1,GG,mQ,GQ,W6)
      CALL IOVXXX(W4,W6,W2,GG,AMP(2))
      CALL JGGXXX(W1,W2,gs,W7)
      CALL IOVXXX(W4,W3,W7,GG,AMP(3))

      CAMP(1) = -AMP(1)+AMP(3)
      CAMP(2) = -AMP(2)-AMP(3)

      GG_WZ = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        GG_WZ = GG_WZ + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      GG_WZ = GG_WZ/64d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END


      REAL*8 FUNCTION UU_WZ(P1,P2,P3a,P3b,P3c,P4a,P4b,P4c,Pf2,Pfb2,
     &  NHEL)

!     FOR PROCESS : q qbar  -> T Tbar with decay to Wb(H) ZWb

      IMPLICIT NONE

!     CONSTANTS

      INTEGER NGRAPHS,NEXTERNAL
      PARAMETER (NGRAPHS=1,NEXTERNAL=12)

!     ARGUMENTS 

      REAL*8 P1(0:3),P2(0:3),P3a(0:3),P3b(0:3),P3c(0:3),P4a(0:3),
     &  P4b(0:3),P4c(0:3),Pf2(0:3),Pfb2(0:3)
      INTEGER NHEL(NEXTERNAL)                                                    

!     LOCAL VARIABLES 

      INTEGER I,J
      COMPLEX*16 ZTEMP
      COMPLEX*16 AMP(NGRAPHS)
      COMPLEX*16 W1(6),W2(6),W3(6),W4(6),W5(6)  
      COMPLEX*16 W3a(6),W3b(6),W3c(6),W3d(6)
      COMPLEX*16 W4a(6),W4b(6),W4c(6),W4d(6)
      COMPLEX*16 Wt(6),Wtb(6),WH1(6),WZ2(6)
      COMPLEX*16 Wf2(6),Wf2b(6)

!     External data

      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 mQ,GQ
      COMMON /Qmass/ mQ,GQ
      REAL*8 Vmix
      COMMON /Qcoup/ Vmix
      INTEGER IMOD,IB1,IB2,IWF1,IWF2,IZF1,IZF2
      COMMON /Qflags/ IMOD,IB1,IB2,IWF1,IWF2,IZF1,IZF2
      REAL*8 Pt1(0:3),Pt2(0:3),PB1(0:3),PB2(0:3)
      COMMON /MOMINT2/ Pt1,Pt2,PB1,PB2

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Colour factors

      INTEGER NCOLOR
      PARAMETER (NCOLOR=1)
      REAL*8 DENOM(NCOLOR),CF(NCOLOR,NCOLOR)
      COMPLEX*16 CAMP(NCOLOR)

!     General couplings

      REAL*8 GWF(2),GZuu(2),GZdd(2),GZvv(2),GZll(2),GG(2)
      REAL*8 cw,gcw,gcwsw2
      REAL*8 GWQb(2),GZQt(2),Xmix
      COMPLEX*16 GHQt(2),GHtQ(2)

!     Specific couplings

      REAL*8 GB2ff(2)

!     Colour data

      DATA Denom(1) /1/
      DATA (CF(i,1),i=1,1) /2/

      cw=SQRT(1d0-sw2)
      gcw=g/cw
      gcwsw2=gcw*sw2

!     ------------------------
!     Define general couplings
!     ------------------------

      include 'input/coupling.inc'

!     -------------------
!     Particular settings
!     -------------------

      IF (IZF2 .EQ. 0) THEN
        GB2ff(1)=GZvv(1)
        GB2ff(2)=GZvv(2)
      ELSE IF (IZF2 .EQ. 1) THEN
        GB2ff(1)=GZll(1)
        GB2ff(2)=GZll(2)
      ELSE IF (IZF2 .EQ. 2) THEN
        GB2ff(1)=GZuu(1)
        GB2ff(2)=GZuu(2)
      ELSE IF (IZF2 .EQ. 3) THEN
        GB2ff(1)=GZdd(1)
        GB2ff(2)=GZdd(2)
      ELSE
        PRINT *,'Wrong IZF2 = ',IZF2
        STOP
      ENDIF

!     Code

      CALL IXXXXX(P1,0d0,NHEL(1), 1,W1)                       
      CALL OXXXXX(P2,0d0,NHEL(2),-1,W2)                       

!     First heavy quark

      CALL OXXXXX(P3a,0d0,NHEL(3),1,W3a)                ! nu / q
      CALL IXXXXX(P3b,0d0,NHEL(4),-1,W3b)               ! e+ / qbar
      CALL OXXXXX(P3c,mb,NHEL(5),1,W3c)                 ! b
      CALL JIOXXX(W3b,W3a,GWF,MW,GW,W3d)                ! W+

      IF (IB1 .EQ. 1) THEN                              ! T -> Wb
        CALL FVOXXX(W3c,W3d,GWQb,mQ,GQ,W3)
      ELSE IF (IB1 .EQ. 3) THEN                         ! T -> Ht
        CALL FVOXXX(W3c,W3d,GWF,mt,Gt,Wt)               ! t
        CALL SXXXXX(PB1,1,WH1)                          ! H
        CALL FSOXXX(Wt,WH1,GHQt,mQ,GQ,W3)               ! T
      ELSE
        PRINT *,'Wrong IB1 = ',IB1,' in UU_WZ'
        STOP
      ENDIF

!     Second heavy quark

      CALL OXXXXX(P4a,0d0,NHEL(6),1,W4a)                ! e-  / q
      CALL IXXXXX(P4b,0d0,NHEL(7),-1,W4b)               ! nu~ / qbar
      CALL IXXXXX(P4c,mb,NHEL(8),-1,W4c)                ! bbar
      CALL JIOXXX(W4b,W4a,GWF,MW,GW,W4d)                ! W-

      IF (IB2 .EQ. 2) THEN                              ! Tbar -> W bbar
        CALL FVIXXX(W4c,W4d,GWF,mt,Gt,Wtb)              ! tbar
        CALL OXXXXX(Pf2,0d0,NHEL(9),1,Wf2)              ! f2: q, l-, nu
        CALL IXXXXX(Pfb2,0d0,NHEL(10),-1,Wf2b)          ! f2bar: q~, l+, nu~ 
        CALL JIOXXX(Wf2b,Wf2,GB2ff,MZ,GZ,WZ2)           ! Z
        CALL FVIXXX(Wtb,WZ2,GZQt,mQ,GQ,W4)              ! Tbar
      ELSE
        PRINT *,'Wrong IB2 = ',IB2,' in UU_WZ'
        STOP
      ENDIF

!     Amplitudes

      CALL JIOXXX(W1,W2,GG,0d0,0d0,W5)                             
      CALL IOVXXX(W4,W3,W5,GG,AMP(1))
      

      CAMP(1) = -AMP(1)

      UU_WZ = 0d0 
      DO I = 1, NCOLOR
        ZTEMP = (0d0,0d0)
        DO J = 1, NCOLOR
          ZTEMP = ZTEMP + CF(J,I)*CAMP(J)
        ENDDO
        UU_WZ = UU_WZ + ZTEMP*DCONJG(CAMP(I))/DENOM(I)   
      ENDDO
      UU_WZ = UU_WZ/9d0

      DO I = 1, NCOLOR
        CAMP2(ICSTR,i) = CAMP2(ICSTR,i) + CAMP(i)*dconjg(CAMP(i))
      ENDDO
      RETURN
      END

