      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER IMATCH
      COMMON /MATCH/ IMATCH

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      INTEGER IAN_PROD,IAN_DEC
      COMMON /ANFLAGS/ IAN_PROD,IAN_DEC
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      INTEGER IGEN
      COMMON /TMIX/ IGEN

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry

!     Local variables to be saved

      INTEGER ISC
      SAVE ISC

!     Local variables

      INTEGER i,j,IOK
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 xW,xb,EW,qW
      REAL*8 gevpb
      REAL*8 coup(4,3)
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)
!
!     Tweaks
!
      OPEN (31,file='input/template.ini',status='old')
      DO i=1,3
      READ (31,*) coup(1,i),coup(2,i),coup(3,i),coup(4,i)
      ENDDO
      CLOSE (31)

      IOK=1
      OPEN (31,file='input/tweak.dat',status='old')
      READ (31,*) IAN_PROD
      IF ((IAN_PROD .NE. 0) .AND. (IAN_PROD .NE. 1)) IOK=0
      READ (31,*) IAN_DEC
      IF ((IAN_DEC .NE. 0) .AND. (IAN_DEC .NE. 1)) IOK=0
      READ (31,*) ISC
      IF ((ISC .LT. 0) .OR. (ISC .GT. 3)) IOK=0
      READ (31,*) IGEN
      IF ((IGEN .LT. 1) .OR. (IGEN .GT. 3)) IOK=0
      CLOSE (31)

      IF (IOK .EQ. 0) THEN
        PRINT *,'Wrong tweak.dat'
        STOP
      ENDIF

      IF (ISC .GT. 0) THEN
        vl = coup(1,ISC)
        vr = coup(2,ISC)
        gl = coup(3,ISC)
        gr = coup(4,ISC)
      ENDIF

!     More info

      IF ((IAN_PROD .EQ. 0) .OR. (IAN_DEC .EQ. 0)) PRINT 1070
      IF (IAN_PROD .EQ. 0) PRINT 1050
      IF (IAN_DEC .EQ. 0) PRINT 1060     
      IF ((IAN_PROD .EQ. 0) .OR. (IAN_DEC .EQ. 0)) THEN
        PRINT 1070
        PRINT 999
      ENDIF
      IF (ISC .EQ. 1) PRINT 1200
      IF (ISC .EQ. 2) PRINT 1210
      IF (ISC .EQ. 3) PRINT 1220
      IF (ISC .NE. 0) THEN
        PRINT 1230
        PRINT 999
      ENDIF

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb                          ! Cross section in pb
        IF (ILEP .LE. 3) THEN
          fact(i)=fact(i)*12d0                 ! Semileptonic (lepton fixed)
        ELSE IF (ILEP .EQ. 4) THEN
          fact(i)=fact(i)*36d0                 ! Semileptonic (sum)
        ELSE IF (ILEP .EQ. 5) THEN
          fact(i)=fact(i)*36d0                 ! Full hadronic
        ELSE
          fact(i)=fact(i)*9d0                  ! Dileptonic
        ENDIF
      ENDDO
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(mt+MW)
      CALL SETALPHAS(mt+MW)

!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)
      NGW=0.95*MW/GW

!     Z boson

      cw=SQRT(1d0-sw2)
      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt
      NGZ=0.95*MZ/GZ

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*(
     &  (ABS(VL)**2+ABS(VR)**2)*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)
     &   -12d0*xW**2*xb*DREAL(VL*CONJG(VR))
     &  +2d0*(ABS(gL)**2+ABS(gR)**2)*
     &     (1d0-xW**2/2d0-2d0*xb**2-xW**4/2d0-xW**2*xb**2/2d0+xb**4)
     &  -12d0*xW**2*xb*DREAL(gL*CONJG(gR))
     &  -6d0*xW*(DREAL(VL*CONJG(gR))+DREAL(VR*CONJG(gL)))*
     &     (1d0-xW**2-xb**2)
     &  +6d0*xW*xb*(DREAL(VL*CONJG(gL))+DREAL(VR*CONJG(gR)))*
     &     (1d0+xW**2-xb**2)
     &   )

      IF (IAN_DEC .EQ. 0) THEN
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)
      ENDIF

      NGt=0.95*mt/Gt

!     Higgs  -- not used

      GH=1d0

      PRINT 1100,mt,Gt
      PRINT 1101,DREAL(VL),DREAL(VR),DREAL(gL),DREAL(gR)
      PRINT 1102,0d0,DIMAG(VR),DIMAG(gL),DIMAG(gR)
      PRINT 999
!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO
      CALL SETPOL(istep,nhel,ncomb)
      DO i=1,4*NPROC
        DO j=1,ncomb
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

!
!     b PDF for subtraction
!

      IF (IMATCH .EQ. 1) CALL INIT_BPDF(mt+MW)
      PRINT 980,ILEP
      IF (IGEN .EQ. 1) PRINT 1301
      IF (IGEN .EQ. 2) PRINT 1302
      IF (IGEN .EQ. 3) PRINT 1303
      IF ((IGEN .NE. 3) .AND. (IMATCH .NE. 0)) THEN
        PRINT 1305
        IMATCH=0
      ENDIF
      PRINT 981,IMATCH
      PRINT 999

      RETURN

500   FORMAT ('______          _            ')
501   FORMAT ('| ___ \\        | |           ')           
502   FORMAT ('| |_/ / __ ___ | |_ ___  ___ ')
503   FORMAT ('|  __/ ''__/ _ \\| __/ _ \\/ __|')
504   FORMAT ('| |  | | | (_) | || (_) \\__ \\')
505   FORMAT ('\\_|  |_|  \\___/ \\__\\___/|___/')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
980   FORMAT ('Decay mode: ILEP = ',I1)
981   FORMAT ('Matching: IMATCH = ',I1)
999   FORMAT ('')
1050  FORMAT ('Warning: anomalous couplings deactivated in production')
1060  FORMAT ('Warning: anomalous couplings deactivated in decay')
1070  FORMAT ('******************************************************')
1100  FORMAT ('mt = ',F6.1,'   Gt = ',F7.3)
1101  FORMAT ('Re VL  = ',F7.4,'   Re VR  = ',F7.4,'   Re gL = ',F7.4,
     &  '   Re gR = ',F7.4)
1102  FORMAT ('Im VL  = ',F7.4,'   Im VR  = ',F7.4,'   Im gL = ',F7.4,
     &  '   Im gR = ',F7.4)
1200  FORMAT ('Scenario W_L selected')
1210  FORMAT ('Scenario W_0 selected')
1220  FORMAT ('Scenario W_R selected')
1230  FORMAT ('Make sure it is tuned for your mt,MW !!!!!')
1301  FORMAT ('Initial quark d/s/b : d')
1302  FORMAT ('Initial quark d/s/b : s')
1303  FORMAT ('Initial quark d/s/b : b')
1305  FORMAT ('Matching disabled, please be careful with parameters')
      END


      SUBROUTINE SETPOL(nspin,hel,nhel)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,NPART
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,NPART
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=NPART
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,NPART
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END


