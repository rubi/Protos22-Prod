      SUBROUTINE EVTOUT(IMODE)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

      REAL*8 EVTTHR
      PARAMETER(EVTTHR=1d4)     ! Fraction of temporary XMAXUP for which 
                                ! events are written to file

!     Arguments

      INTEGER IMODE

!     External functions used

      REAL*8 RAN2
      INTEGER idum
      COMMON /ranno/ idum

!     Data needed for each event

      REAL*8 Q1(0:3),Q2(0:3)
      COMMON /MOMINI/ Q1,Q2
      REAL*8 Pl(0:3),Pn(0:3),Pb(0:3),Pq1(0:3),Pq2(0:3),PbX(0:3)
      COMMON /MOMEXT/ Pl,Pn,Pb,Pq1,Pq2,PbX
      REAL*8 x1,x2,s,Q
      INTEGER IDIR,IF1,IF2
      COMMON /miscdata/ x1,x2,s,Q,IDIR,IF1,IF2
      REAL*8 FXNi(NPROC)
      COMMON /CROSS/ FXNi

!     Data needed for final statistics

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      INTEGER idum0
      COMMON /seed_ini/ idum0
      REAL*8 SIG(MAXGR),SIG2(MAXGR),ERR(MAXGR)
      INTEGER NIN,NOUT
      COMMON /STATS/ SIG,SIG2,ERR,NIN,NOUT
      REAL*8 SIG_t,ERR_t,SIG_tbar,ERR_tbar
      COMMON /FINALSTATS/ SIG_t,ERR_t,SIG_tbar,ERR_tbar
      REAL*8 mWbcut
      INTEGER IMATCH
      COMMON /MATCH/ mWbcut,IMATCH

!     Colour information

      INTEGER MAXSTR,MAXFL
      PARAMETER (MAXSTR=3,MAXFL=20)
      INTEGER ICSTR,IFL
      COMMON /COLOUR1/ ICSTR,IFL
      REAL*8 CAMP2(MAXSTR,0:MAXFL)
      COMMON /COLOUR2/ CAMP2

!     Multigrid integration (process selection)

      INTEGER INITGRID,IPROC
      COMMON /multigr/ INITGRID,IPROC

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l
      INTEGER NRUNS,IRUN
      COMMON /runs/ NRUNS,IRUN

!     Local variables to be saved

      REAL*8 XMAXUP
      INTEGER NUMEVT
      SAVE XMAXUP,NUMEVT
      CHARACTER*100 FILE0,FILE1
      SAVE FILE0,FILE1

!     Local variables for colour structure

      REAL*8 CFAPROB(0:MAXFL)
      INTEGER NFLOWS

!     Local variables

      INTEGER ID(NPART),ICOL1(NPART),ICOL2(NPART)
      REAL*8 XWGTUP,Pz1,Pz2,SIGtot,RCH
      INTEGER i

!     Initialise

      IF (IMODE .NE. -1) GOTO 10
      
      IF (NRUNS .EQ. 1) THEN
        FILE0='../../events/'//PROCNAME(1:l)//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'.par'
      ELSE
        FILE0='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.wgt'
        FILE1='../../events/'//PROCNAME(1:l)//'-r'
     &    //char(48+IRUN/10)//char(48+MOD(IRUN,10))//'.par'
      ENDIF

      OPEN (35,file=FILE0,status='unknown')

      NUMEVT=0
      XMAXUP=0d0
      NIN=0
      NOUT=0
      
      RETURN

10    IF (IMODE .NE. 0) GOTO 11
      
!     Select process according to previous grid selection

      XWGTUP=FXNi(IPROC)                          ! Already in pb
      IF (ABS(XWGTUP) .LT. XMAXUP/EVTTHR) RETURN
      XMAXUP=MAX(XMAXUP,ABS(XWGTUP))
      NUMEVT=NUMEVT+1

!     Flavour

      ID(1)=21
      ID(2)=21

      IF (IPROC .EQ. 1) THEN
        ID(5)=5
        ID(8)=-5
      ELSE
        ID(5)=-5
        ID(8)=5
      ENDIF

      IF (IF1 .EQ. 1) THEN
        ID(3)=12
        ID(4)=-11
      ELSE IF (IF1 .EQ. 2) THEN
        ID(3)=14
        ID(4)=-13
      ELSE IF (IF1 .EQ. 3) THEN
        ID(3)=16
        ID(4)=-15
      ELSE IF (IF1 .EQ. 4) THEN
        ID(3)=2
        ID(4)=-1
      ELSE IF (IF1 .EQ. 5) THEN
        ID(3)=4
        ID(4)=-3
      ELSE
        PRINT *,'Wrong IL'
        STOP
      ENDIF


      IF (IF2 .EQ. 1) THEN
        ID(6)=-12
        ID(7)=11
      ELSE IF (IF2 .EQ. 2) THEN
        ID(6)=-14
        ID(7)=13
      ELSE IF (IF2 .EQ. 3) THEN
        ID(6)=-16
        ID(7)=15
      ELSE IF (IF2 .EQ. 4) THEN
        ID(6)=-2
        ID(7)=1
      ELSE IF (IF2 .EQ. 5) THEN
        ID(6)=-4
        ID(7)=3
      ENDIF

      IF (ID(5) .LT. 0) THEN
        ID(3)=-ID(3)
        ID(4)=-ID(4)
        ID(6)=-ID(6)
        ID(7)=-ID(7)
      ENDIF

!     Colour

      DO i=1,NPART
        ICOL1(i)=0
        ICOL2(i)=0
      ENDDO

!     Select colour structure (only one)

      ICSTR=1
      NFLOWS=CAMP2(ICSTR,0)

!     Calculate probabilities for each colour flow
      
      SIGtot=0d0
      DO i=1,NFLOWS
        SIGtot=SIGtot+ABS(CAMP2(ICSTR,i))
      ENDDO
      CFAPROB(0)=0d0
      DO i=1,NFLOWS
        CFAPROB(i)=CFAPROB(i-1)+ABS(CAMP2(ICSTR,i))/SIGtot
      ENDDO

!     Select colour flow

      RCH=RAN2(IDUM)
      IFL=0
      DO WHILE (CFAPROB(IFL) .LT. RCH)
        IFL=IFL+1
      END DO
      IF (IFL .GT. NFLOWS) THEN
        PRINT *,'IFL > NFLOWS error'
        STOP
      ENDIF

      IF (IFL .EQ. 1) THEN
        ICOL1(1)=2
        ICOL2(1)=3
        ICOL1(2)=1
        ICOL2(2)=2
      ELSE
        ICOL1(1)=1
        ICOL2(1)=2
        ICOL1(2)=2
        ICOL2(2)=3
      ENDIF
      
      IF (ID(5) .GT. 0) THEN
        ICOL1(5)=1
        ICOL2(8)=3
      ELSE
        ICOL1(8)=1
        ICOL2(5)=3
      ENDIF

      DO i=3,4
        IF (ABS(ID(i)) .LT. 10) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=4
          ELSE
            ICOL2(i)=4
          ENDIF
        ENDIF
      ENDDO

      DO i=6,7
        IF (ABS(ID(i)) .LT. 10) THEN
          IF (ID(i) .GT. 0) THEN
            ICOL1(i)=5
          ELSE
            ICOL2(i)=5
          ENDIF
        ENDIF
      ENDDO

      IF (IDIR .EQ. 0) THEN
        Pz1=Q1(3)
        Pz2=Q2(3)
      ELSE
        PRINT *,'How is that IDIR = 1 here?'
        STOP
      ENDIF
      
      WRITE (35,3010) NUMEVT,XWGTUP,Q
      WRITE (35,3020) ID(1),ICOL1(1),ICOL2(1),Pz1
      WRITE (35,3020) ID(2),ICOL1(2),ICOL2(2),Pz2
      WRITE (35,3030) ID(3),ICOL1(3),ICOL2(3),Pn(1),Pn(2),Pn(3)
      WRITE (35,3030) ID(4),ICOL1(4),ICOL2(4),Pl(1),Pl(2),Pl(3)
      WRITE (35,3030) ID(5),ICOL1(5),ICOL2(5),Pb(1),Pb(2),Pb(3)
      WRITE (35,3030) ID(6),ICOL1(6),ICOL2(6),Pq1(1),Pq1(2),Pq1(3)
      WRITE (35,3030) ID(7),ICOL1(7),ICOL2(7),Pq2(1),Pq2(2),Pq2(3)
      WRITE (35,3030) ID(8),ICOL1(8),ICOL2(8),PbX(1),PbX(2),PbX(3)
      RETURN

11    IF (IMODE .NE. 1) GOTO 12

      CLOSE(35)

      OPEN (36,file=FILE1,status='unknown')
      WRITE (36,4000) 7
      WRITE (36,4001) ET
      WRITE (36,4005) mt,Gt,mb,MH
      WRITE (36,4006) IMATCH,mWbcut
      WRITE (36,4010) DREAL(vl),DREAL(vr),DREAL(gl),DREAL(gr)
      WRITE (36,4011) DIMAG(vl),DIMAG(vr),DIMAG(gl),DIMAG(gr)
      WRITE (36,4015) idum0
      WRITE (36,4020) NIN,NUMEVT
      WRITE (36,4030) XMAXUP
      WRITE (36,4040) SIG_t,ERR_t
      WRITE (36,4050) SIG_tbar,ERR_tbar
      CLOSE (36)
      RETURN



12    PRINT *,'Wrong IMODE in event output'
      STOP

3010  FORMAT (I10,' ',D12.6,' ',D12.6)
3020  FORMAT (I3,' ',I3,' ',I3,' ',D14.8)
3030  FORMAT (I3,' ',I3,' ',I3,' ',
     &  D14.8,' ',D14.8,' ',D14.8)
4000  FORMAT (I2,'                      ',
     & '         ! Process code')
4001  FORMAT (F6.0,'                           ! CM energy')
4005  FORMAT (F6.2,'  ',F4.2,'  ',F4.2,'  ',F6.1,
     &  '       ! mt, Gt, mb, MH')
4006  FORMAT (I1,'  ',F5.1,'                         ! Matching')
4010  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Re VL, Re VR, Re gL, Re gR')
4011  FORMAT (F6.3,'  ',F6.3,'  ',F6.3,'  ',F6.3,
     &  '   ! Im VL, Im VR, Im gL, Im gR')
4015  FORMAT (I5,
     &  '                            ! Initial random seed')
4020  FORMAT (I10,' ',I10,
     & '            ! Events generated, saved')
4030  FORMAT (D12.6,'                     ! Maximum weight')
4040  FORMAT (D12.6,'   ',D12.6,
     & '      ! t cross section and error')
4050  FORMAT (D12.6,'   ',D12.6,
     & '      ! tbar cross section and error')
      END

