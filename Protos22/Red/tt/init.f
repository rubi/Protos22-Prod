      SUBROUTINE INITPAR
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Functions used

      REAL*8 ALPHASPDF

!     Some inputs from main

      INTEGER IPDSET,IPPBAR
      COMMON /PDFFLAGS/ IPDSET,IPPBAR
      INTEGER ILEP
      COMMON /FSTATE/ ILEP
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR
      REAL*8 BRFL(3),BRFL_ac(0:3),WTFL(3)
      COMMON /topFL/ BRFL,BRFL_ac,WTFL

!     Outputs: parameters

      REAL*8 pi,ET
      COMMON /const/ pi,ET
      REAL*8 MZ,GZ,MW,GW,mt,Gt,mb,MH,GH,mtau,mc
      COMMON /SMMASS/ MZ,GZ,MW,GW,mt,Gt,MH,GH,mb,mc,mtau
      REAL*8 alpha,sw2,alpha_s,g,e,gs
      COMMON /SMCOUP/ alpha,sw2,alpha_s,g,e,gs
      REAL*8 fact(NPROC),WT_ITMX
      COMMON /GLOBALFACT/ fact,WT_ITMX
      COMPLEX*16 COUP_L(4),COUP_H(4)
      COMMON /tcoup2/ COUP_L,COUP_H

!     Other output

      REAL*8 alpha_MZ,alphas_MZ,sW2_MZ
      COMMON /COUPMZ/ alpha_MZ,alphas_MZ,sW2_MZ
      REAL*8 NGt,NGW,NGZ
      COMMON /BWdata/ NGt,NGW,NGZ
      INTEGER nhel(NPART,3**NPART)
      LOGICAL GOODHEL(4*NPROC,3**NPART)
      INTEGER ncomb,ntry(4*NPROC)
      COMMON /HELI/ nhel,GOODHEL,ncomb,ntry

!     Local variables to be saved

      INTEGER ISC_LEP,ISC_HAD,ISC
      REAL*8 coup(4,0:3)
      SAVE ISC_LEP,ISC_HAD,ISC,coup

!     Local variables

      INTEGER i,j,IOK
      REAL*8 xx,cw
      REAL*8 GZ_uc,GZ_dsb,GZ_nu,GZ_emt
      REAL*8 xW,xb,EW,qW
      REAL*8 gevpb
      REAL*8 WGT(3),WGTsum
      REAL*8 Gt0
      INTEGER istep(NPART)
      CHARACTER*20 parm(20)
      REAL*8 value(20)

!     Logo

      PRINT 999
      PRINT 500
      PRINT 501
      PRINT 502
      PRINT 503
      PRINT 504
      PRINT 505
      PRINT 999

      pi=2d0*dasin(1d0)
!
!     Tweaks
!
      OPEN (31,file='input/template.ini',status='old')
      DO i=1,3
      READ (31,*) coup(1,i),coup(2,i),coup(3,i),coup(4,i)
      ENDDO
      CLOSE (31)

      coup(1,0)=1d0
      coup(2,0)=0d0
      coup(3,0)=0d0
      coup(4,0)=0d0

      IOK=1
      OPEN (31,file='input/tweak.dat',status='old')
      READ (31,*) ISC
      READ (31,*) ISC_LEP
      IF ((ISC_LEP .LT. 0) .OR. (ISC_LEP .GT. 3)) IOK=0
      READ (31,*) ISC_HAD
      IF ((ISC_HAD .LT. 0) .OR. (ISC_HAD .GT. 3)) IOK=0
      READ (31,*) WGT(1),WGT(2),WGT(3)
      IF ( (WGT(1).LT.0.).OR.(WGT(2).LT.0.).OR.(WGT(3).LT.0.) ) IOK=0
      CLOSE (31)

      IF (IOK .EQ. 0) THEN
        PRINT *,'Wrong tweak.dat'
        STOP
      ENDIF

      WGTsum=WGT(1)+WGT(2)+WGT(3)
      BRFL_ac(0)=0d0
      DO i=1,3
        BRFL(i)=WGT(i)/WGTsum
        BRFL_ac(i)=BRFL_ac(i-1)+BRFL(i)
        WTFL(i)=1d0/BRFL(i)
        IF (BRFL(i) .EQ. 0d0) WTFL(i)=0d0
      ENDDO

!     PDF init

      parm(1)='DEFAULT'
      value(1)=IPDSET
      CALL SetLHAPARM('SILENT')
      CALL PDFSET(parm,value)
      PRINT 970,IPDSET
c      call getdesc()

!
!     SM parameters
!
      OPEN (31,FILE='input/smpar.dat',status='old')
      READ (31,*) MZ
      READ (31,*) MW
      READ (31,*) MH
      READ (31,*) mt
      READ (31,*) mb
      READ (31,*) mc
      READ (31,*) mtau
      READ (31,*) xx
      alpha_MZ=1d0/xx
      READ (31,*) sw2_MZ
      CLOSE (31)

      alphas_MZ=ALPHASPDF(mz)
      PRINT 973,alphas_MZ
      PRINT 999

!
!     Fixed parameters
!
      gevpb=3.8938d8
!
!     Global factor for cross sections
!
      DO i=1,NPROC
        fact(i)=gevpb                          ! Cross section in pb
        IF (ILEP .LE. 3) THEN
          fact(i)=fact(i)*12d0                 ! Semileptonic (lepton fixed)
        ELSE IF (ILEP .EQ. 4) THEN
          fact(i)=fact(i)*36d0                 ! Semileptonic (sum)
        ELSE IF (ILEP .EQ. 5) THEN
          fact(i)=fact(i)*36d0                 ! Full hadronic
        ELSE
          fact(i)=fact(i)*9d0                  ! Dileptonic
        ENDIF
      ENDDO
      RETURN

      ENTRY REINITPAR

!
!     Running coupling constants
!
      CALL SETALPHA(mt)
      CALL SETALPHAS(mt)

      IF (ISC .EQ. 0) THEN
        COUP_L(1) = vl
        COUP_L(2) = vr
        COUP_L(3) = gl
        COUP_L(4) = gr
        COUP_H(1) = vl
        COUP_H(2) = vr
        COUP_H(3) = gl
        COUP_H(4) = gr
      ELSE
        DO i=1,4
          COUP_L(i) = coup(i,ISC_LEP)
          COUP_H(i) = coup(i,ISC_HAD)
        ENDDO
      ENDIF
      
!
!     Calculate widths, etc.
!

!     W boson

      GW=9d0*g**2*MW/(48d0*pi)
      NGW=0.95*MW/GW

!     Z boson

      cw=SQRT(1d0-sw2)
      GZ_uc=6d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (1d0-4d0/3d0*sW2)**2 + (-4d0/3d0*sW2)**2 )
      GZ_dsb=9d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0/3d0*sW2)**2 + (2d0/3d0*sW2)**2 )
      GZ_nu=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0)**2  )
      GZ_emt=3d0*g**2/(96d0*pi*cw**2)*MZ*
     &  ( (-1d0+2d0*sW2)**2 + (2d0*sW2)**2 )
      GZ=GZ_uc+GZ_dsb+GZ_nu+GZ_emt
      NGZ=0.95*MZ/GZ

!     t quark

      xW=MW/mt
      xb=mb/mt
      EW=(mt**2+MW**2-mb**2)/(2d0*mt)
      qW=SQRT(EW**2-MW**2)
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*(
     &  (ABS(VL)**2+ABS(VR)**2)*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)
     &   -12d0*xW**2*xb*DREAL(VL*CONJG(VR))
     &  +2d0*(ABS(gL)**2+ABS(gR)**2)*
     &     (1d0-xW**2/2d0-2d0*xb**2-xW**4/2d0-xW**2*xb**2/2d0+xb**4)
     &  -12d0*xW**2*xb*DREAL(gL*CONJG(gR))
     &  -6d0*xW*(DREAL(VL*CONJG(gR))+DREAL(VR*CONJG(gL)))*
     &     (1d0-xW**2-xb**2)
     &  +6d0*xW*xb*(DREAL(VL*CONJG(gL))+DREAL(VR*CONJG(gR)))*
     &     (1d0+xW**2-xb**2)
     &   )
      Gt0=g**2/(32d0*Pi)*qW*(mt/MW)**2*(
     &  (ABS(VL)**2+ABS(VR)**2)*
     &     (1d0+xW**2-2d0*xW**4)
     &  +2d0*(ABS(gL)**2+ABS(gR)**2)*
     &     (1d0-xW**2/2d0-xW**4/2d0)
     &  -6d0*xW*(DREAL(VL*CONJG(gR))+DREAL(VR*CONJG(gL)))*
     &     (1d0-xW**2)
     &   )

      IF (BRFL(1)+BRFL(2) .NE. 0d0) THEN
      PRINT 1310
      PRINT 1320,BRFL(1),BRFL(2),BRFL(3)
      PRINT 999
      Gt=BRFL(3)*Gt+(BRFL(1)+BRFL(2))*Gt0
      ENDIF

      IF (ISC .NE. 0) THEN
      Gt=g**2/(32d0*Pi)*qW*(mt/MW)**2*
     &     (1d0+xW**2-2d0*xb**2-2d0*xW**4+xW**2*xb**2+xb**4)
      ENDIF

      NGt=0.95*mt/Gt

!     Higgs  -- not used

      GH=1d0

      IF (ISC .EQ. 1) THEN
        IF (ISC_LEP .EQ. 1) PRINT 1200
        IF (ISC_LEP .EQ. 2) PRINT 1210
        IF (ISC_LEP .EQ. 3) PRINT 1220
        IF (ISC_LEP .EQ. 0) PRINT 1230
        IF (ISC_HAD .EQ. 1) PRINT 1201
        IF (ISC_HAD .EQ. 2) PRINT 1211
        IF (ISC_HAD .EQ. 3) PRINT 1221
        IF (ISC_HAD .EQ. 0) PRINT 1231
        PRINT 1240
        PRINT 999
      ENDIF
      PRINT 1100,mt,Gt
      IF (ISC .EQ. 0) THEN
      PRINT 1101,DREAL(VL),DREAL(VR),DREAL(gL),DREAL(gR)
      PRINT 1102,0d0,DIMAG(VR),DIMAG(gL),DIMAG(gR)
      ENDIF
      PRINT 999
!
!     Helicity combinations
!
      DO i=1,NPART
        istep(i)=2
      ENDDO
      CALL SETPOL(istep,nhel,ncomb)
      DO i=1,4*NPROC
        DO j=1,ncomb
          GOODHEL(i,j)=.FALSE.
        ENDDO
        ntry(i)=0
      ENDDO

      PRINT 980,ILEP
      PRINT 999

      RETURN

500   FORMAT ('______          _            ')
501   FORMAT ('| ___ \\        | |           ')           
502   FORMAT ('| |_/ / __ ___ | |_ ___  ___ ')
503   FORMAT ('|  __/ ''__/ _ \\| __/ _ \\/ __|')
504   FORMAT ('| |  | | | (_) | || (_) \\__ \\')
505   FORMAT ('\\_|  |_|  \\___/ \\__\\___/|___/')
970   FORMAT ('PDF set ',I5,' selected')
973   FORMAT ('alphas_MZ = ',F6.4,' from PDF set')
980   FORMAT ('Decay mode: ILEP = ',I1)
999   FORMAT ('')
1100  FORMAT ('mt = ',F6.1,'   Gt = ',F7.3)
1101  FORMAT ('Re VL  = ',F7.4,'   Re VR  = ',F7.4,'   Re gL = ',F7.4,
     &  '   Re gR = ',F7.4)
1102  FORMAT ('Im VL  = ',F7.4,'   Im VR  = ',F7.4,'   Im gL = ',F7.4,
     &  '   Im gR = ',F7.4)
1200  FORMAT ('Scenario W_L selected for t1')
1210  FORMAT ('Scenario W_0 selected for t1')
1220  FORMAT ('Scenario W_R selected for t1')
1230  FORMAT ('Scenario SM  selected for t1')
1201  FORMAT ('Scenario W_L selected for t2')
1211  FORMAT ('Scenario W_0 selected for t2')
1221  FORMAT ('Scenario W_R selected for t2')
1231  FORMAT ('Scenario SM  selected for t2')
1240  FORMAT ('Make sure they are tuned for your mt,MW !!!!!')
1310  FORMAT ('Non-standard top mixing selected')
1320  FORMAT ('Br(t -> Wd) = ',F6.3,'   Br(t -> Ws) = ',F6.3,
     &  '   Br(t -> Wb) = ',F6.3)
      END


      SUBROUTINE SETPOL(nspin,hel,nhel)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/generator.inc'

!     Arguments

      INTEGER nspin(NPART),hel(NPART,3**NPART),nhel

!     Local variables

      INTEGER i,j
      LOGICAL change

!     Set first combination

      nhel=1
      DO i=1,NPART
        hel(i,nhel)=-1
      ENDDO

!     Fill the rest of combinations

      change=.TRUE.
      DO WHILE (change)
        change=.FALSE.
        nhel=nhel+1
        DO i=1,NPART
          hel(i,nhel)=hel(i,nhel-1)
        ENDDO
        j=NPART
        DO WHILE (hel(j,nhel) .EQ. 1)
          j=j-1
        ENDDO
        IF (j .GT. 0) THEN
          change=.TRUE.
          hel(j,nhel) = hel(j,nhel)+nspin(j)
          DO i=j+1,NPART
            hel(i,nhel)=-1
          ENDDO
        ELSE
          change=.FALSE.
        ENDIF
      ENDDO
      nhel=nhel-1
      RETURN
      END


