      SUBROUTINE CHGPAR(id,step)
      IMPLICIT NONE

!     Arguments

      INTEGER id
      REAL*8 step

!     Parameters that can be changed

      INTEGER idum0
      COMMON /seed_ini/ idum0
      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR

      IF (id .EQ. 1) THEN
        VL=VL+step
      ELSE IF (id .EQ. 2) THEN
        VR=VR+step
      ELSE IF (id .EQ. 3) THEN
        gL=gL+step
      ELSE IF (id .EQ. 4) THEN
        gR=gR+step
      ELSE IF (id .EQ. 5) THEN
        VR=VR+step*(0d0,1d0)
      ELSE IF (id .EQ. 6) THEN
        gL=gL+step*(0d0,1d0)
      ELSE IF (id .EQ. 7) THEN
        gR=gR+step*(0d0,1d0)
      ELSE IF (id .EQ. -1) THEN
        idum0=idum0+1
      ELSE
        print *,id
        PRINT 100
        STOP
      ENDIF

      RETURN

100   FORMAT ('Unsupported parameter scan')
      END


      SUBROUTINE LOGINI(IMODE,id)
      IMPLICIT NONE

!     Parameters

      INCLUDE 'input/output.inc'

!     Arguments

      INTEGER IMODE,id

!     Parameters

      COMPLEX*16 VL,VR,gL,gR
      COMMON /tcoup/ VL,VR,gL,gR

!     For logging

      REAL*8 SIG_tot,Rt
      COMMON /logextra/ SIG_tot,Rt
      REAL*8 asym(n_a)
      COMMON /ASYMRES/ asym

!     For file output

      CHARACTER*100 PROCNAME
      INTEGER l
      COMMON /prname/ PROCNAME,l

!     Local

      REAL*8 scpar

      IF (IMODE .EQ. -1) THEN 
        OPEN (40,FILE='output/'//PROCNAME(1:l)//'.log',
     &    status='unknown')
        RETURN
      ELSE IF (IMODE .EQ. 1) THEN
        CLOSE (40)
        RETURN
      ELSE IF (IMODE .NE. 0) THEN
        PRINT 99
        STOP
      ENDIF

!     Write results

      IF ((id .EQ. 0) .OR. (id .EQ. -1)) THEN
        scpar=0d0
      ELSE IF (id .EQ. 1) THEN
        scpar=DREAL(VL)
      ELSE IF (id .EQ. 2) THEN
        scpar=DREAL(VR)
      ELSE IF (id .EQ. 3) THEN
        scpar=DREAL(gL)
      ELSE IF (id .EQ. 4) THEN
        scpar=DREAL(gR)
      ELSE IF (id .EQ. 5) THEN
        scpar=DIMAG(VR)
      ELSE IF (id .EQ. 6) THEN
        scpar=DIMAG(gL)
      ELSE IF (id .EQ. 7) THEN
        scpar=DIMAG(gR)
      ELSE
        PRINT 100
        STOP
      ENDIF

      WRITE (40,200) scpar,SIG_tot,Rt,asym(2),asym(3),asym(10)

      RETURN
99    FORMAT ('Error in WRITELOG call')
100   FORMAT ('Unsupported parameter scan')
200   FORMAT (F7.4,' ',D12.6,' ',D12.6,' ',D12.6,' ',D12.6,' ',D12.6)
      END


